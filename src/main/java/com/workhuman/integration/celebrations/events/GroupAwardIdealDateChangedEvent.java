package com.workhuman.integration.celebrations.events;

import java.io.Serializable;

public class GroupAwardIdealDateChangedEvent implements Serializable {
    private static final long serialVersionUID = 1L;
    protected long clientId;
    protected long linkId;

    public GroupAwardIdealDateChangedEvent() {
    }

    public long getClientId() {
        return this.clientId;
    }

    public void setClientId(long value) {
        this.clientId = value;
    }

    public long getLinkId() {
        return this.linkId;
    }

    public void setLinkId(long value) {
        this.linkId = value;
    }
}
