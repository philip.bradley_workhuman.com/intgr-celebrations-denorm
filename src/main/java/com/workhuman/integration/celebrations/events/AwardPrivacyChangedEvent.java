package com.workhuman.integration.celebrations.events;

import java.io.Serializable;
import java.util.Calendar;

public class AwardPrivacyChangedEvent implements Serializable {

    private static final long serialVersionUID = 1L;
    protected long clientId;
    protected long awardHistoryId;
    protected String glbcertId;
    protected boolean awardShared;
    protected boolean awardPrivate;
    protected boolean awardRestricted;
    protected Calendar created;

    public AwardPrivacyChangedEvent() {
    }

    public long getClientId() {
        return this.clientId;
    }

    public void setClientId(long value) {
        this.clientId = value;
    }

    public long getAwardHistoryId() {
        return this.awardHistoryId;
    }

    public void setAwardHistoryId(long value) {
        this.awardHistoryId = value;
    }

    public String getGlbcertId() {
        return this.glbcertId;
    }

    public void setGlbcertId(String value) {
        this.glbcertId = value;
    }

    public boolean isAwardShared() {
        return this.awardShared;
    }

    public void setAwardShared(boolean value) {
        this.awardShared = value;
    }

    public boolean isAwardPrivate() {
        return this.awardPrivate;
    }

    public void setAwardPrivate(boolean value) {
        this.awardPrivate = value;
    }

    public boolean isAwardRestricted() {
        return this.awardRestricted;
    }

    public void setAwardRestricted(boolean value) {
        this.awardRestricted = value;
    }

    public Calendar getCreated() {
        return this.created;
    }

    public void setCreated(Calendar value) {
        this.created = value;
    }
}
