package com.workhuman.integration.celebrations.events;

import java.io.Serializable;

public class AwardGalleryUpdatedEvent implements Serializable {
    private static final long serialVersionUID = 1L;
    protected long userId;
    protected String mediaEntryId;
    protected Long awardHistoryId;

    public AwardGalleryUpdatedEvent() {
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long value) {
        this.userId = value;
    }

    public String getMediaEntryId() {
        return this.mediaEntryId;
    }

    public void setMediaEntryId(String value) {
        this.mediaEntryId = value;
    }

    public Long getAwardHistoryId() {
        return this.awardHistoryId;
    }

    public void setAwardHistoryId(Long value) {
        this.awardHistoryId = value;
    }
}
