package com.workhuman.integration.celebrations.events;

import java.io.Serializable;
import java.util.Calendar;

public class AwardIdealDateChangedEvent implements Serializable {

    private static final long serialVersionUID = 1L;
    protected long clientId;
    protected String glbcertId;
    protected String indOrderId;
    protected Calendar newIdealDate;

    public AwardIdealDateChangedEvent() {
    }

    public long getClientId() {
        return this.clientId;
    }

    public void setClientId(long value) {
        this.clientId = value;
    }

    public String getGlbcertId() {
        return this.glbcertId;
    }

    public void setGlbcertId(String value) {
        this.glbcertId = value;
    }

    public String getIndOrderId() {
        return this.indOrderId;
    }

    public void setIndOrderId(String value) {
        this.indOrderId = value;
    }

    public Calendar getNewIdealDate() {
        return this.newIdealDate;
    }

    public void setNewIdealDate(Calendar value) {
        this.newIdealDate = value;
    }
}
