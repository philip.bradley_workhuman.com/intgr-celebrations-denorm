package com.workhuman.integration.celebrations.events;

import java.io.Serializable;

public class GroupAwardGalleryUpdatedEvent implements Serializable {
    private static final long serialVersionUID = 1L;
    protected long userId;
    protected String mediaEntryId;
    protected Long linkId;

    public GroupAwardGalleryUpdatedEvent() {
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long value) {
        this.userId = value;
    }

    public String getMediaEntryId() {
        return this.mediaEntryId;
    }

    public void setMediaEntryId(String value) {
        this.mediaEntryId = value;
    }

    public Long getLinkId() {
        return this.linkId;
    }

    public void setLinkId(Long value) {
        this.linkId = value;
    }
}
