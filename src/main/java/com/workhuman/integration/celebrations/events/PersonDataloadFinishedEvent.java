package com.workhuman.integration.celebrations.events;

import java.io.Serializable;
import java.util.Calendar;

public class PersonDataloadFinishedEvent implements Serializable {
    private static final long serialVersionUID = 1L;
    protected String dataloadId;
    protected long clientId;
    protected String clientName;
    protected Calendar executionTime;

    public PersonDataloadFinishedEvent() {
    }

    public String getDataloadId() {
        return this.dataloadId;
    }

    public void setDataloadId(String value) {
        this.dataloadId = value;
    }

    public long getClientId() {
        return this.clientId;
    }

    public void setClientId(long value) {
        this.clientId = value;
    }

    public String getClientName() {
        return this.clientName;
    }

    public void setClientName(String value) {
        this.clientName = value;
    }

    public Calendar getExecutionTime() {
        return this.executionTime;
    }

    public void setExecutionTime(Calendar value) {
        this.executionTime = value;
    }
}
