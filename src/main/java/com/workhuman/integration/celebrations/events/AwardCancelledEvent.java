package com.workhuman.integration.celebrations.events;

import java.io.Serializable;
import java.util.Calendar;

public class AwardCancelledEvent implements Serializable {
    private static final long serialVersionUID = 1L;
    protected long clientId;
    protected long awardHistoryId;
    protected String glbcertId;
    protected long personId;
    protected Calendar created;


    public long getClientId() {
        return this.clientId;
    }

    public void setClientId(long value) {
        this.clientId = value;
    }

    public long getAwardHistoryId() {
        return this.awardHistoryId;
    }

    public void setAwardHistoryId(long value) {
        this.awardHistoryId = value;
    }

    public String getGlbcertId() {
        return this.glbcertId;
    }

    public void setGlbcertId(String value) {
        this.glbcertId = value;
    }

    public long getPersonId() {
        return this.personId;
    }

    public void setPersonId(long value) {
        this.personId = value;
    }

    public Calendar getCreated() {
        return this.created;
    }

    public void setCreated(Calendar value) {
        this.created = value;
    }
}
