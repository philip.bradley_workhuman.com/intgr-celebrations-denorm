package com.workhuman.integration.celebrations.events;

import java.io.Serializable;
import java.util.Calendar;

public class AwardDeliveredEvent implements Serializable  {

    private static final long serialVersionUID = 1L;
    protected long clientId;
    protected long awarderId;
    protected long awardTypeId;
    protected long awardId;

    protected String glbcertId;
    protected long awardeeId;
    protected Long awardeeManagerId;
    protected long clientProgramId;
    protected long productId;

    protected Calendar publicDate;

    protected Calendar created;


    public long getClientId() {
        return this.clientId;
    }

    public void setClientId(long value) {
        this.clientId = value;
    }

    public long getAwarderId() {
        return this.awarderId;
    }

    public void setAwarderId(long value) {
        this.awarderId = value;
    }

    public long getAwardTypeId() {
        return this.awardTypeId;
    }

    public void setAwardTypeId(long value) {
        this.awardTypeId = value;
    }

    public long getAwardId() {
        return this.awardId;
    }

    public void setAwardId(long value) {
        this.awardId = value;
    }

    public String getGlbcertId() {
        return this.glbcertId;
    }

    public void setGlbcertId(String value) {
        this.glbcertId = value;
    }

    public long getAwardeeId() {
        return this.awardeeId;
    }

    public void setAwardeeId(long value) {
        this.awardeeId = value;
    }

    public Long getAwardeeManagerId() {
        return this.awardeeManagerId;
    }

    public void setAwardeeManagerId(Long value) {
        this.awardeeManagerId = value;
    }

    public long getClientProgramId() {
        return this.clientProgramId;
    }

    public void setClientProgramId(long value) {
        this.clientProgramId = value;
    }

    public long getProductId() {
        return this.productId;
    }

    public void setProductId(long value) {
        this.productId = value;
    }

    public Calendar getPublicDate() {
        return this.publicDate;
    }

    public void setPublicDate(Calendar value) {
        this.publicDate = value;
    }

    public Calendar getCreated() {
        return this.created;
    }

    public void setCreated(Calendar value) {
        this.created = value;
    }
}
