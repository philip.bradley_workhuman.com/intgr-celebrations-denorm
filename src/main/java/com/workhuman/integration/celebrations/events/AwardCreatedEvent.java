package com.workhuman.integration.celebrations.events;

import java.io.Serializable;
import java.util.Calendar;

public class AwardCreatedEvent implements Serializable {
    private static final long serialVersionUID = 1L;
    protected long awardId;
    protected String glbcertId;
    protected long awardeeId;
    protected long clientId;
    protected long awarderId;
    protected Calendar publicDate;
    protected long productTypeId;
    protected long awardTypeId;
    protected long nominationGroupId;
    protected boolean messagePrivate;
    protected boolean awardPrivate;
    protected Calendar highlightPublicDate;
    protected Calendar releaseDate;
    protected Long misuseDelay;
    protected Boolean misuse;

    public AwardCreatedEvent() {
    }

    public long getAwardId() {
        return awardId;
    }

    public void setAwardId(long awardId) {
        this.awardId = awardId;
    }

    public String getGlbcertId() {
        return glbcertId;
    }

    public void setGlbcertId(String glbcertId) {
        this.glbcertId = glbcertId;
    }

    public long getAwardeeId() {
        return awardeeId;
    }

    public void setAwardeeId(long awardeeId) {
        this.awardeeId = awardeeId;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public long getAwarderId() {
        return awarderId;
    }

    public void setAwarderId(long awarderId) {
        this.awarderId = awarderId;
    }

    public Calendar getPublicDate() {
        return publicDate;
    }

    public void setPublicDate(Calendar publicDate) {
        this.publicDate = publicDate;
    }

    public long getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(long productTypeId) {
        this.productTypeId = productTypeId;
    }

    public long getAwardTypeId() {
        return awardTypeId;
    }

    public void setAwardTypeId(long awardTypeId) {
        this.awardTypeId = awardTypeId;
    }

    public long getNominationGroupId() {
        return nominationGroupId;
    }

    public void setNominationGroupId(long nominationGroupId) {
        this.nominationGroupId = nominationGroupId;
    }

    public boolean isMessagePrivate() {
        return messagePrivate;
    }

    public void setMessagePrivate(boolean messagePrivate) {
        this.messagePrivate = messagePrivate;
    }

    public boolean isAwardPrivate() {
        return awardPrivate;
    }

    public void setAwardPrivate(boolean awardPrivate) {
        this.awardPrivate = awardPrivate;
    }

    public Calendar getHighlightPublicDate() {
        return highlightPublicDate;
    }

    public void setHighlightPublicDate(Calendar highlightPublicDate) {
        this.highlightPublicDate = highlightPublicDate;
    }

    public Calendar getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Calendar releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Long getMisuseDelay() {
        return misuseDelay;
    }

    public void setMisuseDelay(Long misuseDelay) {
        this.misuseDelay = misuseDelay;
    }

    public Boolean getMisuse() {
        return misuse;
    }

    public void setMisuse(Boolean misuse) {
        this.misuse = misuse;
    }
}
