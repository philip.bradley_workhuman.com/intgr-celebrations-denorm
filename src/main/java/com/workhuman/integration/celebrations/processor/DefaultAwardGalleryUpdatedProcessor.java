package com.workhuman.integration.celebrations.processor;


import com.workhuman.integration.celebrations.service.IntgrCelebrationDenormService;
import com.workhuman.integration.celebrations.service.celebration.CelebrationAwardDenormService;
import com.workhuman.integration.celebrations.service.celebration.IntgrCelebrationDenormVO;
import com.workhuman.integration.celebrations.service.media.MediaDetailsService;
import com.workhuman.integration.celebrations.service.media.MediaValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Default Award Gallery Updated processor. Contains common logic to all related processors.
 */
public class DefaultAwardGalleryUpdatedProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultAwardGalleryUpdatedProcessor.class);

    @Autowired
    protected IntgrCelebrationDenormService intgrCelebrationDenormService;

    @Autowired
    protected CelebrationAwardDenormService celebrationAwardDenormService;

    @Autowired
    protected MediaDetailsService mediaDetailsService;

    @Autowired
    protected MediaValidator mediaValidator;

    boolean shouldSkipProcessing(final long userId, final String eventMediaEntryId, final IntgrCelebrationDenormVO denormalizedAward) {
        return (isDenormalizedAwardNull(eventMediaEntryId, denormalizedAward))
                || (doesMediaEntryNotBelongToAwardGallery(userId, eventMediaEntryId))
                || (isRemovedMediaNotTheFirstAwardMedia(userId, eventMediaEntryId, denormalizedAward));
    }

    boolean doesMediaEntryNotBelongToAwardGallery(final long userId, final String eventMediaEntryId) {
        if (mediaValidator.mediaEntryDoesNotBelongToAwardGallery(eventMediaEntryId)) {
            LOGGER.info(String.format("Processing is SKIPPED as New Media Entry with MediaEntryId [ '%s' ]  DOES NOT BELONG to Celebration Award Gallery for userId [ %d ]",
                    eventMediaEntryId, userId));
            return true;
        }
        return false;
    }

    boolean isDenormalizedAwardNull(final String eventMediaEntryId, final IntgrCelebrationDenormVO denormalizedAward) {
        if (denormalizedAward == null) {
            LOGGER.warn(String.format("Processing is SKIPPED as NO RECORDS WERE FOUND in REC_CELEBRATIONS_DENORN for MediaEntryId [ '%s' ]",
                    eventMediaEntryId));
            return true;
        }
        return false;
    }

    boolean areMediaEntriesEquals(final long userId, final IntgrCelebrationDenormVO denormalizedAward, final String newFirstMediaEntryId) {
        if (mediaValidator.areEquals(newFirstMediaEntryId, denormalizedAward)) {
            LOGGER.info(String.format("Processing is SKIPPED as new First Media Entry with MediaEntryId [ '%s' ] is already THE FIRST media entry in Celebration Award Gallery for userId [ %d ]",
                    newFirstMediaEntryId, userId));
            return true;
        }
        return false;
    }

    boolean isRemovedMediaNotTheFirstAwardMedia(final long userId, final String eventMediaEntryId, final IntgrCelebrationDenormVO denormalizedAward) {
        if (mediaValidator.isRemovedMediaNotTheFirstAwardMedia(eventMediaEntryId, denormalizedAward)) {
            LOGGER.info(String.format("Processing is SKIPPED as Removed Media with MediaEntryId [ '%s' ] is NOT THE FIRST media entry in Celebration Award Gallery for userId [ %d ]",
                    eventMediaEntryId, userId));
            return true;
        }
        return false;
    }
}
