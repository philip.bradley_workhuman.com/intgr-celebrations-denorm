package com.workhuman.integration.celebrations.processor;

import com.workhuman.integration.celebrations.events.AwardGalleryUpdatedEvent;
import com.workhuman.integration.celebrations.service.celebration.IntgrCelebrationDenormVO;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AwardGalleryUpdatedProcessor extends DefaultAwardGalleryUpdatedProcessor implements Processor {

    private static final Logger LOGGER = LoggerFactory.getLogger(AwardGalleryUpdatedProcessor.class);

    @Override
    public void process(final Exchange exchange) {
        final AwardGalleryUpdatedEvent awardGalleryUpdatedEvent = exchange.getIn().getBody(AwardGalleryUpdatedEvent.class);

        final long userId = awardGalleryUpdatedEvent.getUserId();
        final String eventMediaEntryId = awardGalleryUpdatedEvent.getMediaEntryId();
        final Long eventAwardHistoryId = awardGalleryUpdatedEvent.getAwardHistoryId();

        LOGGER.info(String.format("Processing AwardGalleryUpdatedEvent for userId= %d; awardHistoryId= '%d'; mediaEntryId= '%s' ",
                userId, eventAwardHistoryId, eventMediaEntryId));

        final IntgrCelebrationDenormVO denormalizedAward = celebrationAwardDenormService.findByAwardHistoryId(eventAwardHistoryId);
        if (shouldSkipProcessing(userId, eventMediaEntryId, denormalizedAward)) {
            return;
        }

        final String newFirstMediaEntryId = mediaDetailsService.findFirstMediaEntryId(denormalizedAward);
        if (areMediaEntriesEquals(userId, denormalizedAward, newFirstMediaEntryId)) return;

        intgrCelebrationDenormService.denormalizeSingleAwardGalleryUpdated(denormalizedAward.getClientId(),
                denormalizedAward.getAwardHistoryId(), newFirstMediaEntryId);
    }
}
