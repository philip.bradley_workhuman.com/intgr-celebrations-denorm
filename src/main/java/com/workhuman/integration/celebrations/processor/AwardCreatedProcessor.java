package com.workhuman.integration.celebrations.processor;

import com.workhuman.integration.celebrations.events.AwardCreatedEvent;
import com.workhuman.integration.celebrations.service.award.AwardDetailsTO;
import com.workhuman.integration.celebrations.service.exception.IntgrCelebrationDenormServiceException;
import com.workhuman.integration.celebrations.service.order.OrderDetailsTO;
import com.workhuman.integration.celebrations.service.utils.ProductIdFamily;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Processes AwardCreatedEvent which is triggered when the award is approved
 */
@Component
public class AwardCreatedProcessor extends CelebrationEventsProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(AwardCreatedProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        AwardCreatedEvent awardCreatedEvent = exchange.getIn().getBody(AwardCreatedEvent.class);
        Long clientId = awardCreatedEvent.getClientId();
        String glbcertId = awardCreatedEvent.getGlbcertId();
        Long awardHistoryId = awardCreatedEvent.getAwardId();
        Long productId = awardCreatedEvent.getProductTypeId();

        log.info(String.format("Processing AwardCreatedEvent for clientId= %d; glbcertId= '%s'; awardHistoryId= %d; productId=[%d]",
                clientId, glbcertId, awardHistoryId, productId));

        processEvent(clientId, glbcertId,null, awardHistoryId, productId);
    }

    protected AwardDetailsTO getAwardDetails(long clientId, Long awardHistoryId, OrderDetailsTO orderDetails) {
        Long productId = orderDetails.getProductId();
        String glbcertId = orderDetails.getGlbcertUniqueId();
        
        AwardDetailsTO awardDetails = null;
        if (ProductIdFamily.isYOSProdId(productId)) {
            awardDetails = awardDetailsService.findServiceMilestonesAward(clientId, glbcertId);
            if (awardDetails != null) {
                awardDetails.setTimelines(true);
            }
        } else if (ProductIdFamily.isPremiumAwardsProdId(productId)) {
            awardDetails = awardDetailsService.findLifeEventAward(clientId, glbcertId);
        }
        
        return awardDetails; 
    }


    @Override
    protected void handleInvalidResult(String errorMessage) {
        throw new IntgrCelebrationDenormServiceException(errorMessage);
    }

    protected void denormalizeAwardDetails(AwardDetailsTO awardDetails) {
        intgrCelebrationDenormService.denormalizeAwardCreation(awardDetails);
        intgrCelebrationDenormService.syncGroupAwardMedia(awardDetails);
    }

    @Override
    protected Logger getLog() {
        return log;
    }
}
