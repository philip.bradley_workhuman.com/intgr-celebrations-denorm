package com.workhuman.integration.celebrations.processor;

import com.workhuman.integration.celebrations.events.GroupAwardIdealDateChangedEvent;
import com.workhuman.integration.celebrations.service.IntgrCelebrationDenormService;
import com.workhuman.integration.celebrations.service.exception.IntgrCelebrationDenormUpdateDataException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GroupAwardIdealDateChangedProcessor implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(GroupAwardIdealDateChangedProcessor.class);

    @Autowired
    protected IntgrCelebrationDenormService intgrCelebrationDenormService;

    @Override
    public void process(Exchange exchange) throws Exception {
        GroupAwardIdealDateChangedEvent event = exchange.getIn().getBody(GroupAwardIdealDateChangedEvent.class);
        long clientId = event.getClientId();
        long linkId = event.getLinkId();
        LOG.info(String.format("Processing GroupAwardIdealDateChangedEvent for clientId= %d; linkId= '%d' ", clientId, linkId));

        validateParameterNotZero("clientId", clientId);
        validateParameterNotZero("linkId", linkId);

        intgrCelebrationDenormService.denormalizeGroupAwardDeliveryDateChanged(clientId, linkId);
    }

    private void validateParameterNotZero(String paramName, long paramValue) {
        if (paramValue == 0) {
            String msg = String.format("GroupAwardIdealDateChangedEvent: [%s] cannot be 0", paramName);
            LOG.warn(msg);
            throw new IntgrCelebrationDenormUpdateDataException(msg);
        }
    }
}
