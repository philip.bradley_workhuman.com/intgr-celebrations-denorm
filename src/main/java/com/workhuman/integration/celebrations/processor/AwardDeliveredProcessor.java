package com.workhuman.integration.celebrations.processor;

import com.workhuman.integration.celebrations.events.AwardDeliveredEvent;
import com.workhuman.integration.celebrations.service.award.AwardDetailsTO;
import com.workhuman.integration.celebrations.service.order.OrderDetailsTO;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
 * <awardDeliveredEvent xmlns="http://www.globoforce.com/integration/schemas/awardDeliveredEvent">
 *     <clientId>901</clientId>
 *     <awarderId>3192878</awarderId>
 *     <awardTypeId>122361</awardTypeId>
 *     <awardId>7699829</awardId>
 *     <glbcertId>KYEA-6KTYK2-WQD42N</glbcertId>
 *     <awardeeId>3192873</awardeeId>
 *     <clientProgramId>999999900315</clientProgramId>
 *     <productId>680</productId>
 *     <publicDate>2018-08-16T07:42:58Z</publicDate>
 *     <created>2018-08-16T07:42:58Z</created>
 * </awardDeliveredEvent>
 */
@Component
public class AwardDeliveredProcessor extends CelebrationEventsProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(AwardDeliveredProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        AwardDeliveredEvent awardDeliveredEvent = exchange.getIn().getBody(AwardDeliveredEvent.class);

        Long clientId = awardDeliveredEvent.getClientId();
        String glbcertId = awardDeliveredEvent.getGlbcertId();
        Long awardHistoryId = awardDeliveredEvent.getAwardId();
        Long productId = awardDeliveredEvent.getProductId();

        log.info(String.format("Processing AwardDeliveredEvent for clientId= %d; glbcertId= '%s'; awardHistoryId= %d ",
                clientId, glbcertId, awardHistoryId));
        
        processEvent(clientId, glbcertId, null, awardHistoryId, productId);
    }
    
    @Override
    protected AwardDetailsTO getAwardDetails(long clientId, Long awardHistoryId, OrderDetailsTO orderDetails) {
        AwardDetailsTO awardDetails = new AwardDetailsTO();
        awardDetails.setPkClient(clientId);
        awardDetails.setGlbcertUniqueId(orderDetails.getGlbcertUniqueId());
        awardDetails.setBaoBatchOutId(orderDetails.getBaoBatchOutId());
        
        return awardDetails;
    }
    
    @Override
    protected void denormalizeAwardDetails(AwardDetailsTO awardDetails) {
        intgrCelebrationDenormService.denormalizeAwardDelivery(awardDetails);
    }

    @Override
    protected Logger getLog() {
        return log;
    }
}
