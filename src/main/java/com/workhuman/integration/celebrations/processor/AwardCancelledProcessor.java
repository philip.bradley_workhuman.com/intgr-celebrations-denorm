package com.workhuman.integration.celebrations.processor;

import com.workhuman.integration.celebrations.events.AwardCancelledEvent;
import com.workhuman.integration.celebrations.service.award.AwardDetailsTO;
import com.workhuman.integration.celebrations.service.order.OrderDetailsTO;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AwardCancelledProcessor extends CelebrationEventsProcessor implements Processor {

    /*
    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <awardCancelledEvent xmlns="http://www.globoforce.com/integration/schemas/awardCancelledEvent">
        <clientId>901</clientId>
        <awardHistoryId>7699825</awardHistoryId>
        <glbcertId>9UGU-9QLDDZ-F2D2G4</glbcertId>
        <personId>67043</personId>
        <created>2018-09-13T10:33:29Z</created>
    </awardCancelledEvent>
     */
    private static final Logger log = LoggerFactory.getLogger(AwardCancelledProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        AwardCancelledEvent awardCancelledEvent = exchange.getIn().getBody(AwardCancelledEvent.class);

        long awardHistoryId = awardCancelledEvent.getAwardHistoryId();
        long clientId = awardCancelledEvent.getClientId();
        String glbcertId = awardCancelledEvent.getGlbcertId();

        log.info(String.format("Processing AwardCancelledEvent for clientId= %d; glbcertId=[ %s ]; awardHistoryId=[ %d ]",
                clientId, glbcertId, awardHistoryId));

        processEvent(clientId, glbcertId,null, awardHistoryId, null);
    }

    @Override
    protected AwardDetailsTO getAwardDetails(long clientId, Long awardHistoryId, OrderDetailsTO orderDetails) {
        return awardDetailsService.findAwardDetailsByAwardHistoryId(clientId, awardHistoryId);
    }
    
    @Override
    protected void denormalizeAwardDetails(AwardDetailsTO awardDetails) {
        intgrCelebrationDenormService.denormalizeAwardCancellation(awardDetails);
    }
    
    @Override
    protected Logger getLog() {
        return log;
    }
}
