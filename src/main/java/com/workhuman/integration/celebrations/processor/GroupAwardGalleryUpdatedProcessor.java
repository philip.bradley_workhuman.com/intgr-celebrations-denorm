package com.workhuman.integration.celebrations.processor;

import com.workhuman.integration.celebrations.events.GroupAwardGalleryUpdatedEvent;
import com.workhuman.integration.celebrations.service.celebration.IntgrCelebrationDenormVO;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Processor class for {@link GroupAwardGalleryUpdatedEvent}. It will validate if first media has been updated,
 * and if so, update it for all linked denormalized awards.
 */
@Component
public class GroupAwardGalleryUpdatedProcessor extends DefaultAwardGalleryUpdatedProcessor implements Processor {

    private static final Logger LOGGER = LoggerFactory.getLogger(AwardGalleryUpdatedProcessor.class);

    @Override
    public void process(final Exchange exchange) {
        final GroupAwardGalleryUpdatedEvent event = exchange.getIn().getBody(GroupAwardGalleryUpdatedEvent.class);

        final long userId = event.getUserId();
        final String eventMediaEntryId = event.getMediaEntryId();
        final long linkId = event.getLinkId();

        LOGGER.info(String.format("Processing GroupAwardGalleryUpdatedEvent for userId= %d; linkId= '%d'; mediaEntryId= '%s' ",
                userId, linkId, eventMediaEntryId));


        final IntgrCelebrationDenormVO denormalizedAward = celebrationAwardDenormService.findFirstByLinkId(linkId);
        if (shouldSkipProcessing(userId, eventMediaEntryId, denormalizedAward)) {
            return;
        }

        final String newFirstMediaEntryId = mediaDetailsService.findFirstMediaEntryId(denormalizedAward);
        if (areMediaEntriesEquals(userId, denormalizedAward, newFirstMediaEntryId)) {
            return;
        }

        intgrCelebrationDenormService.denormalizeGroupAwardGalleryUpdated(denormalizedAward.getClientId(), linkId, newFirstMediaEntryId);
    }
}
