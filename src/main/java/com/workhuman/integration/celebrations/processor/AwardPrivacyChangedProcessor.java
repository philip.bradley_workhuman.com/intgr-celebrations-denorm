package com.workhuman.integration.celebrations.processor;


import com.workhuman.integration.celebrations.events.AwardPrivacyChangedEvent;
import com.workhuman.integration.celebrations.service.award.AwardDetailsTO;
import com.workhuman.integration.celebrations.service.order.OrderDetailsTO;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
 * <awardPrivacyChangedEvent xmlns="http://www.globoforce.com/integration/schemas/awardPrivacyChangedEvent">
 *     <clientId>901</clientId>
 *     <awardHistoryId>7699901</awardHistoryId>
 *     <glbcertId>563R-HEFCF9-4Z8ED9</glbcertId>
 *     <awardShared>false</awardShared>
 *     <awardPrivate>true</awardPrivate>
 *     <awardRestricted>false</awardRestricted>
 *     <created>2018-08-23T13:39:18Z</created>
 * </awardPrivacyChangedEvent>
 */
@Component
public class AwardPrivacyChangedProcessor extends CelebrationEventsProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger( AwardPrivacyChangedProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        AwardPrivacyChangedEvent awardPrivacyChangedEvent = exchange.getIn().getBody(AwardPrivacyChangedEvent.class);

        Long clientId = awardPrivacyChangedEvent.getClientId();
        String glbcertId = awardPrivacyChangedEvent.getGlbcertId();
        Long awardHistoryId = awardPrivacyChangedEvent.getAwardHistoryId();

        log.info(String.format("Processing AwardPrivacyChangedEvent for clientId= %d; glbcertId=[ %s ]; awardHistoryId=[ %d ]",
                clientId, glbcertId, awardHistoryId));

        processEvent(clientId, glbcertId, null, awardHistoryId, null);
    }
    
    @Override
    protected AwardDetailsTO getAwardDetails(long clientId, Long awardHistoryId, OrderDetailsTO orderDetails) {
        return awardDetailsService.findAwardDetailsByAwardHistoryId(clientId, awardHistoryId);
    }

    @Override
    protected void denormalizeAwardDetails(AwardDetailsTO awardDetails) {
       intgrCelebrationDenormService.denormalizeAwardPrivacyChanged(awardDetails);
    }
    
    @Override
    protected Logger getLog() {
        return log;
    }
}
