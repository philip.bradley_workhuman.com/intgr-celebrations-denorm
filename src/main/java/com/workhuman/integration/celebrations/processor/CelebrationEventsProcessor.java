package com.workhuman.integration.celebrations.processor;


import com.workhuman.integration.celebrations.service.IntgrCelebrationDenormService;
import com.workhuman.integration.celebrations.service.award.AwardDetailsService;
import com.workhuman.integration.celebrations.service.award.AwardDetailsTO;
import com.workhuman.integration.celebrations.service.exception.IntgrCelebrationDenormUpdateDataException;
import com.workhuman.integration.celebrations.service.order.OrderDetailsService;
import com.workhuman.integration.celebrations.service.order.OrderDetailsTO;
import com.workhuman.integration.celebrations.service.utils.ProductIdFamily;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class CelebrationEventsProcessor {

    protected static final Long NOT_DEFINED_PRODUCT_ID = 0L;

    @Autowired
    protected IntgrCelebrationDenormService intgrCelebrationDenormService;

    @Autowired
    protected OrderDetailsService orderDetailsService;

    @Autowired
    protected AwardDetailsService awardDetailsService;
    
    protected abstract Logger getLog();

    /**
     * Gets needed product details
     *
     * @param clientId as long
     * @param glbcertId as String
     * @param indOrderId as String
     *
     * @return orderDetails as OrderDetails
     */
    protected OrderDetailsTO getProductOrders(long clientId, String glbcertId, String indOrderId) {
        OrderDetailsTO orderDetails = getOrderDetails(clientId, glbcertId, indOrderId);
        if (orderDetails == null) {
            String msg = String.format("Order Details was not found for clientId= %d; glbcertId= '%s'; indOrderId=%s", clientId, glbcertId, indOrderId);
            getLog().warn(msg);
            handleInvalidResult(msg);
        }
        return orderDetails;
    }

    /**
     * Process Event based on parameters
     * @param clientId  as Client Id
     * @param eventGlbcertId as Glbcert Id
     * @param indOrderId  as Order Id
     * @param awardHistoryId as Award History Id
     * @param eventProductId as product Id
     */
    protected void processEvent(long clientId, String eventGlbcertId, String indOrderId, Long awardHistoryId, Long eventProductId) {
        // if productId is populated in the event do initial check to skip non-celebrations awards
        if (productDoesNotBelongToCelebration(eventProductId)) {
            getLog().info(String.format("processing is skipped due to productId [%d] for clientId= %d;",
                    eventProductId, clientId));
            return;
        }
        if (StringUtils.isBlank(eventGlbcertId) && StringUtils.isBlank(indOrderId)) {
            String msg = String.format("glbcertId and indOrderId are both NULL or empty for clientId= %d;", clientId);
            getLog().warn(msg);
            throw new IntgrCelebrationDenormUpdateDataException(msg);
        }

        OrderDetailsTO orderDetails = getProductOrders(clientId, eventGlbcertId, indOrderId);
        long productId = orderDetails.getProductId();
        String glbcertId = orderDetails.getGlbcertUniqueId();

        // skip non-celebrations awards - check prod id in DB
        if (!ProductIdFamily.isCelebrationProdId(productId)) {
            getLog().info(String.format("processing is skipped due to productId [%d] for clientId= %d; glbcertId= '%s'",
                    productId, clientId, glbcertId));
            return;
        }
        // additional verification (e.g. skip old YOS awards)
        if (!verifyProductId(productId, glbcertId)) {
            getLog().info(String.format("processing is skipped due to additional verification of productId [%d] for clientId= %d;",
                    productId, clientId));
            return;
        }
        
        AwardDetailsTO awardDetails = getAwardDetails(clientId, awardHistoryId, orderDetails);
        
        if (awardDetails == null) {
            String msg = String.format("Award was not found for clientId=%d, glbcertId='%s', awardHistoryId=%d, " +
                    "productId=%d", clientId, glbcertId, awardHistoryId, productId);
            getLog().warn(msg);
            throw new IntgrCelebrationDenormUpdateDataException(msg);
        } else if (awardDetails.isCancelled()) {
            getLog().warn(String.format("Award is cancelled for clientId=%d, glbcertId='%s', " +
                    "awardHistoryId=%d, productId=%d", clientId, glbcertId, awardHistoryId, productId));
        } else if (awardDetails.isHoldOrder()) {
            getLog().warn(String.format("Award is on hold for clientId=%d, glbcertId='%s', awardHistoryId=%d, " +
                    "productId=%d", clientId, glbcertId, awardHistoryId, productId));
        } else {
            denormalizeAwardDetails(awardDetails);
        }
    }
    
    /**
     * Additional verification by productId for timelines YOS awards.
     * @param productId - product ID
     * @return true if productId is verified
     */
     protected boolean verifyProductId(Long productId, String glbcertId) {
         // Additional check for YOS awards in order to exclude not timelenes YOS awards
         // as only timelines YOS award are handled
         if (ProductIdFamily.isYOSProdId(productId) && StringUtils.isNotBlank(glbcertId)) {
            return awardDetailsService.isTimelinesYosAward(glbcertId);
        }
        return true;
    }

    /**
     * Check if product Does Not Belong To Celebrations Products
     * @param productId as Long
     * @return true if product Does Not Belong To Celebrations Products
     */
    protected boolean productDoesNotBelongToCelebration(Long productId) {
         return (productId != null && productId.longValue() != NOT_DEFINED_PRODUCT_ID && !ProductIdFamily.isCelebrationProdId(productId));
    }

    /**
     * Get Order Details
     *
     * @param clientId as long ClientId
     * @param glbcertId as String
     * @param indOrderId as String
     * @return orderDetails as OrderDetailsTO
     */
    protected OrderDetailsTO getOrderDetails(long clientId, String glbcertId, String indOrderId) {
        OrderDetailsTO orderDetails = null;
        if (StringUtils.isNotBlank(glbcertId)) {
            orderDetails = orderDetailsService.findByGlbcertId(clientId, glbcertId);
        } else if (StringUtils.isNotBlank(indOrderId)) {
            orderDetails = orderDetailsService.findByOrderId(clientId, indOrderId);
        }
        return orderDetails;
    }

    /**
     *  Handle Invalid Result - throw specified exception
     *
     * @param errorMessage as String
     */
    protected void handleInvalidResult(String errorMessage) {
        throw new IntgrCelebrationDenormUpdateDataException(errorMessage);
    }

    /**
     * Gets award details.
     * @param clientId
     * @param awardHistoryId
     * @param orderDetails
     * @return award details
     */
    protected abstract AwardDetailsTO getAwardDetails(long clientId, Long awardHistoryId, OrderDetailsTO orderDetails);

    /**
     * Denormalizes award details.
     * @param awardDetails as AwardDetailsTO
     */
    protected abstract void denormalizeAwardDetails(AwardDetailsTO awardDetails);
}
