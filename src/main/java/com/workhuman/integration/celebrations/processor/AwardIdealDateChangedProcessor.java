package com.workhuman.integration.celebrations.processor;


import com.workhuman.integration.celebrations.events.AwardIdealDateChangedEvent;
import com.workhuman.integration.celebrations.service.award.AwardDetailsTO;
import com.workhuman.integration.celebrations.service.order.OrderDetailsTO;
import com.workhuman.integration.celebrations.service.utils.ProductIdFamily;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AwardIdealDateChangedProcessor extends CelebrationEventsProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(AwardIdealDateChangedProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {

        AwardIdealDateChangedEvent awardIdealDateChangedEvent = exchange.getIn().getBody(AwardIdealDateChangedEvent.class);

        long clientId = awardIdealDateChangedEvent.getClientId();
        String glbcertId = awardIdealDateChangedEvent.getGlbcertId();
        String indOrderId = awardIdealDateChangedEvent.getIndOrderId();

        log.info(String.format("Processing AwardIdealDateChangedEvent for clientId= %d; glbcertId= '%s'; indOrderId= '%s' ",
                clientId, glbcertId,indOrderId));

        processEvent(clientId, glbcertId, indOrderId, null, null);
    }
    
    @Override
    protected boolean verifyProductId(Long productId, String glbcertId) {
        //restrict current processor only for Premium Awards, not for YOS awards because they are never processed by this processor
        return ProductIdFamily.isPremiumAwardsProdId(productId);
    }
    
    @Override
    protected AwardDetailsTO getAwardDetails(long clientId, Long awardHistoryId, OrderDetailsTO orderDetails) {
        AwardDetailsTO awardDetails = new AwardDetailsTO();

        awardDetails.setPkClient(clientId);
        awardDetails.setGlbcertUniqueId(orderDetails.getGlbcertUniqueId());
        awardDetails.setIdealDate(orderDetails.getIdealDate());
        
        return awardDetails;
    }
    
    @Override
    protected void denormalizeAwardDetails(AwardDetailsTO awardDetails) {
        intgrCelebrationDenormService.denormalizeAwardDeliveryDateChanged(awardDetails);
    }

    @Override
    protected Logger getLog() {
        return log;
    }
}
