package com.workhuman.integration.celebrations.processor;

import com.workhuman.integration.celebrations.events.PersonDataloadFinishedEvent;
import com.workhuman.integration.celebrations.service.IntgrCelebrationDenormService;
import com.workhuman.integration.celebrations.service.exception.IntgrCelebrationDenormUpdateDataException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class PersonDataloadFinishedProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(PersonDataloadFinishedProcessor.class);

    @Autowired
    protected IntgrCelebrationDenormService intgrCelebrationDenormService;
    
    @Override
    public void process(Exchange exchange) throws Exception {
        PersonDataloadFinishedEvent personDataloadFinishedEvent = exchange.getIn().getBody(PersonDataloadFinishedEvent.class);
        Long clientId = personDataloadFinishedEvent.getClientId();

        log.info(String.format("Processing PersonDataloadFinishedEvent for clientId= %d; clientName=[ %s ]", clientId, personDataloadFinishedEvent.getClientName()));

        if(clientId != null && clientId != 0L) {
            intgrCelebrationDenormService.denormalizePersonDataloadFinished(clientId);
        } else {
            String msg = String.format("PersonDataloadFinishedEvent: [clientId] is NULL or empty is detected during processing PersonDataloadFinishedEvent: %s", personDataloadFinishedEvent.toString());
            log.warn(msg);
            throw new IntgrCelebrationDenormUpdateDataException(msg);
        }
    }
}
