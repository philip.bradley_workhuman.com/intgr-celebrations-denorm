package com.workhuman.integration.celebrations.route;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.workhuman.integration.celebrations.events.*;
import com.workhuman.integration.celebrations.processor.*;
import com.workhuman.integration.celebrations.service.properties.PropertiesService;
import com.workhuman.integration.kafka.common.route.RouteConfigurator;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.workhuman.integration.kafka.common.CamelKafkaCommonConstants.HEADER_JMS_TYPE;


@Component
public class CelebrationDenormMainRoute extends RouteBuilder {
//    private static final long DEFAULT_CLIENT_ID = 999l;
//
//    private static final String INTGR_CELEBRATION_DENORM_PROP_NAME = "g_cfg_intgr_celebration_denorm_enabled";
//
//    @Value("#{intgrCelebrationDenormProperties['queue.intgr.celebration.denorm.events']}")
//    protected String intgrCelebrationDenormEvents = "INTGR.CELEBRATION.DENORM.EVENTS";
//
//    @Value("#{intgrCelebrationDenormProperties['celebration.denorm.integration.enabled']}")
//    protected boolean celebrationDenormIntegrationEnabled = true;
//
//    @Value("#{intgrCelebrationDenormProperties['errorHandling.recoverable.maximumRedeliveries']}")
//    protected int maximumRedeliveries = 3;
//
//    @Value("#{intgrCelebrationDenormProperties['errorHandling.recoverable.redeliveryDelay']}")
//    protected int redeliveryDelay = 60000;
//
//    @Value("#{intgrCelebrationDenormProperties['errorHandling.recoverable.maximumRedeliveryDelay']}")
//    protected int maximumRedeliveryDelay = 600000;
//
//    @Value("#{intgrCelebrationDenormProperties['errorHandling.recoverable.backOffMultiplier']}")
//    protected int backOffMultiplier = 2;
//
//    @Value("#{intgrCelebrationDenormProperties['errorHandling.dlq.recovery.maxMessagesCount']}")
//    protected int dlqRecoveryMaxMessagesCount = 1;
//
//    @Value("#{intgrCelebrationDenormProperties['errorHandling.dlq.recovery.period']}")
//    protected int dlqRecoveryPeriod = 120000;
//
//    @Value("#{intgrCelebrationDenormProperties['errorHandling.dlq.recovery.enabled']}")
//    protected boolean dlqRecoveryEnabled = false;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AwardCreatedProcessor awardCreatedProcessor;

    @Autowired
    private AwardCancelledProcessor awardCancelledProcessor;

    @Autowired
    private AwardPrivacyChangedProcessor awardPrivacyChangedProcessor;

    @Autowired
    private AwardIdealDateChangedProcessor awardIdealDateChangedProcessor;

    @Autowired
    private GroupAwardIdealDateChangedProcessor groupAwardIdealDateChangedProcessor;

    @Autowired
    private AwardDeliveredProcessor awardDeliveredProcessor;

    @Autowired
    private PersonDataloadFinishedProcessor personDataloadFinishedProcessor;

    @Autowired
    private AwardGalleryUpdatedProcessor awardGalleryUpdatedProcessor;

    @Autowired
    private GroupAwardGalleryUpdatedProcessor groupAwardGalleryUpdatedProcessor;

    @Autowired
    private PropertiesService propertiesService;

    @Autowired
    private RouteConfigurator routeConfigurator;

    @Autowired
    private CelebrationEventDenormConfig celebrationEventDenormConfig;


    protected static final String HEADER_NAME = "JMSType";

//    protected static final String PROPAGATION_REQUIRED = "PROPAGATION_REQUIRED";
//
//    public static final String ACTIVEMQ_QUEUE = "activemq:queue:";

    /**
     * Check whether celebrationDenormIntegration is enabled
     * @return true if celebrationDenormIntegration is enabled
     */
//    public boolean celebrationIntegrationEnabled() {
//        return propertiesService.getBooleanPropertyByName(DEFAULT_CLIENT_ID, INTGR_CELEBRATION_DENORM_PROP_NAME);
//    }

    @Override
    public void configure() throws Exception {

        getContext().setTracing(true);

        String routeBaseName = "CelebrationDenorm";
        String processEventDirect = "direct:" + routeBaseName + "ProcessEvent";

        routeConfigurator.create()
                .withSource(celebrationEventDenormConfig.getInput())
                .withRetryTopic(celebrationEventDenormConfig.getRetry())
                .withNonRecoverableExceptions(IllegalArgumentException.class)
                .withConsumerRoute()
                .withRouteBaseName(routeBaseName)
                .withDestination(processEventDirect)
                .endConsumerRoute()
                .configure(this);

        from(processEventDirect)
            .log("Got message \nHeaders: ${headers} \nBody: ${body}")
            .choice()
                .when(header(HEADER_JMS_TYPE).isEqualTo(AwardCancelledEvent.class.getSimpleName()))
                    .to("direct:sendAwardCancelledEvent")
                .when(header(HEADER_JMS_TYPE).isEqualTo(AwardCreatedEvent.class.getSimpleName()))
                    .to("direct:sendAwardCreatedEvent")
                .when(header(HEADER_JMS_TYPE).isEqualTo(AwardDeliveredEvent.class.getSimpleName()))
                    .to("direct:sendAwardDeliveredEvent")
                .when(header(HEADER_JMS_TYPE).isEqualTo(AwardGalleryUpdatedEvent.class.getSimpleName()))
                    .to("direct:sendAwardGalleryUpdatedEvent")
                .when(header(HEADER_JMS_TYPE).isEqualTo(AwardIdealDateChangedEvent.class.getSimpleName()))
                    .to("direct:sendAwardIdealDateChangedEvent")
                .when(header(HEADER_JMS_TYPE).isEqualTo(AwardPrivacyChangedEvent.class.getSimpleName()))
                    .to("direct:sendAwardPrivacyChangedEvent")
                .when(header(HEADER_JMS_TYPE).isEqualTo(GroupAwardGalleryUpdatedEvent.class.getSimpleName()))
                    .to("direct:sendGroupAwardGalleryUpdatedEvent")
                .when(header(HEADER_JMS_TYPE).isEqualTo(GroupAwardIdealDateChangedEvent.class.getSimpleName()))
                    .to("direct:sendGroupAwardIdealDateChangedEvent")
                .when(header(HEADER_JMS_TYPE).isEqualTo(PersonDataloadFinishedEvent.class.getSimpleName()))
                    .to("direct:sendPersonDataloadFinishedEvent")
                 .otherwise()
                    .to("direct:unhandled")
            .end();

        from("direct:sendAwardCancelledEvent")
                .routeId("AwardCancelledRoute")
                .unmarshal(new JacksonDataFormat(objectMapper, AwardCancelledEvent.class))
                .process(awardCancelledProcessor);

        from("direct:sendAwardCreatedEvent")
                .routeId("AwardCreatedRoute")
                .unmarshal(new JacksonDataFormat(objectMapper, AwardCreatedEvent.class))
                .process(awardCreatedProcessor);

        from("direct:sendAwardDeliveredEvent")
                .routeId("AwardDeliveredRoute")
                .unmarshal(new JacksonDataFormat(objectMapper, AwardDeliveredEvent.class))
                .process(awardDeliveredProcessor);

        from("direct:sendAwardGalleryUpdatedEvent")
                .routeId("AwardGalleryUpdatedRoute")
                .unmarshal(new JacksonDataFormat(objectMapper, AwardGalleryUpdatedEvent.class))
                .process(awardGalleryUpdatedProcessor);

        from("direct:sendAwardIdealDateChangedEvent")
                .routeId("AwardIdealDateChangedRoute")
                .unmarshal(new JacksonDataFormat(objectMapper, AwardIdealDateChangedEvent.class))
                .process(awardIdealDateChangedProcessor);

        from("direct:sendAwardIdealDateChangedEvent")
                .routeId("AwardIdealDateChangedRoute")
                .unmarshal(new JacksonDataFormat(objectMapper, AwardIdealDateChangedEvent.class))
                .process(awardIdealDateChangedProcessor);

        from("direct:sendAwardPrivacyChangedEvent")
                .routeId("AwardPrivacyChangedRoute")
                .unmarshal(new JacksonDataFormat(objectMapper, AwardPrivacyChangedEvent.class))
                .process(awardPrivacyChangedProcessor);

        from("direct:sendGroupAwardGalleryUpdatedEvent")
                .routeId("GroupAwardGalleryUpdatedRoute")
                .unmarshal(new JacksonDataFormat(objectMapper, GroupAwardGalleryUpdatedEvent.class))
                .process(groupAwardGalleryUpdatedProcessor);

        from("direct:sendGroupAwardIdealDateChangedEvent")
                .routeId("GroupAwardIdealDateChangedRoute")
                .unmarshal(new JacksonDataFormat(objectMapper, GroupAwardIdealDateChangedEvent.class))
                .process(groupAwardIdealDateChangedProcessor);

        from("direct:sendPersonDataloadFinishedEvent")
                .routeId("PersonDataloadFinishedRoute")
                .unmarshal(new JacksonDataFormat(objectMapper, PersonDataloadFinishedEvent.class))
                .process(personDataloadFinishedProcessor);
}

}
