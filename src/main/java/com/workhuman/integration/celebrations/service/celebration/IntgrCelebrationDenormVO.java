package com.workhuman.integration.celebrations.service.celebration;

import java.sql.Timestamp;
import java.util.Date;

public class IntgrCelebrationDenormVO {

    //PK_CELEBRATION
    private Long id;
    // FK_CLIENT
    private Long clientId;
    // FK_AWARD_HISTORY
    private Long awardHistoryId;
    // GLBCERT_UNIQUE_ID
    private String glbcertUniqueId;
    // FK_RECIPIENT
    private Long recipientId;
    // RECIPIENT_FIRSTNAME
    private String recipientFirstname;
    // RECIPIENT_FIRSTNAME
    private String recipientLastname;
    // RECIPIENT_HAS_NEWSFEED
    private boolean recipientHasNewsfeedPrivilege;
    // RECIPIENT_EXPIRED
    private Timestamp recipientExpired;
    // FK_AWARD_TYPE
    private Long awardTypeId;
    //AWARD_REASON_CODE
    private String awardReasonCode;
    // DELIVERY_DATE
    private Date deliveryDate;
    // LINK_ID
    private Long linkId;
    // IS_TIMELINES
    private boolean timelines;
    // YOS_YEARS
    private Integer yosYears;
    // AWARD_PRIVATE
    private boolean awardPrivate;
    //BAO_BATCH_OUT_ID
    private Integer baoBatchOutId;
    //MEDIA_ENTRY_ID
    private String mediaEntryId;
    //MEDIA_TYPE
    private Integer mediaType;
    // CREATED
    private Date created;
    // MODIFIED
    private Date modified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getAwardHistoryId() {
        return awardHistoryId;
    }

    public void setAwardHistoryId(Long awardHistoryId) {
        this.awardHistoryId = awardHistoryId;
    }

    public String getGlbcertUniqueId() {
        return glbcertUniqueId;
    }

    public void setGlbcertUniqueId(String glbcertUniqueId) {
        this.glbcertUniqueId = glbcertUniqueId;
    }

    public Long getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(Long recipientId) {
        this.recipientId = recipientId;
    }

    public String getRecipientFirstname() {
        return recipientFirstname;
    }

    public void setRecipientFirstname(String recipientFirstname) {
        this.recipientFirstname = recipientFirstname;
    }

    public String getRecipientLastname() {
        return recipientLastname;
    }

    public void setRecipientLastname(String recipientLastname) {
        this.recipientLastname = recipientLastname;
    }

    public boolean isRecipientHasNewsfeedPrivilege() {
        return recipientHasNewsfeedPrivilege;
    }

    public void setRecipientHasNewsfeedPrivilege(boolean recipientHasNewsfeedPrivilege) {
        this.recipientHasNewsfeedPrivilege = recipientHasNewsfeedPrivilege;
    }

    public Timestamp getRecipientExpired() {
        return recipientExpired;
    }

    public void setRecipientExpired(Timestamp recipientExpired) {
        this.recipientExpired = recipientExpired;
    }

    public Long getAwardTypeId() {
        return awardTypeId;
    }

    public void setAwardTypeId(Long awardTypeId) {
        this.awardTypeId = awardTypeId;
    }

    public String getAwardReasonCode() {
        return awardReasonCode;
    }

    public void setAwardReasonCode(String awardReasonCode) {
        this.awardReasonCode = awardReasonCode;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Long getLinkId() {
        return linkId;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }

    public boolean isTimelines() {
        return timelines;
    }

    public void setTimelines(boolean timelines) {
        this.timelines = timelines;
    }

    public Integer getYosYears() {
        return yosYears;
    }

    public void setYosYears(Integer yosYears) {
        this.yosYears = yosYears;
    }

    public boolean isAwardPrivate() {
        return awardPrivate;
    }

    public void setAwardPrivate(boolean awardPrivate) {
        this.awardPrivate = awardPrivate;
    }

    public Integer getBaoBatchOutId() {
        return baoBatchOutId;
    }

    public void setBaoBatchOutId(Integer baoBatchOutId) {
        this.baoBatchOutId = baoBatchOutId;
    }

    public String getMediaEntryId() {
        return mediaEntryId;
    }

    public void setMediaEntryId(String mediaEntryId) {
        this.mediaEntryId = mediaEntryId;
    }

    public Integer getMediaType() {
        return mediaType;
    }

    public void setMediaType(Integer mediaType) {
        this.mediaType = mediaType;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    @Override
    public String toString() {
        return "IntgrCelebrationDenormVO{" +
                "id=" + id +
                ", clientId=" + clientId +
                ", awardHistoryId=" + awardHistoryId +
                ", glbcertUniqueId='" + glbcertUniqueId + '\'' +
                ", recipientId=" + recipientId +
                ", recipientFirstname='" + recipientFirstname + '\'' +
                ", recipientLastname='" + recipientLastname + '\'' +
                ", recipientHasNewsfeedPrivilege=" + recipientHasNewsfeedPrivilege +
                ", recipientExpired=" + recipientExpired +
                ", awardTypeId=" + awardTypeId +
                ", awardReasonCode='" + awardReasonCode + '\'' +
                ", deliveryDate=" + deliveryDate +
                ", linkId=" + linkId +
                ", timelines=" + timelines +
                ", yosYears=" + yosYears +
                ", awardPrivate=" + awardPrivate +
                ", baoBatchOutId=" + baoBatchOutId +
                ", mediaEntryId=" + mediaEntryId +
                ", mediaType=" + mediaType +
                ", created=" + created +
                ", modified=" + modified +
                '}';
    }
}
