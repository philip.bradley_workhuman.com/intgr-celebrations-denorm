package com.workhuman.integration.celebrations.service.exception;

/**
 *  Class IntgrCelebrationDenormServiceException handles all exceptions
 *  which should be redirected into DLQ queue (such as DB connection issues etc)
 *  and such events will have possibility to process again
 */
public class IntgrCelebrationDenormServiceException extends RuntimeException {
    public IntgrCelebrationDenormServiceException(String message) {
        super(message);
    }

    public IntgrCelebrationDenormServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
