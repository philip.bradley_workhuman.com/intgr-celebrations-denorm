package com.workhuman.integration.celebrations.service.properties;

import java.util.List;

public interface PropertiesService {

    List<PropertyTO> getPropertiesByTag(long var1, String var3);

    PropertyTO getPropertyByTagAndName(long var1, String var3, String var4);

    PropertyTO getPropertyByName(long var1, String var3);

    long getLongPropertyByName(long var1, String var3);

    boolean getBooleanPropertyByName(long var1, String var3);
    String getStringPropertyByName(long var1, String var3);

    String getStringLargePropertyByName(long var1, String var3);
}
