package com.workhuman.integration.celebrations.service.celebration;


import com.workhuman.integration.celebrations.service.exception.IntgrCelebrationDenormDuplicationException;
import com.workhuman.integration.celebrations.service.utils.DAOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.workhuman.integration.celebrations.service.utils.DAOUtils.CLIENT_ID_PLACEHOLDER;


/**
 * DAO which updates rec_celebrations_denorm
 */
@Repository
class CelebrationAwardDenormDaoImpl implements CelebrationAwardDenormDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(CelebrationAwardDenormDaoImpl.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String AWARD_HISTORY_ID = "awardHistoryId";

    private static final String LINK_ID = "linkId";

    private static final String CLIENT_ID = "clientId";

    private static final String MEDIA_ENTRY_ID = "mediaEntryId";

    private static final String MODIFIED = "modified";

    private static final String SQL_CELEBRATION_DENORM_SEQUENCE_NEXT_VALUE = "select REC_CELEBRATIONS_DENORM_SEQ.NEXTVAL from dual";

    private static final String SQL_INSERT_CELEBRATION_DENORM = "" +
            "insert into REC_CELEBRATIONS_DENORM ( \n" +
            "PK_CELEBRATION, \n" +
            "FK_CLIENT, \n" +
            "GLBCERT_UNIQUE_ID, \n" +
            "FK_AWARD_HISTORY, \n" +
            "FK_AWARD_TYPE, \n" +
            "LINK_ID, \n" +
            "AWARD_REASON_CODE, \n" +
            "AWARD_PRIVATE, \n" +
            "DELIVERY_DATE, \n" +
            "BAO_BATCH_OUT_ID, \n" +
            "FK_RECIPIENT, \n" +
            "RECIPIENT_FIRSTNAME, \n" +
            "RECIPIENT_LASTNAME, \n" +
            "RECIPIENT_HAS_NEWSFEED, \n" +
            "RECIPIENT_EXPIRED, \n" +
            "IS_TIMELINES, \n" +
            "YOS_YEARS, \n" +
            "MEDIA_ENTRY_ID, \n" +
            "MEDIA_TYPE, \n" +
            "CREATED, \n" +
            "MODIFIED \n" +
            ") \n" +
            "VALUES( \n" +
            " :id, \n" +
            " :clientId,  \n" +
            " :glbcertUniqueId, \n" +
            " :awardHistoryId, \n" +
            " :awardTypeId, \n" +
            " :linkId, \n" +
            " :awardReasonCode, \n" +
            " :awardPrivate, \n" +
            " :deliveryDate, \n" +
            " :baoBatchOutId, \n" +
            " :recipientId, \n" +
            " :recipientFirstname, \n" +
            " :recipientLastname, \n" +
            " :recipientHasNewsfeedPrivilege, \n" +
            " :recipientExpired, \n" +
            " :timelines, \n" +
            " :yosYears, \n" +
            " :mediaEntryId, \n" +
            " :mediaType, \n" +
            " :created,\n" +
            " :modified\n" +
            ")";

    private static final String SQL_SELECT_CELEBRATION_DENORM_BASE = "SELECT " +
            "PK_CELEBRATION as id, \n" +
            "FK_CLIENT as clientId, \n" +
            "GLBCERT_UNIQUE_ID as glbcertUniqueId, \n" +
            "FK_AWARD_HISTORY as awardHistoryId, \n" +
            "FK_AWARD_TYPE as awardTypeId, \n" +
            "LINK_ID as linkId, \n" +
            "AWARD_REASON_CODE as awardReasonCode, \n" +
            "AWARD_PRIVATE as awardPrivate, \n" +
            "DELIVERY_DATE as deliveryDate, \n" +
            "BAO_BATCH_OUT_ID as baoBatchOutId, \n" +
            "FK_RECIPIENT as recipientId, \n" +
            "RECIPIENT_FIRSTNAME as recipientFirstname, \n" +
            "RECIPIENT_LASTNAME as recipientLastname, \n" +
            "RECIPIENT_HAS_NEWSFEED as recipientHasNewsfeedPrivilege, \n" +
            "RECIPIENT_EXPIRED as recipientExpired, \n" +
            "IS_TIMELINES as timelines, \n" +
            "YOS_YEARS as yosYears, \n" +
            "MEDIA_ENTRY_ID as mediaEntryId, \n" +
            "MEDIA_TYPE as mediaType, \n" +
            "CREATED as created, \n" +
            "MODIFIED as modified \n" +
            " FROM REC_CELEBRATIONS_DENORM " +
            " WHERE ";

    private static final String SQL_SELECT_CELEBRATION_DENORM_BY_AWARD_HISTORY_ID =
            SQL_SELECT_CELEBRATION_DENORM_BASE + " FK_AWARD_HISTORY=:awardHistoryId";

    private static final String CLAUSE_LINK_ID = " LINK_ID = :linkId";

    private static final String SQL_SELECT_CELEBRATION_DENORM_BY_LINK_ID =
            SQL_SELECT_CELEBRATION_DENORM_BASE + CLAUSE_LINK_ID;

    private static final String SQL_SELECT_FIRST_CELEBRATION_DENORM_BY_LINK_ID =
            SQL_SELECT_CELEBRATION_DENORM_BY_LINK_ID + " fetch first row only";

    private static final String SQL_UPDATE_REC_CELEBRATIONS_DENORM = "UPDATE REC_CELEBRATIONS_DENORM SET %s, \n" +
            "MODIFIED = :modified WHERE FK_CLIENT=" + CLIENT_ID_PLACEHOLDER + " AND %s ";

    private static final String SET_AWARD_PRIVATE = "AWARD_PRIVATE = :awardPrivate";
    private static final String SET_DELIVERY_DATE = "DELIVERY_DATE = :deliveryDate";
    private static final String SET_BAO_BATCH_OUT_ID = "BAO_BATCH_OUT_ID = :baoBatchOutId";
    private static final String SET_MEDIA_ENTRY_ID = "MEDIA_ENTRY_ID = :mediaEntryId, " +
            "MEDIA_TYPE = (SELECT DISTINCT MEDIA_TYPE FROM gg_media WHERE ENTRY_ID = :mediaEntryId)";

    private static final String FK_AWARD_HISTORY = "FK_AWARD_HISTORY=:awardHistoryId";
    private static final String GLBCERT_UNIQUE_ID = "GLBCERT_UNIQUE_ID = TO_CHAR(:glbcertUniqueId)";

    private static final String SQL_DELETE_CELEBRATION_DENORM = "DELETE FROM REC_CELEBRATIONS_DENORM " +
            "WHERE FK_CLIENT=" + CLIENT_ID_PLACEHOLDER + " and FK_AWARD_HISTORY=:awardHistoryId";

    private static final String SQL_UPDATE_RECIPIENTS_BY_CLIENT_ID = "UPDATE rec_celebrations_denorm d " +
            " SET (d.recipient_firstname, d.recipient_lastname, d.recipient_has_newsfeed, d.recipient_expired, d.modified) = (" +
            "    SELECT p.firstname, p.lastname, sign(bitand(privilege, 4194304)), p.expired, sysdate \n" +
            "    FROM gg_ms_person p \n" +
            "    WHERE p.pk_client = " + CLIENT_ID_PLACEHOLDER + "\n" +
            "    AND d.fk_recipient = p.pk_person \n" +
            ") \n" +
            "WHERE d.fk_client = " + CLIENT_ID_PLACEHOLDER;

    private static final String SQL_UPDATE_GROUP_AWARD_DELIVERY_DATE_BY_LINK_ID = "UPDATE rec_celebrations_denorm rcd " +
            " SET rcd.modified = :modified, rcd.delivery_date = (\n" +
            "    SELECT i.ideal_date FROM gg_inbox i, gg_pending_orders po\n" +
            "    WHERE po.glbcert_unique_id = i.glbcert_unique_id\n" +
            "    AND i.glbcert_unique_id = rcd.glbcert_unique_id\n" +
            "    AND po.link_id= :linkId)\n" +
            " WHERE rcd.fk_client= " + CLIENT_ID_PLACEHOLDER + " AND rcd.link_id= :linkId";

    @Autowired
    public CelebrationAwardDenormDaoImpl(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public Long sequenceNextVal() {
        return jdbcTemplate.getJdbcOperations().queryForObject(SQL_CELEBRATION_DENORM_SEQUENCE_NEXT_VALUE, Long.class);
    }

    @Override
    public IntgrCelebrationDenormVO findByAwardHistoryId(long awardHistoryId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource(AWARD_HISTORY_ID, awardHistoryId);
        List<IntgrCelebrationDenormVO> celebrationAwardDenormTOS = jdbcTemplate.query(SQL_SELECT_CELEBRATION_DENORM_BY_AWARD_HISTORY_ID, parameters, new BeanPropertyRowMapper<IntgrCelebrationDenormVO>(IntgrCelebrationDenormVO.class));
        return DAOUtils.getSingleObject(celebrationAwardDenormTOS, LOGGER, AWARD_HISTORY_ID, Long.toString(awardHistoryId));
    }

    @Override
    public List<IntgrCelebrationDenormVO> findByLinkId(final long linkId) {
        final MapSqlParameterSource parameters = new MapSqlParameterSource(LINK_ID, linkId);
        return jdbcTemplate.query(SQL_SELECT_CELEBRATION_DENORM_BY_LINK_ID, parameters,
                new BeanPropertyRowMapper<>(IntgrCelebrationDenormVO.class));
    }

    @Override
    public IntgrCelebrationDenormVO findFirstByLinkId(final long linkId) {
        final MapSqlParameterSource parameters = new MapSqlParameterSource(LINK_ID, linkId);
        return jdbcTemplate.queryForObject(SQL_SELECT_FIRST_CELEBRATION_DENORM_BY_LINK_ID, parameters,
                new BeanPropertyRowMapper<>(IntgrCelebrationDenormVO.class));
    }

    @Override
    public int add(IntgrCelebrationDenormVO celebrationDenormVO) {
        Long id = sequenceNextVal();
        celebrationDenormVO.setId(id);
        Date created = new Date();
        celebrationDenormVO.setCreated(created);
        celebrationDenormVO.setModified(created);

        try {
            return updateWithBeanProperty(SQL_INSERT_CELEBRATION_DENORM, celebrationDenormVO);
        } catch (DuplicateKeyException e) {
            throw new IntgrCelebrationDenormDuplicationException(e);
        }
    }

    @Override
    public int updatePrivacyLevel(IntgrCelebrationDenormVO celebrationDenormVO) {
        setModified(celebrationDenormVO);
        String replacedSql = SQL_UPDATE_REC_CELEBRATIONS_DENORM
                .replace(CLIENT_ID_PLACEHOLDER, celebrationDenormVO.getClientId().toString());
        String sql = String.format(replacedSql, SET_AWARD_PRIVATE, FK_AWARD_HISTORY);
        return updateWithBeanProperty(sql, celebrationDenormVO);
    }

    @Override
    public int updateDeliveryDate(IntgrCelebrationDenormVO celebrationDenormVO) {
        setModified(celebrationDenormVO);
        String replacedSql = SQL_UPDATE_REC_CELEBRATIONS_DENORM
                .replace(CLIENT_ID_PLACEHOLDER, celebrationDenormVO.getClientId().toString());
        String sql = String.format(replacedSql, SET_DELIVERY_DATE, GLBCERT_UNIQUE_ID);
        return updateWithBeanProperty(sql, celebrationDenormVO);
    }

    @Override
    public int updateGroupAwardDeliveryDate(long clientId, long linkId) {
        Date modified = new Date();
        MapSqlParameterSource queryParameterSource = new MapSqlParameterSource();
        queryParameterSource.addValue("clientId", clientId);
        queryParameterSource.addValue("linkId", linkId);
        queryParameterSource.addValue("modified", modified);

        String replacedSql = SQL_UPDATE_GROUP_AWARD_DELIVERY_DATE_BY_LINK_ID
                .replace(CLIENT_ID_PLACEHOLDER, Long.toString(clientId));
        return jdbcTemplate.update(replacedSql, queryParameterSource);
    }

    @Override
    public int updateBaoBatchOutId(IntgrCelebrationDenormVO celebrationDenormVO) {
        setModified(celebrationDenormVO);
        String replacedSql = SQL_UPDATE_REC_CELEBRATIONS_DENORM
                .replace(CLIENT_ID_PLACEHOLDER, celebrationDenormVO.getClientId().toString());
        String sql = String.format(replacedSql, SET_BAO_BATCH_OUT_ID, GLBCERT_UNIQUE_ID);
        return updateWithBeanProperty(sql, celebrationDenormVO);
    }

    @Override
    public int updateSingleAwardMediaEntryId(final long clientId, final long awardHistoryId, final String mediaEntryId) {
        final Map<String, Object> params = new HashMap<>(4);
        params.put(CLIENT_ID, clientId);
        params.put(MODIFIED, new Date());
        params.put(AWARD_HISTORY_ID, awardHistoryId);
        params.put(MEDIA_ENTRY_ID, mediaEntryId);

        String replacedSql = SQL_UPDATE_REC_CELEBRATIONS_DENORM.replace(CLIENT_ID_PLACEHOLDER, Long.toString(clientId));

        final String sql = String.format(replacedSql, SET_MEDIA_ENTRY_ID, FK_AWARD_HISTORY);
        return jdbcTemplate.update(sql, params);
    }

    @Override
    public int updateGroupAwardMediaEntryId(final long clientId, final long linkId, final String mediaEntryId) {
        final Map<String, Object> params = new HashMap<>(4);
        params.put(CLIENT_ID, clientId);
        params.put(MODIFIED, new Date());
        params.put(LINK_ID, linkId);
        params.put(MEDIA_ENTRY_ID, mediaEntryId);

        String replacedSql = SQL_UPDATE_REC_CELEBRATIONS_DENORM.replace(CLIENT_ID_PLACEHOLDER, Long.toString(clientId));
        final String sql = String.format(replacedSql, SET_MEDIA_ENTRY_ID, CLAUSE_LINK_ID);
        return jdbcTemplate.update(sql, params);
    }

    @Override
    public int updateRecipientsByClientId(long clientId) {
        MapSqlParameterSource queryParameterSource = new MapSqlParameterSource();
        queryParameterSource.addValue("clientId", clientId);

        String replacedSql = SQL_UPDATE_RECIPIENTS_BY_CLIENT_ID
                .replaceAll(CLIENT_ID_PLACEHOLDER, Long.toString(clientId));
        return jdbcTemplate.update(replacedSql, queryParameterSource);
    }

    @Override
    public int delete(IntgrCelebrationDenormVO celebrationDenormVO) {
        String replacedSql = SQL_DELETE_CELEBRATION_DENORM
                .replace(CLIENT_ID_PLACEHOLDER, celebrationDenormVO.getClientId().toString());
        return updateWithBeanProperty(replacedSql, celebrationDenormVO);
    }

    private int updateWithBeanProperty(String sql, IntgrCelebrationDenormVO celebrationDenormVO) {
        return jdbcTemplate.update(sql, new BeanPropertySqlParameterSource(celebrationDenormVO));
    }

    private void setModified(IntgrCelebrationDenormVO celebrationDenormVO) {
        Date modified = new Date();
        celebrationDenormVO.setModified(modified);
    }
}
