package com.workhuman.integration.celebrations.service.media;


import com.workhuman.integration.celebrations.service.celebration.IntgrCelebrationDenormVO;

/**
 * Validator class containing validation methods for media entries and {@link IntgrCelebrationDenormVO} media
 * used by Award Gallery event's processors.
 */
public interface MediaValidator {

    /**
     * Check if Media Entry does not belong to Celebrations Media
     * (record exists in gg_media but does not exist in gg_media_award_gallery
     * and was not loaded during nomination process)
     *
     * @param eventMediaEntryId as the media entry id
     * @return true if Media Entry DOES NOT belong to Celebrations Media
     */
    boolean mediaEntryDoesNotBelongToAwardGallery(final String eventMediaEntryId);

    /**
     * Checks if mediaEntryId is the first media (denormalized award has no media and they're not the same)
     * when media details does not exist (has been deleted).
     *
     * @param mediaEntryId      the media entry id to be checked
     * @param denormalizedAward the existing denormalized award record.
     * @return {@code true} if it's the first award media
     */
    boolean isRemovedMediaNotTheFirstAwardMedia(final String mediaEntryId, final IntgrCelebrationDenormVO denormalizedAward);

    /**
     * Check if mediaEntryId is same as media entry id defined in the denorm award object.
     *
     * @param mediaEntryId      as the media entry id
     * @param denormalizedAward as the award denorm object
     * @return true both media entry ids are equals.
     */
    boolean areEquals(final String mediaEntryId, final IntgrCelebrationDenormVO denormalizedAward);

}
