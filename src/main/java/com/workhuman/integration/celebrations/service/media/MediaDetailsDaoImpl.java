package com.workhuman.integration.celebrations.service.media;


import com.workhuman.integration.celebrations.service.utils.DAOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class MediaDetailsDaoImpl implements MediaDetailsDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(MediaDetailsDaoImpl.class);

    private final NamedParameterJdbcTemplate jdbcTemplate;

    private static final String MEDIA_ENTRY_ID = "mediaEntryId";

    private static final String PK_AWARD_HISTORY = "pkAwardHistory";

    private static final String LINK_ID = "linkId";

    private static final String SQL_SELECT_MEDIA_DETAILS = """
            SELECT 
                m.pk_media AS pkMedia,
                p.pk_client AS clientId, 
                m.creator_id AS creatorId, 
                m.entry_id AS entryId, 
                m.group_id AS groupId, 
                mg.fk_award_history AS fkAwardHistory, 
                mg.link_id AS linkId, 
                NVL(mg.sort_order, 0) AS sortOrder 
            FROM gg_ms_person p, gg_media m
            LEFT JOIN gg_media_award_gallery mg ON m.pk_media = mg.fk_media
            WHERE m.creator_id= p.pk_person 
            AND entry_id = :mediaEntryId
                   """;

    private static final String SQL_SELECT_NOMINATION_MEDIA_ENTRY_ID = """
                    SELECT 
                        entry_id 
                    FROM gg_pending_orders po 
                    JOIN gg_media m ON po.groupid = m.group_id 
                    JOIN gg_award_history ah ON ah.glbcert_id = po.glbcert_unique_id 
                    WHERE ah.pk_award_history = :pkAwardHistory
            """;

    private static final String SQL_SELECT_FIRST_MEDIA_GALLERY_ENTRY_ID = """
                        SELECT 
                            m.entry_id as entry_id 
                        FROM gg_media_award_gallery mag 
                        JOIN gg_award_history ah ON ah.pk_award_history = mag.fk_award_history
                        JOIN gg_media m ON m.pk_media = mag.fk_media 
                        WHERE ah.pk_award_history = :pkAwardHistory ORDER BY sort_order ASC FETCH FIRST ROW ONLY
            """;

    private static final String SQL_SELECT_FIRST_MEDIA_GALLERY_ENTRY_ID_BY_LINK_ID = """
                        SELECT 
                            m.entry_id as entry_id 
                        FROM gg_media_award_gallery mag 
                        JOIN gg_media m ON m.pk_media = mag.fk_media 
                        WHERE mag.link_id = :linkId 
                        ORDER BY sort_order ASC 
                        FETCH FIRST ROW ONLY
            """;

    @Autowired
    public MediaDetailsDaoImpl(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public String findNominationMediaEntryId(final long pkAwardHistory) {
        final MapSqlParameterSource parameters = new MapSqlParameterSource(PK_AWARD_HISTORY, pkAwardHistory);
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_NOMINATION_MEDIA_ENTRY_ID, parameters, String.class);
        } catch (EmptyResultDataAccessException er) {
            LOGGER.info(String.format("Nomination mediaEntryId not found for pkAwardHistory [%d]", pkAwardHistory));
            return null;
        }
    }

    @Override
    public String findFirstMediaGalleryEntryId(long pkAwardHistory) {
        final MapSqlParameterSource parameters = new MapSqlParameterSource(PK_AWARD_HISTORY, pkAwardHistory);
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_FIRST_MEDIA_GALLERY_ENTRY_ID, parameters, String.class);
        } catch (EmptyResultDataAccessException er) {
            LOGGER.info(String.format("Media Gallery  mediaEntryId id not found for pkAwardHistory [%d]", pkAwardHistory));
            return null;
        }
    }

    @Override
    public String findFirstMediaGalleryEntryIdByLinkId(final long linkId) {
        final MapSqlParameterSource parameters = new MapSqlParameterSource(LINK_ID, linkId);
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_FIRST_MEDIA_GALLERY_ENTRY_ID_BY_LINK_ID, parameters, String.class);
        } catch (EmptyResultDataAccessException er) {
            LOGGER.info(String.format("Media Gallery  mediaEntryId id not found for linkId [%d]", linkId));
            return null;
        }
    }

    @Override
    public MediaDetailsTO getMediaDetails(String mediaEntryId) {
        final MapSqlParameterSource parameters = new MapSqlParameterSource(MEDIA_ENTRY_ID, mediaEntryId);

        final List<MediaDetailsTO> items = jdbcTemplate.query(SQL_SELECT_MEDIA_DETAILS, parameters, new BeanPropertyRowMapper<MediaDetailsTO>(MediaDetailsTO.class));
        return DAOUtils.getSingleObject(items, LOGGER, MEDIA_ENTRY_ID, mediaEntryId);
    }
}
