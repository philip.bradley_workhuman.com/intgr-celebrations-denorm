package com.workhuman.integration.celebrations.service.award;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AwardDetailsServiceImpl implements AwardDetailsService {

    private static final Logger log = LoggerFactory.getLogger(AwardDetailsServiceImpl.class);

    @Autowired
    private AwardDetailsDao awardDetailsDao;

    @Override
    public AwardDetailsTO findLifeEventAward(long clientId, String glbcertId) {
        log.info("Start to search Life Event Award Details for glbcertId=" + glbcertId);
        AwardDetailsTO awardDetailsTO = awardDetailsDao.findLifeEventAward(clientId, glbcertId);
        if (awardDetailsTO != null) {
            log.info("Found Life Event Award Details for glbcertId=" + glbcertId);
        }
        return awardDetailsTO;
    }

    @Override
    public AwardDetailsTO findServiceMilestonesAward(long clientId, String glbcertId) {
        log.info("Find Service Milestones Award Details for glbcertId=" + glbcertId);
        AwardDetailsTO awardDetailsTO = awardDetailsDao.findServiceMilestonesAward(clientId, glbcertId);
        if (awardDetailsTO != null) {
            log.info("Found Service Milestones Award Details for glbcertId=" + glbcertId);
        }
        return awardDetailsTO;
    }

    @Override
    public AwardDetailsTO findAwardDetailsByAwardHistoryId(long clientId, Long pkAwardHistory) {
        log.info("Find award history for awardHistoryId=" + pkAwardHistory);
        AwardDetailsTO awardDetails = null;

        AwardHistoryTO awardHistoryTO = awardDetailsDao.findAwardHistoryByAwardHistoryId(clientId, pkAwardHistory);
        if (awardHistoryTO != null) {
            log.info("Found award history for awardHistoryId=" + pkAwardHistory);

            awardDetails = new AwardDetailsTO();
            awardDetails.setGlbcertUniqueId(awardHistoryTO.getGlbcertId());
            awardDetails.setPkClient(clientId);
            awardDetails.setPkAwardHistory(awardHistoryTO.getPkAwardHistory());
            awardDetails.setAwardPrivate(awardHistoryTO.isAwardPrivate());
            awardDetails.setLinkId(awardHistoryTO.getLinkId());
            awardDetails.setPkRecipient(awardHistoryTO.getPkAwardee());
            awardDetails.setReason(awardHistoryTO.getReason());
        } else {
            log.info("Not found award history for awardHistoryId=" + pkAwardHistory);
        }
        return awardDetails;
    }

    @Override
    public boolean isTimelinesYosAward(String glbcertId) {
        log.info("Check if award is timelines for glbcertId=" + glbcertId);
        return awardDetailsDao.isTimelinesYosAward(glbcertId);
    }
}
