package com.workhuman.integration.celebrations.service.utils;

public class StoreCodeUtil {
    private StoreCodeUtil() {
    }

    /**
     * Provides Store Code based on clientId
     * @param clientId
     * @return  client StoreCode
     */
    public static String getStoreCode(long clientId) {
        return String.format("INTCOR%04d", clientId);
    }
}
