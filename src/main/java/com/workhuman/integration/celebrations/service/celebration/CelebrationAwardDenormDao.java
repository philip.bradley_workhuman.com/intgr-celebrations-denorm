package com.workhuman.integration.celebrations.service.celebration;

import java.util.List;

interface CelebrationAwardDenormDao {
    Long sequenceNextVal();

    /**
     * Find an object of IntgrCelebrationDenormVO by AwardHistoryId
     *
     * @param awardHistoryId as String
     * @return object of IntgrCelebrationDenormVO
     */
    IntgrCelebrationDenormVO findByAwardHistoryId(long awardHistoryId);

    /**
     * Find list of objects of IntgrCelebrationDenormVO by linkId
     *
     * @param linkId as String
     * @return list of object of IntgrCelebrationDenormVO
     */
    List<IntgrCelebrationDenormVO> findByLinkId(final long linkId);

    /**
     * Find first (by id) IntgrCelebrationDenormVO object by linkId
     *
     * @param linkId as String
     * @return first object (by id) of IntgrCelebrationDenormVO
     */
    IntgrCelebrationDenormVO findFirstByLinkId(final long linkId);

    /**
     * Add an object of IntgrCelebrationDenormVO
     *
     * @param celebrationDenormVO as IntgrCelebrationDenormVO
     * @return rows affected
     */
    int add(IntgrCelebrationDenormVO celebrationDenormVO);

    /**
     * Delete an object of IntgrCelebrationDenormVO
     *
     * @param celebrationAwardDenormTO as IntgrCelebrationDenormVO
     * @return rows affected
     */
    int delete(IntgrCelebrationDenormVO celebrationAwardDenormTO);

    /**
     * Update Privacy Level
     *
     * @param celebrationAwardDenormTO as IntgrCelebrationDenormVO
     * @return rows affected
     */
    int updatePrivacyLevel(IntgrCelebrationDenormVO celebrationAwardDenormTO);

    /**
     * Update Delivery Date
     *
     * @param celebrationAwardDenormTO as IntgrCelebrationDenormVO
     * @return rows affected
     */
    int updateDeliveryDate(IntgrCelebrationDenormVO celebrationAwardDenormTO);

    /**
     * Update Delivery Date for Group Award
     * with GG_INBOX.IDEAL_DATE value
     * for specified clientId and linkId
     *
     * @param clientId Client Id
     * @param linkId Link Id of Group Award
     * @return rows affected
     */
    int updateGroupAwardDeliveryDate(long clientId, long linkId);

    /**
     * Update BaoBatchOutId
     *
     * @param celebrationAwardDenormTO as IntgrCelebrationDenormVO
     * @return rows affected
     */
    int updateBaoBatchOutId(IntgrCelebrationDenormVO celebrationAwardDenormTO);

    /**
     * Update media entry id for one single award.
     *
     * @param clientId       the client id
     * @param awardHistoryId the award history id
     * @param mediaEntryId   the media entry id
     * @return rows affected
     */
    int updateSingleAwardMediaEntryId(final long clientId, final long awardHistoryId, final String mediaEntryId);

    /**
     * Update media entry id for group awards (all awards sharing same link id).
     *
     * @param clientId     the client id
     * @param linkId       the group (link) award id
     * @param mediaEntryId the media entry id
     * @return rows affected
     */
    int updateGroupAwardMediaEntryId(final long clientId, final long linkId, final String mediaEntryId);

    /**
     * Update Recipients By ClientId
     *
     * @param clientId as long
     * @return rows affected
     */
    int updateRecipientsByClientId(long clientId);
}
