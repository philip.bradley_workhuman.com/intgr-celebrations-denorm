package com.workhuman.integration.celebrations.service.order;

import java.util.Date;

public class OrderDetailsTO {

    private String glbcertUniqueId;

    private Long productId;

    // DELIVERY_DATE : i.ideal_date
    private Date idealDate;

    //i.bao_batch_out_id
    private int baoBatchOutId;


    public String getGlbcertUniqueId() {
        return glbcertUniqueId;
    }

    public void setGlbcertUniqueId(String glbcertUniqueId) {
        this.glbcertUniqueId = glbcertUniqueId;
    }

    public Date getIdealDate() {
        return idealDate;
    }

    public void setIdealDate(Date idealDate) {
        this.idealDate = idealDate;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public int getBaoBatchOutId() {
        return baoBatchOutId;
    }

    public void setBaoBatchOutId(int baoBatchOutId) {
        this.baoBatchOutId = baoBatchOutId;
    }

}
