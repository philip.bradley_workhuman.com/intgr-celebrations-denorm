package com.workhuman.integration.celebrations.service.order;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailsServiceImpl implements OrderDetailsService {
    private static final Logger log = LoggerFactory.getLogger(OrderDetailsServiceImpl.class);

    @Autowired
    private OrderDetailsDao orderDetailsDao;

    @Override
    public OrderDetailsTO findByOrderId(long clientId, String indOrderId) {
        OrderDetailsTO orderDetailsTO = orderDetailsDao.findByOrderId(clientId, indOrderId);
        if (orderDetailsTO != null) {
            log.info("Found Order Details for indOrderId= " + indOrderId);
        }
        return orderDetailsTO;
    }

    @Override
    public OrderDetailsTO findByGlbcertId(long clientId, String glbcertId) {
        OrderDetailsTO orderDetailsTO = orderDetailsDao.findByGlbcertId(clientId, glbcertId);
        if (orderDetailsTO != null) {
            log.info("Found Order Details for GlbcertId= " + glbcertId);
        }
        return orderDetailsTO;
    }

    @Override
    public Long findProductId(long clientId, String glbcertId) {
        Long productId = orderDetailsDao.findProductId(clientId, glbcertId);
        if (productId != null) {
            log.info("Found productId for glbcertId= " + glbcertId);
        }
        return productId;
    }
}
