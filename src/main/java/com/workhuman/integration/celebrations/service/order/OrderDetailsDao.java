package com.workhuman.integration.celebrations.service.order;

public interface OrderDetailsDao {

    /**
     * Find OrderDetails by orderId
     *
     * @param clientId as long
     * @param indOrderId as String
     * @return orderDetailsTO
     */
    OrderDetailsTO findByOrderId(long clientId, String indOrderId);

    /**
     * Find OrderDetails by glbcertId
     *
     * @param clientId as long
     * @param glbcertId as String
     * @return orderDetailsTO
     */
    OrderDetailsTO findByGlbcertId(long clientId, String glbcertId);

    /**
     * Find productId based on glbcertId
     *
     * @param clientId as long
     * @param glbcertId as String
     * @return productId as Long
     */
    Long findProductId(long clientId, String glbcertId);
}

