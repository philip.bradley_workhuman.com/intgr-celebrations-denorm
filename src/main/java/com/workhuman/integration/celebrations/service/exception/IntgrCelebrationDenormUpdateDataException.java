package com.workhuman.integration.celebrations.service.exception;


/**
 *  Class IntgrCelebrationDenormUpdateDataException handles all exceptions
 *  which should be redirected into IMQ queue (such as missing required events data or
 *   missing details in DB etc)
 *  and they will not have possibility to process again
 */

public class IntgrCelebrationDenormUpdateDataException extends RuntimeException  {

    public IntgrCelebrationDenormUpdateDataException(String message) {
        super(message);
    }

    public IntgrCelebrationDenormUpdateDataException(String message, Throwable cause) {
        super(message, cause);
    }

}
