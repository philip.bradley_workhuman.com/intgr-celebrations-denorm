package com.workhuman.integration.celebrations.service.award;


import com.workhuman.integration.celebrations.service.utils.DAOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

import static com.workhuman.integration.celebrations.service.utils.DAOUtils.CLIENT_ID_PLACEHOLDER;
import static com.workhuman.integration.celebrations.service.utils.DAOUtils.CLIENT_STORECODE_PLACEHOLDER;
import static com.workhuman.integration.celebrations.service.utils.StoreCodeUtil.getStoreCode;


@Repository
public class AwardDetailsDaoImpl implements AwardDetailsDao {
    private static final Logger log = LoggerFactory.getLogger(AwardDetailsDaoImpl.class);

    private static final String GLBCERT_ID = "glbcertUniqueId";

    private static final String[] CLIENT_ID_AND_STORECODE_PLACEHOLDERS = {CLIENT_ID_PLACEHOLDER,
            CLIENT_STORECODE_PLACEHOLDER};

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public AwardDetailsDaoImpl(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    private static final String SQL_SELECT_AWARD_HISTORY = """
            SELECT 
                PK_AWARD_HISTORY AS pkAwardHistory,
                GLBCERT_ID            AS glbcertId,
                REASON                AS reason,
                PK_AWARDEE            AS pkAwardee,
                AWARD_PRIVATE         AS awardPrivate,
                LINK_ID               AS linkId
            FROM GG_AWARD_HISTORY
            WHERE PK_AWARD_HISTORY = :pkAwardHistory AND CLIENT_STORECODE = '#CLIENT_STORE_CODE'
            """;

    private static final String SQL_SELECT_AWARD_DETAILS = """
                         SELECT
                             po.glbcert_unique_id as glbcertUniqueId, 
                             po.fk_award_type     as fkAwardType, 
                             po.link_id           as linkId,
                             at.fk_client         as pkClient, 
                             ah.pk_awardee        as pkRecipient, 
                             ah.pk_award_history  as pkAwardHistory,
                             ah.award_private     as awardPrivate, 
                             ah.reason            as reason, 
                             ah.awarded_date      as awardedDate,
                             i.ideal_date         as idealDate, 
                             i.pro_prod_id        as proProdId, 
                             i.bao_batch_out_id   as baoBatchOutId,
                             m.entry_id           as mediaEntryId, 
                             m.media_type         as mediaType, 
                             i.cancelled, 
                             i.hold_order         as holdOrder, 
                             po.groupid  
                             %s
                         FROM 
                            gg_inbox i, 
                            gg_award_history ah, 
                            v_award_types at  
                            %s,  
                            gg_pending_orders po 
                         LEFT OUTER JOIN GG_MEDIA m ON po.groupid = m.group_id 
                         WHERE po.glbcert_unique_id = TO_CHAR(:glbcertUniqueId)
                         AND   po.client_storecode  = '#CLIENT_STORE_CODE'
                         AND   at.fk_client         = #CLIENT_ID 
                         AND   i.glbcert_unique_id  = po.glbcert_unique_id
                         AND   ah.glbcert_id        = po.glbcert_unique_id
                         AND   at.pk_award_type     = po.fk_award_type
                         AND   po.status            = 1
                         AND   i.bao_batch_out_id in (0,2) %s
            """;


    private static final String SELECT_SM_FIELD = ", j.years as years";
    private static final String FROM_SM_TABLES = ", gg_yos_award ya, v_jubilee j";

    private static final String WHERE_LE = " AND at.award_plus=1";
    private static final String WHERE_SM = " AND ya.glbcert_unique_id = ah.glbcert_id" +
            " AND ya.fk_jubilee = j.pk_jubilee" +
            " AND j.fk_client = at.fk_client " +
            " AND ya.fk_yos_ceo_message IS NOT NULL";

    private static final String SQL_CHECK_TIMELINES = "SELECT fk_yos_ceo_message FROM gg_yos_award " +
            "WHERE glbcert_unique_id = TO_CHAR(:glbcertUniqueId)";

    @Override
    public AwardDetailsTO findLifeEventAward(long clientId, String glbcertId) {
        final String replacedSql = StringUtils.replaceEach(SQL_SELECT_AWARD_DETAILS,
                CLIENT_ID_AND_STORECODE_PLACEHOLDERS, new String[]{Long.toString(clientId), getStoreCode(clientId)});

        String sql = String.format(replacedSql, "", "", WHERE_LE);
        return getSingleObject(sql, clientId, GLBCERT_ID, glbcertId, AwardDetailsTO.class);
    }

    @Override
    public AwardDetailsTO findServiceMilestonesAward(long clientId, String glbcertId) {
        final String replacedSql = StringUtils.replaceEach(SQL_SELECT_AWARD_DETAILS,
                CLIENT_ID_AND_STORECODE_PLACEHOLDERS, new String[]{Long.toString(clientId), getStoreCode(clientId)});
        String sql = String.format(replacedSql, SELECT_SM_FIELD, FROM_SM_TABLES, WHERE_SM);
        return getSingleObject(sql, clientId, GLBCERT_ID, glbcertId, AwardDetailsTO.class);
    }

    @Override
    public AwardHistoryTO findAwardHistoryByAwardHistoryId(long clientId, Long pkAwardHistory) {
        String replacedSql = SQL_SELECT_AWARD_HISTORY.replace(CLIENT_STORECODE_PLACEHOLDER, getStoreCode(clientId));
        return getSingleObject(replacedSql, clientId, "pkAwardHistory", String.valueOf(pkAwardHistory),
                AwardHistoryTO.class);
    }

    @Override
    public boolean isTimelinesYosAward(String glbcertId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource(GLBCERT_ID, glbcertId);
        Long fkYosCeoMessage = null;
        try {
            fkYosCeoMessage = jdbcTemplate.queryForObject(SQL_CHECK_TIMELINES, parameters, Long.class);
        } catch (EmptyResultDataAccessException er) {
            return false;
        }
        return fkYosCeoMessage == null ? false : true;
    }

    /**
     * Find Entity for specified field based on given sql
     *
     * @param sql        as String
     * @param clientId
     * @param paramName  - sql parameter name
     * @param paramValue - sql parameter value
     * @param clazz      - class of the bean to convert result to
     * @return awardDetailsTO
     */
    private <T> T getSingleObject(String sql, long clientId, String paramName, Object paramValue, Class<T> clazz) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("clientId", clientId);
        parameters.addValue(paramName, paramValue);

        List<T> items = jdbcTemplate.query(sql, parameters, new BeanPropertyRowMapper<>(clazz));
        return DAOUtils.getSingleObject(items, log, paramName, String.valueOf(paramValue));
    }
}
