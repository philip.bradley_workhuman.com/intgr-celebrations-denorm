package com.workhuman.integration.celebrations.service.media;

import com.workhuman.integration.celebrations.service.celebration.IntgrCelebrationDenormVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class MediaDetailsServiceImpl implements MediaDetailsService {

    private static final Logger LOG = LoggerFactory.getLogger(MediaDetailsServiceImpl.class);

    @Autowired
    private MediaDetailsDao mediaDetailsDao;

    @Override
    public MediaDetailsTO getMediaDetails(String mediaEntryId) {
        LOG.info("Start to search mediaDetails for mediaEntryId=" + mediaEntryId);
        MediaDetailsTO mediaDetailsTO = mediaDetailsDao.getMediaDetails(mediaEntryId);

        if (mediaDetailsTO != null) {
            LOG.info("For mediaEntryId [ " + mediaEntryId+ " ] the following media details were found  " + mediaDetailsTO.toString());
        } else {
            LOG.info("For mediaEntryId [ " + mediaEntryId+ " ]  no media details were found.");
        }
        return mediaDetailsTO;
    }

    @Override
    public String findFirstMediaEntryId(final IntgrCelebrationDenormVO celebrationDenormVO) {
        final Long awardHistoryId = celebrationDenormVO.getAwardHistoryId();
        final Long linkId = celebrationDenormVO.getLinkId();

        LOG.info(String.format("Start to search FirstMediaEntryId for pkAwardHistory=%d and linkId=%d", awardHistoryId, linkId));
        final String nominationMediaEntryId = mediaDetailsDao.findNominationMediaEntryId(awardHistoryId);
        LOG.info(String.format("For pkAwardHistory [%d] the following nomination mediaEntryId was found [%s]",
                awardHistoryId, nominationMediaEntryId));

        if (Objects.isNull(nominationMediaEntryId)) {
            String firstMediaGalleryEntryId;
            if (linkId != null){
                firstMediaGalleryEntryId = mediaDetailsDao.findFirstMediaGalleryEntryIdByLinkId(linkId);
            } else {
                firstMediaGalleryEntryId = mediaDetailsDao.findFirstMediaGalleryEntryId(awardHistoryId);
            }
            LOG.info(String.format("For pkAwardHistory [%d] and linkId [%d] the following award gallery mediaEntryId was found [%s]",
                    awardHistoryId, linkId, firstMediaGalleryEntryId));
            return firstMediaGalleryEntryId;
        }

        return nominationMediaEntryId;
    }
}
