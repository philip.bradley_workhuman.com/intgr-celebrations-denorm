package com.workhuman.integration.celebrations.service.celebration;

public interface CelebrationAwardDenormService {

    /**
     * Find an object of IntgrCelebrationDenormVO by AwardHistoryId
     *
     * @param awardHistoryId as String
     * @return object of IntgrCelebrationDenormVO
     */
    IntgrCelebrationDenormVO findByAwardHistoryId(long awardHistoryId);

    /**
     * Find list of objects of IntgrCelebrationDenormVO by linkId
     *
     * @param linkId as String
     * @return list of objects of IntgrCelebrationDenormVO
     */
    IntgrCelebrationDenormVO findFirstByLinkId(final long linkId);

    /**
     * Add an object of IntgrCelebrationDenormVO
     *
     * @param celebrationDenormVO as IntgrCelebrationDenormVO
     * @return rows affected
     */
    int add(IntgrCelebrationDenormVO celebrationDenormVO);

    /**
     * Delete an object of IntgrCelebrationDenormVO
     *
     * @param celebrationAwardDenormTO as IntgrCelebrationDenormVO
     * @return rows affected
     */
    int delete(IntgrCelebrationDenormVO celebrationAwardDenormTO);

    /**
     * Update Privacy Level
     *
     * @param celebrationAwardDenormTO as IntgrCelebrationDenormVO
     * @return rows affected
     */
    int updatePrivacyLevel(IntgrCelebrationDenormVO celebrationAwardDenormTO);

    /**
     * Update Delivery Date
     *
     * @param celebrationAwardDenormTO as IntgrCelebrationDenormVO
     * @return rows affected
     */
    int updateDeliveryDate(IntgrCelebrationDenormVO celebrationAwardDenormTO);

    /**
     * Update Delivery Date for Group Premium Award
     * with GG_INBOX.IDEAL_DATE value changes
     *
     * @param clientId Client Id
     * @param linkId   LinkId
     * @return rows affected
     */
    int updateGroupAwardDeliveryDate(long clientId, long linkId);

    /**
     * Update media entry id for single award.
     *
     * @param clientId       the client id
     * @param awardHistoryId the award history id
     * @param mediaEntryId   the media entry id
     * @return rows affected
     */
    int updateSingleAwardGallery(final long clientId, final long awardHistoryId, final String mediaEntryId);

    /**
     * Update media entry id for group awards (all awards sharing same link id).
     *
     * @param clientId     the client id
     * @param linkId       the group (link) award id
     * @param mediaEntryId the media entry id
     * @return rows affected
     */
    int updateGroupAwardGallery(final long clientId, final long linkId, final String mediaEntryId);

    /**
     * Update BaoBatchOutId
     *
     * @param celebrationAwardDenormTO as IntgrCelebrationDenormVO
     * @return rows affected
     */
    int updateBaoBatchOutId(IntgrCelebrationDenormVO celebrationAwardDenormTO);

    /**
     * Update Recipients By ClientId
     *
     * @param clientId as long
     * @return rows affected
     */
    int updateRecipientsByClientId(long clientId);
}
