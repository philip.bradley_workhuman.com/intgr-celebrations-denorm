package com.workhuman.integration.celebrations.service.award;

import java.sql.Timestamp;
import java.util.Date;

public class AwardDetailsTO {

    // recip.pk_client
    private long pkClient;

    // po.glbcert_unique_id
    private String glbcertUniqueId;

    // po.fk_award_type
    private Long fkAwardType;

    // i.PRO_PROD_ID
    private Long proProdId;

    // po.link_id
    private Long linkId;

    // ah.pk_award_history
    private Long pkAwardHistory;

    // ah.award_private
    private boolean awardPrivate;

    // ah.reason
    private String reason;

    // DELIVERY_DATE : i.ideal_date
    private Date idealDate;

    // ah.awarded_date
    private Date awardedDate;

    // recip.pk_person
    private Long pkRecipient;

    // recip.firstname
    private String recipientFirstname;

    // recip.lastname
    private String recipientLastname;

     // recip.expired
    private Timestamp recipientExpired;

    // RECIPIENT_HAS_NEWSFEED
    private boolean recipientHasNewsfeed;

    // j.years
    private Integer years;

    //i.bao_batch_out_id
    private int baoBatchOutId;

    // IS_TIMELINES
    private boolean timelines;

    // GG_MEDIA.ENTRY_ID
    private String mediaEntryId;

    // GG_MEDIA.MEDIA_TYPE
    private Integer mediaType;

    // i.cancelled
    private boolean cancelled;

    // i.hold_order
    private boolean holdOrder;

    public long getPkClient() {
        return pkClient;
    }

    public void setPkClient(long pkClient) {
        this.pkClient = pkClient;
    }

    public String getGlbcertUniqueId() {
        return glbcertUniqueId;
    }

    public void setGlbcertUniqueId(String glbcertUniqueId) {
        this.glbcertUniqueId = glbcertUniqueId;
    }

    public Long getFkAwardType() {
        return fkAwardType;
    }

    public void setFkAwardType(Long fkAwardType) {
        this.fkAwardType = fkAwardType;
    }

    public Long getProProdId() {
        return proProdId;
    }

    public void setProProdId(Long proProdId) {
        this.proProdId = proProdId;
    }

    public Long getLinkId() {
        return linkId;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }

    public Long getPkAwardHistory() {
        return pkAwardHistory;
    }

    public void setPkAwardHistory(Long pkAwardHistory) {
        this.pkAwardHistory = pkAwardHistory;
    }

    public boolean isAwardPrivate() {
        return awardPrivate;
    }

    public void setAwardPrivate(boolean awardPrivate) {
        this.awardPrivate = awardPrivate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getIdealDate() {
        return idealDate;
    }

    public void setIdealDate(Date idealDate) {
        this.idealDate = idealDate;
    }

    public Date getAwardedDate() {
        return awardedDate;
    }

    public void setAwardedDate(Date awardedDate) {
        this.awardedDate = awardedDate;
    }
    public Long getPkRecipient() {
        return pkRecipient;
    }

    public void setPkRecipient(Long pkRecipient) {
        this.pkRecipient = pkRecipient;
    }

    public String getRecipientFirstname() {
        return recipientFirstname;
    }

    public void setRecipientFirstname(String recipientFirstname) {
        this.recipientFirstname = recipientFirstname;
    }

    public String getRecipientLastname() {
        return recipientLastname;
    }

    public void setRecipientLastname(String recipientLastname) {
        this.recipientLastname = recipientLastname;
    }

    public Timestamp getRecipientExpired() {
        return recipientExpired;
    }

    public void setRecipientExpired(Timestamp recipientExpired) {
        this.recipientExpired = recipientExpired;
    }

    public boolean isRecipientHasNewsfeed() {
        return recipientHasNewsfeed;
    }

    public void setRecipientHasNewsfeed(boolean recipientHasNewsFeed) {
        this.recipientHasNewsfeed = recipientHasNewsFeed;
    }

    public Integer getYears() {
        return years;
    }

    public void setYears(Integer years) {
        this.years = years;
    }

    public int getBaoBatchOutId() {
        return baoBatchOutId;
    }

    public void setBaoBatchOutId(int baoBatchOutId) {
        this.baoBatchOutId = baoBatchOutId;
    }

    public boolean isTimelines() {
        return timelines;
    }

    public void setTimelines(boolean timelines) {
        this.timelines = timelines;
    }


    public String getMediaEntryId() {
        return mediaEntryId;
    }

    public void setMediaEntryId(String mediaEntryId) {
        this.mediaEntryId = mediaEntryId;
    }

    public Integer getMediaType() {
        return mediaType;
    }

    public void setMediaType(Integer mediaType) {
        this.mediaType = mediaType;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public boolean isHoldOrder() {
        return holdOrder;
    }

    public void setHoldOrder(boolean holdOrder) {
        this.holdOrder = holdOrder;
    }

    @Override
    public String toString() {
        return "AwardDetailsTO{" +
                "pkClient=" + pkClient +
                ", glbcertUniqueId='" + glbcertUniqueId + '\'' +
                ", fkAwardType=" + fkAwardType +
                ", proProdId=" + proProdId +
                ", linkId=" + linkId +
                ", pkAwardHistory=" + pkAwardHistory +
                ", awardPrivate=" + awardPrivate +
                ", reason='" + reason + '\'' +
                ", idealDate=" + idealDate +
                ", awardedDate=" + awardedDate +
                ", pkRecipient=" + pkRecipient +
                ", recipientFirstname='" + recipientFirstname + '\'' +
                ", recipientLastname='" + recipientLastname + '\'' +
                ", recipientExpired=" + recipientExpired +
                ", recipientHasNewsfeed=" + recipientHasNewsfeed +
                ", years=" + years +
                ", baoBatchOutId=" + baoBatchOutId +
                ", timelines=" + timelines +
                ", mediaEntryId='" + mediaEntryId + '\'' +
                ", mediaType='" + mediaType + '\'' +
                ", cancelled=" + cancelled +
                ", holdOrder=" + holdOrder +
                '}';
    }
}
