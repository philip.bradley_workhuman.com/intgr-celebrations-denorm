package com.workhuman.integration.celebrations.service.award;


public class AwardHistoryTO {
    private Long pkAwardHistory;
    private String glbcertId;
    private String reason;
    private Long pkAwardee;
    private boolean awardPrivate;
    private Long linkId;

    public Long getPkAwardHistory() {
        return pkAwardHistory;
    }

    public void setPkAwardHistory(Long pkAwardHistory) {
        this.pkAwardHistory = pkAwardHistory;
    }

    public String getGlbcertId() {
        return glbcertId;
    }

    public void setGlbcertId(String glbcertId) {
        this.glbcertId = glbcertId;
    }


    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getPkAwardee() {
        return pkAwardee;
    }

    public void setPkAwardee(Long pkAwardee) {
        this.pkAwardee = pkAwardee;
    }

    public boolean isAwardPrivate() {
        return awardPrivate;
    }

    public void setAwardPrivate(boolean awardPrivate) {
        this.awardPrivate = awardPrivate;
    }

    public Long getLinkId() {
        return linkId;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }
}
