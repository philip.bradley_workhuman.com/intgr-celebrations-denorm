package com.workhuman.integration.celebrations.service;


import com.workhuman.integration.celebrations.service.award.AwardDetailsTO;
import com.workhuman.integration.celebrations.service.celebration.CelebrationAwardDenormService;
import com.workhuman.integration.celebrations.service.celebration.IntgrCelebrationDenormVO;
import com.workhuman.integration.celebrations.service.exception.IntgrCelebrationDenormDuplicationException;
import com.workhuman.integration.celebrations.service.exception.IntgrCelebrationDenormServiceException;
import com.workhuman.integration.celebrations.service.exception.IntgrCelebrationDenormUpdateDataException;
import com.workhuman.integration.celebrations.service.person.PersonService;
import com.workhuman.integration.celebrations.service.person.PersonTO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IntgrCelebrationDenormServiceImpl implements IntgrCelebrationDenormService {

    private static final Logger LOG = LoggerFactory.getLogger(IntgrCelebrationDenormServiceImpl.class);

    static final Long NEWS_FEEDER_PRIVILEGE = 4194304L;

    @Autowired
    private CelebrationAwardDenormService celebrationAwardDenormService;

    @Autowired
    private PersonService personService;

    @Override
    public void denormalizeAwardCreation(AwardDetailsTO awardDetails) {
        try {
            populateRecipient(awardDetails);
            IntgrCelebrationDenormVO intgrAwardDenormVO = populateIntgrAwardDenormVO(awardDetails);
            int rowsAdded = celebrationAwardDenormService.add(intgrAwardDenormVO);
            String msg = String.format("Newly created award is denormalized with pk_celebration=[%d] for " +
                            "clientId= %d; glbcertId=['%s']; awardHistoryId= %d", intgrAwardDenormVO.getId(),
                    intgrAwardDenormVO.getClientId(), intgrAwardDenormVO.getGlbcertUniqueId(),
                    intgrAwardDenormVO.getAwardHistoryId());
            logAndCheckUpdatedRows(msg, rowsAdded);
        } catch (IntgrCelebrationDenormDuplicationException e) {
            LOG.warn(String.format("Duplication detected for glbcertId=['%s']; awardHistoryId=%d",
                    awardDetails.getGlbcertUniqueId(), awardDetails.getPkAwardHistory()));
        } catch (Exception ex) {
            LOG.warn(String.format("Denormalize failed for glbcertId=['%s']; awardHistoryId=%d",
                    awardDetails.getGlbcertUniqueId(), awardDetails.getPkAwardHistory()));
            throw new IntgrCelebrationDenormServiceException("Exception during denormalizeAwardCreation; ",
                    ex.fillInStackTrace());
        }
    }

    @Override
    public void denormalizeAwardCancellation(AwardDetailsTO awardDetails) {
        try {
            IntgrCelebrationDenormVO intgrAwardDenormVO = populateIntgrAwardDenormVO(awardDetails);
            int rowsUpdated = celebrationAwardDenormService.delete(intgrAwardDenormVO);
            String msg = String.format("[ %d ] rows were deleted due to Award Cancellation for clientId= %d; " +
                            "glbcertId=['%s']; awardHistoryId= %d", rowsUpdated, intgrAwardDenormVO.getClientId(),
                    intgrAwardDenormVO.getGlbcertUniqueId(), intgrAwardDenormVO.getAwardHistoryId());
            logPayAttention(msg, rowsUpdated);
        } catch (Exception ex) {
            LOG.warn(String.format("Denormalize failed for glbcertId=['%s']; awardHistoryId=%d",
                    awardDetails.getGlbcertUniqueId(), awardDetails.getPkAwardHistory()));
            throw new IntgrCelebrationDenormServiceException("Exception during denormalizeAwardCancellation; ",
                    ex.fillInStackTrace());
        }
    }

    @Override
    public void denormalizeAwardPrivacyChanged(AwardDetailsTO awardDetails) {
        try {
            IntgrCelebrationDenormVO intgrAwardDenormVO = populateIntgrAwardDenormVO(awardDetails);
            int rowsUpdated = celebrationAwardDenormService.updatePrivacyLevel(intgrAwardDenormVO);
            String msg = String.format("[ %d ] rows were updated with new Award Privacy Level for clientId= %d; " +
                            "glbcertId=['%s']; awardHistoryId= %d", rowsUpdated, intgrAwardDenormVO.getClientId(),
                    intgrAwardDenormVO.getGlbcertUniqueId(), intgrAwardDenormVO.getAwardHistoryId());
            logPayAttention(msg, rowsUpdated);
        } catch (Exception ex) {
            LOG.warn(String.format("Denormalize failed for glbcertId=['%s']; awardHistoryId=%d",
                    awardDetails.getGlbcertUniqueId(), awardDetails.getPkAwardHistory()));
            throw new IntgrCelebrationDenormServiceException("Exception during denormalizeAwardPrivacyChanged; ",
                    ex.fillInStackTrace());
        }
    }

    @Override
    public void denormalizeAwardDeliveryDateChanged(AwardDetailsTO awardDetails) {
        try {
            IntgrCelebrationDenormVO intgrAwardDenormVO = populateIntgrAwardDenormVO(awardDetails);
            int rowsUpdated = celebrationAwardDenormService.updateDeliveryDate(intgrAwardDenormVO);
            String msg = String.format("[ %d ] rows were updated with new Delivery Date for clientId= %d; " +
                            "glbcertId=['%s']; awardHistoryId= %d",
                    rowsUpdated, intgrAwardDenormVO.getClientId(), intgrAwardDenormVO.getGlbcertUniqueId(),
                    intgrAwardDenormVO.getAwardHistoryId());
            logPayAttention(msg, rowsUpdated);
        } catch (Exception ex) {
            LOG.warn(String.format("Denormalize failed for glbcertId=['%s']; awardHistoryId=%d",
                    awardDetails.getGlbcertUniqueId(), awardDetails.getPkAwardHistory()));
            throw new IntgrCelebrationDenormServiceException("Exception during  denormalizeAwardDeliveryDateChanged; ",
                    ex.fillInStackTrace());
        }
    }

    @Override
    public void denormalizeGroupAwardDeliveryDateChanged(long clientId, long linkId) {
        try {
            int rowsUpdated = celebrationAwardDenormService.updateGroupAwardDeliveryDate(clientId, linkId);
            String msg = String.format("[ %d ] rows were updated with a new Delivery Date based on " +
                    "GG_INBOX.IDEAL_DATE changes on GroupAwardDeliveryDateChanged for clientId= %d; linkId= %d",
                    rowsUpdated, clientId, linkId);
            logPayAttention(msg, rowsUpdated);
        } catch (Exception ex) {
            LOG.warn(String.format("Denormalize failed for clientId=%d, linkId=%d", clientId, linkId));
            throw new IntgrCelebrationDenormServiceException("Exception during GroupAwardDeliveryDateChanged; ",
                    ex.fillInStackTrace());
        }
    }

    @Override
    public void denormalizeAwardDelivery(AwardDetailsTO awardDetails) {
        try {
            IntgrCelebrationDenormVO intgrAwardDenormVO = populateIntgrAwardDenormVO(awardDetails);
            int rowsUpdated = celebrationAwardDenormService.updateBaoBatchOutId(intgrAwardDenormVO);
            String msg = String.format("[ %d ] rows were updated with new [bao_batch_out_id] for clientId= %d; " +
                            "glbcertId=['%s']; awardHistoryId= %d",
                    rowsUpdated, intgrAwardDenormVO.getClientId(), intgrAwardDenormVO.getGlbcertUniqueId(),
                    intgrAwardDenormVO.getAwardHistoryId());
            logPayAttention(msg, rowsUpdated);
        } catch (Exception ex) {
            LOG.warn(String.format("Denormalize failed for glbcertId=['%s']; awardHistoryId=%d",
                    awardDetails.getGlbcertUniqueId(), awardDetails.getPkAwardHistory()));
            throw new IntgrCelebrationDenormServiceException("Exception during denormalizeAwardDelivery; ",
                    ex.fillInStackTrace());
        }
    }

    @Override
    public void denormalizeSingleAwardGalleryUpdated(final long clientId, final long awardHistoryId,
                                                     final String mediaEntryId) {
        try {
            int rowsUpdated = celebrationAwardDenormService
                    .updateSingleAwardGallery(clientId, awardHistoryId, mediaEntryId);
            final String msg = String.format("[ %d ] rows were updated with new media_entry_id [ %s ] for " +
                            "clientId= %d; awardHistoryId= %d",
                    rowsUpdated, mediaEntryId, clientId, awardHistoryId);
            logPayAttention(msg, rowsUpdated);
        } catch (final Exception ex) {
            LOG.warn(String.format("Denormalize failed for clientId=%d; awardHistoryId=%d, mediaEntryId=%s",
                    clientId, awardHistoryId, mediaEntryId));
            throw new IntgrCelebrationDenormServiceException("Exception during AwardGalleryUpdated; ",
                    ex.fillInStackTrace());
        }
    }

    @Override
    public void denormalizeGroupAwardGalleryUpdated(final long clientId, final long linkId,
                                                    final String newFirstMediaEntryId) {
        try {
            int rowsUpdated = celebrationAwardDenormService
                    .updateGroupAwardGallery(clientId, linkId, newFirstMediaEntryId);
            final String msg = String.format("[ %d ] rows were updated with new media_entry_id [ %s ] for " +
                            "clientId= %d; linkId= %d",
                    rowsUpdated, newFirstMediaEntryId, clientId, linkId);
            logPayAttention(msg, rowsUpdated);
        } catch (final Exception ex) {
            LOG.warn(String.format("Denormalize failed for clientId=%d; linkId=%d, newFirstMediaEntryId=%s",
                    clientId, linkId, newFirstMediaEntryId));
            throw new IntgrCelebrationDenormServiceException("Exception during GroupAwardGalleryUpdated; ",
                    ex.fillInStackTrace());
        }
    }

    @Override
    public void denormalizePersonDataloadFinished(long clientId) {
        try {
            int rowsUpdated = celebrationAwardDenormService.updateRecipientsByClientId(clientId);
            String msg = String.format("[ %d ] rows were updated on PersonDataloadFinished for clientId= %d",
                    rowsUpdated, clientId);
            logPayAttention(msg, rowsUpdated);
        } catch (Exception ex) {
            LOG.warn(String.format("Denormalize failed for clientId=%d", clientId));
            throw new IntgrCelebrationDenormServiceException("Exception during PersonDataloadFinished; ",
                    ex.fillInStackTrace());
        }
    }

    @Override
    public void syncGroupAwardMedia(final AwardDetailsTO awardDetails) {
        if (awardDetails == null){
            throw new IllegalArgumentException("Award Details can not be null.");
        }

        final Long linkId = awardDetails.getLinkId();
        if (linkId == null){
            LOG.debug("Skipping process as award is not part of a group.");
            return;
        }

        final IntgrCelebrationDenormVO celebrationDenormAward = celebrationAwardDenormService.findFirstByLinkId(linkId);
        if (celebrationDenormAward == null){
            LOG.debug("Skipping process as no award has been found as part of the group (processing first award).");
            return;
        }

        final String existingGroupAwardMediaEntryId = celebrationDenormAward.getMediaEntryId();

        if (existingGroupAwardMediaEntryId == null || !StringUtils.equals(existingGroupAwardMediaEntryId, awardDetails.getMediaEntryId())){
            celebrationAwardDenormService.updateSingleAwardGallery(awardDetails.getPkClient(),
                    awardDetails.getPkAwardHistory(), existingGroupAwardMediaEntryId);
            LOG.info(String.format("Back filling new awardId=%d with existing mediaEntryId=%s", awardDetails.getPkAwardHistory(), existingGroupAwardMediaEntryId));
        }
    }

    /**
     * Check if news_feeder_privilege is included in userPrivilege
     *
     * @param userPrivilege
     * @return true if news_feeder_privilege is included in userPrivilege
     */
    private boolean hasNewsFeedPrivilege(Long userPrivilege) {
        return (userPrivilege & NEWS_FEEDER_PRIVILEGE) == NEWS_FEEDER_PRIVILEGE;
    }

    /**
     * Populate Recipient Details
     *
     * @param awardDetails
     */
    protected void populateRecipient(AwardDetailsTO awardDetails) {
        Long awardeeId = awardDetails.getPkRecipient();

        LOG.info(String.format("Try to find person [%d] for order glbcertId= '%s'", awardeeId, awardDetails.getGlbcertUniqueId()));
        PersonTO personTO = personService.findPersonTO(awardeeId, awardDetails.getPkClient());
        boolean userWithNewsfeedPrivilege = false;

        if (personTO != null) {
            LOG.info(String.format("Person [%d] is found for order glbcertId= '%s'", awardeeId, awardDetails.getGlbcertUniqueId()));
            userWithNewsfeedPrivilege = hasNewsFeedPrivilege(personTO.getPrivilege());
            awardDetails.setRecipientFirstname(personTO.getFirstName());
            awardDetails.setRecipientLastname(personTO.getLastName());
            awardDetails.setRecipientExpired(personTO.getExpired());
        } else {
            LOG.info(String.format("Person [%d] is NOT found for order glbcertId= '%s'", awardeeId, awardDetails.getGlbcertUniqueId()));
        }
        awardDetails.setRecipientHasNewsfeed(userWithNewsfeedPrivilege);
    }

    /**
     * Populate IntgrAwardDenormVO
     *
     * @param awardDetails as AwardDetailsTO
     */
    protected IntgrCelebrationDenormVO populateIntgrAwardDenormVO(AwardDetailsTO awardDetails) {
        IntgrCelebrationDenormVO intgrAwardDenormVO = new IntgrCelebrationDenormVO();

        intgrAwardDenormVO.setClientId(awardDetails.getPkClient());
        intgrAwardDenormVO.setGlbcertUniqueId(awardDetails.getGlbcertUniqueId());
        intgrAwardDenormVO.setAwardHistoryId(awardDetails.getPkAwardHistory());
        intgrAwardDenormVO.setAwardTypeId(awardDetails.getFkAwardType());
        intgrAwardDenormVO.setTimelines(awardDetails.isTimelines());
        intgrAwardDenormVO.setAwardReasonCode(awardDetails.getReason());
        intgrAwardDenormVO.setBaoBatchOutId(awardDetails.getBaoBatchOutId());
        intgrAwardDenormVO.setLinkId(awardDetails.getLinkId());
        if (awardDetails.isTimelines()) {
            intgrAwardDenormVO.setTimelines(awardDetails.isTimelines());
            intgrAwardDenormVO.setYosYears(awardDetails.getYears());
            intgrAwardDenormVO.setDeliveryDate(awardDetails.getAwardedDate());
        } else {
            intgrAwardDenormVO.setDeliveryDate(awardDetails.getIdealDate());
        }
        intgrAwardDenormVO.setAwardPrivate(awardDetails.isAwardPrivate());
        intgrAwardDenormVO.setMediaEntryId((awardDetails.getMediaEntryId()));
        intgrAwardDenormVO.setMediaType((awardDetails.getMediaType()));
        intgrAwardDenormVO.setRecipientExpired(awardDetails.getRecipientExpired());
        intgrAwardDenormVO.setRecipientFirstname(awardDetails.getRecipientFirstname());
        intgrAwardDenormVO.setRecipientLastname(awardDetails.getRecipientLastname());
        intgrAwardDenormVO.setRecipientId(awardDetails.getPkRecipient());
        intgrAwardDenormVO.setRecipientHasNewsfeedPrivilege(awardDetails.isRecipientHasNewsfeed());

        return intgrAwardDenormVO;
    }

    private void logAndCheckUpdatedRows(String messsage, int rowsUpdated) {
        if (rowsUpdated < 1) {
            LOG.warn(messsage);
            throw new IntgrCelebrationDenormUpdateDataException(messsage);
        } else {
            LOG.info(messsage);
        }
    }

    private void logPayAttention(String messsage, int rowsUpdated) {
        if (rowsUpdated < 1) {
            LOG.warn(messsage);
        } else {
            LOG.info(messsage);
        }
    }
}
