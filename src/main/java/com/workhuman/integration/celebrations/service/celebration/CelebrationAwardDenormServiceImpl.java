package com.workhuman.integration.celebrations.service.celebration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class CelebrationAwardDenormServiceImpl implements CelebrationAwardDenormService {
    private static final Logger LOG = LoggerFactory.getLogger(CelebrationAwardDenormServiceImpl.class);
    @Autowired
    private CelebrationAwardDenormDao celebrationAwardDenormDao;

    @Override
    public IntgrCelebrationDenormVO findByAwardHistoryId(long awardHistoryId) {
        return celebrationAwardDenormDao.findByAwardHistoryId(awardHistoryId);
    }

    @Override
    public IntgrCelebrationDenormVO findFirstByLinkId(final long linkId) {
        try{
            return celebrationAwardDenormDao.findFirstByLinkId(linkId);
        } catch (final EmptyResultDataAccessException ex){
            return null;
        }
    }

    @Override
    public int add(IntgrCelebrationDenormVO celebrationDenormVO) {
        return celebrationAwardDenormDao.add(celebrationDenormVO);
    }

    @Override
    public int updatePrivacyLevel(IntgrCelebrationDenormVO celebrationDenormVO) {
        LOG.info("CelebrationAwardDenormServiceImpl: Ready to update with new Award Privacy Level" + celebrationDenormVO.toString());
        return celebrationAwardDenormDao.updatePrivacyLevel(celebrationDenormVO);
    }

    @Override
    public int updateDeliveryDate(IntgrCelebrationDenormVO celebrationDenormVO) {
        LOG.info("CelebrationAwardDenormServiceImpl: Ready to update with new Delivery Date" + celebrationDenormVO.toString());
        return celebrationAwardDenormDao.updateDeliveryDate(celebrationDenormVO);
    }

    @Override
    public int updateGroupAwardDeliveryDate(long clientId, long linkId) {
        LOG.info("CelebrationAwardDenormServiceImpl: Ready to update with a new Delivery Date based on GG_INBOX.IDEAL_DATE changes for the Group Award with linkId:" + linkId);
        return celebrationAwardDenormDao.updateGroupAwardDeliveryDate(clientId, linkId);
    }

    @Override
    public int updateBaoBatchOutId(IntgrCelebrationDenormVO celebrationDenormVO) {
        LOG.info("CelebrationAwardDenormServiceImpl: Ready to update with new bao_batch_out_id" + celebrationDenormVO.toString());
        return celebrationAwardDenormDao.updateBaoBatchOutId(celebrationDenormVO);
    }

    @Override
    public int updateSingleAwardGallery(final long clientId, final long awardHistoryId, final String mediaEntryId) {
        LOG.info(String.format("CelebrationAwardDenormServiceImpl: Ready to update with new media_entry_id= %s and linkId=%d",
                mediaEntryId, awardHistoryId));
        return celebrationAwardDenormDao.updateSingleAwardMediaEntryId(clientId, awardHistoryId, mediaEntryId);
    }

    @Override
    public int updateGroupAwardGallery(final long clientId, final long linkId, final String mediaEntryId) {
        LOG.info(String.format("CelebrationAwardDenormServiceImpl: Ready to update with new media_entry_id= %s and linkId=%d",
                mediaEntryId, linkId));
        return celebrationAwardDenormDao.updateGroupAwardMediaEntryId(clientId, linkId, mediaEntryId);
    }

    @Override
    public int delete(IntgrCelebrationDenormVO celebrationDenormVO) {
        return celebrationAwardDenormDao.delete(celebrationDenormVO);
    }

    @Override
    public int updateRecipientsByClientId(long clientId) {
        return celebrationAwardDenormDao.updateRecipientsByClientId(clientId);
    }
}
