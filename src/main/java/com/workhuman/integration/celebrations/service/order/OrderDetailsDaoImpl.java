package com.workhuman.integration.celebrations.service.order;

import com.workhuman.integration.celebrations.service.utils.DAOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

import static com.workhuman.integration.celebrations.service.utils.DAOUtils.CLIENT_STORECODE_PLACEHOLDER;
import static com.workhuman.integration.celebrations.service.utils.StoreCodeUtil.getStoreCode;


@Repository
public class OrderDetailsDaoImpl implements OrderDetailsDao {
    private static final Logger log = LoggerFactory.getLogger(OrderDetailsDaoImpl.class);
    
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String SQL_SELECT_ORDER_DETAILS = "SELECT " +
            " i.pro_prod_id AS productId, " +
            " i.glbcert_unique_id AS glbcertUniqueId," +
            " i.ideal_date as idealDate," +
            " i.bao_batch_out_id as baoBatchOutId" +
            " FROM gg_inbox i " +
            " WHERE i.orig_store_code = '" + CLIENT_STORECODE_PLACEHOLDER + "' ";

    private static final String SQL_WHERE_ORDER_ID = " AND i.ind_order_id = TO_CHAR(:indOrderId)";

    private static final String SQL_WHERE_GLBCERT_ID = " AND i.glbcert_unique_id = TO_CHAR(:glbcertUniqueId)";

    private static final String SQL_SELECT_PRODUCT_ID = "SELECT pro_prod_id as productid FROM gg_inbox " +
            " WHERE orig_store_code = '" + CLIENT_STORECODE_PLACEHOLDER + "'" +
            " and glbcert_unique_id = TO_CHAR(:glbcertUniqueId)";


    @Autowired
    public OrderDetailsDaoImpl(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public OrderDetailsTO findByOrderId(long clientId, String indOrderId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        String replacedSql = SQL_SELECT_ORDER_DETAILS.replace(CLIENT_STORECODE_PLACEHOLDER, getStoreCode(clientId));
        String sql = replacedSql + SQL_WHERE_ORDER_ID;
        return find(sql, parameters, "indOrderId", indOrderId);
    }

    @Override
    public OrderDetailsTO findByGlbcertId(long clientId, String glbcertId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        String replacedSql = SQL_SELECT_ORDER_DETAILS.replace(CLIENT_STORECODE_PLACEHOLDER, getStoreCode(clientId));
        String sql = replacedSql + SQL_WHERE_GLBCERT_ID;
        return find(sql, parameters, "glbcertUniqueId", glbcertId);
    }

    @Override
    public Long findProductId(long clientId, String glbcertId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource("glbcertUniqueId", glbcertId);
        String replacedSql = SQL_SELECT_PRODUCT_ID.replace(CLIENT_STORECODE_PLACEHOLDER, getStoreCode(clientId));
        return jdbcTemplate.queryForObject(replacedSql, parameters, Long.class);
    }

    /**
     * Find Order Details based on sql and parameters
     * @param sql as String
     * @param parameters as MapSqlParameterSource
     * @return  orderDetailsTO or null
     */
    private OrderDetailsTO find(String sql, MapSqlParameterSource parameters, String paramName, String paramValue) {
        parameters.addValue(paramName, paramValue);
        
        List<OrderDetailsTO> orderDetailsTOList = jdbcTemplate.query(sql, parameters, new BeanPropertyRowMapper<OrderDetailsTO>(OrderDetailsTO.class));
        
        return DAOUtils.getSingleObject(orderDetailsTOList, log, paramName, paramValue);
    }
}
