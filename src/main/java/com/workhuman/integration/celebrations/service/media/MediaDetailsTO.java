package com.workhuman.integration.celebrations.service.media;

public class MediaDetailsTO {

    private long pkMedia;

    private long clientId;

    private Long creatorId;

    private String entryId;

    private Long groupId;

    private Long linkId;

    private Long fkAwardHistory;

    private int sortOrder;

    public long getPkMedia() {
        return pkMedia;
    }

    public void setPkMedia(long pkMedia) {
        this.pkMedia = pkMedia;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getEntryId() {
        return entryId;
    }

    public void setEntryId(String entryId) {
        this.entryId = entryId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getLinkId() {
        return linkId;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }

    public Long getFkAwardHistory() {
        return fkAwardHistory;
    }

    public void setFkAwardHistory(Long fkAwardHistory) {
        this.fkAwardHistory = fkAwardHistory;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Override
    public String toString() {
        return "MediaDetailsTO{" +
                "pkMedia=" + pkMedia +
                ", clientId=" + clientId +
                ", creatorId=" + creatorId +
                ", entryId='" + entryId + '\'' +
                ", groupId=" + groupId +
                ", linkId=" + linkId +
                ", fkAwardHistory=" + fkAwardHistory +
                ", sortOrder=" + sortOrder +
                '}';
    }

}
