package com.workhuman.integration.celebrations.service.properties;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PropertiesServiceImpl implements PropertiesService {
    @Override
    public List<PropertyTO> getPropertiesByTag(long var1, String var3) {
        return null;
    }

    @Override
    public PropertyTO getPropertyByTagAndName(long var1, String var3, String var4) {
        return null;
    }

    @Override
    public PropertyTO getPropertyByName(long var1, String var3) {
        return null;
    }

    @Override
    public long getLongPropertyByName(long var1, String var3) {
        return 0;
    }

    @Override
    public boolean getBooleanPropertyByName(long var1, String var3) {
        return false;
    }

    @Override
    public String getStringPropertyByName(long var1, String var3) {
        return null;
    }

    @Override
    public String getStringLargePropertyByName(long var1, String var3) {
        return null;
    }
}
