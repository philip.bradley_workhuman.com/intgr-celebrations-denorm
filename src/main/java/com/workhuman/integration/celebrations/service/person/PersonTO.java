package com.workhuman.integration.celebrations.service.person;

import java.sql.Timestamp;

public class PersonTO {

    private long pkPerson;

    private long privilege;
    private String firstName;
    private String lastName;
    private Timestamp expired;

    public long getPkPerson() {
        return pkPerson;
    }

    public void setPkPerson(long pkPerson) {
        this.pkPerson = pkPerson;
    }

    public long getPrivilege() {
        return privilege;
    }

    public void setPrivilege(long privilege) {
        this.privilege = privilege;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Timestamp getExpired() {
        return expired;
    }

    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }
}
