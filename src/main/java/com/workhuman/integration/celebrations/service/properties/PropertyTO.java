package com.workhuman.integration.celebrations.service.properties;

import java.util.Objects;

public class PropertyTO {

    private String name;
    private String value;
    private long clientId;
    private String lang;
    private String countryCode;

    public PropertyTO() {
    }

    public String getName() {
        return this.name;
    }

    public String getValue() {
        return this.value;
    }

    public long getClientId() {
        return this.clientId;
    }

    public String getLang() {
        return this.lang;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o != null && this.getClass() == o.getClass()) {
            PropertyTO that = (PropertyTO)o;
            return this.clientId == that.clientId && Objects.equals(this.name, that.name) && Objects.equals(this.value, that.value) && Objects.equals(this.lang, that.lang) && Objects.equals(this.countryCode, that.countryCode);
        } else {
            return false;
        }
    }

    public int hashCode() {
        return Objects.hash(new Object[]{this.name, this.value, this.clientId, this.lang, this.countryCode});
    }

    public String toString() {
        return "PropertyTO{name='" + this.name + '\'' + ", value='" + this.value + '\'' + ", clientId=" + this.clientId + ", lang='" + this.lang + '\'' + ", countryCode='" + this.countryCode + '\'' + '}';
    }
}
