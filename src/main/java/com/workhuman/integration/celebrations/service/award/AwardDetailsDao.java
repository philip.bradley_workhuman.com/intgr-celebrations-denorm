package com.workhuman.integration.celebrations.service.award;

public interface AwardDetailsDao {

       /**
        * Find LifeEvent Award
        * @param clientId
        * @param glbcertId
        * @return awardDetailsTO as AwardDetailsTO
        */
       AwardDetailsTO findLifeEventAward(long clientId, String glbcertId);

       /**
        * Find ServiceMilestones Award
        *
        * @param clientId
        * @param glbcertId
        * @return awardDetailsTO as AwardDetailsTO
        */
       AwardDetailsTO findServiceMilestonesAward(long clientId, String glbcertId);

       /**
        * Find AwardHistory By AwardHistoryId
        * @param clientId
        * @param pkAwardHistory
        * @return awardHistoryTO as AwardHistoryTO
        */
       AwardHistoryTO findAwardHistoryByAwardHistoryId(long clientId, Long pkAwardHistory);

       /**
        *
        * @param glbcertId
        * @return true if YOS award is timelines
        */
       boolean isTimelinesYosAward(String glbcertId);
}
