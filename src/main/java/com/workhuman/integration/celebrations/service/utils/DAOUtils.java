package com.workhuman.integration.celebrations.service.utils;

import com.workhuman.integration.celebrations.service.exception.TooManyResultsException;
import org.slf4j.Logger;
import org.springframework.util.CollectionUtils;

import java.util.List;

public class DAOUtils {

    public static final String CLIENT_ID_PLACEHOLDER = "#CLIENT_ID";
    public static final String CLIENT_STORECODE_PLACEHOLDER = "#CLIENT_STORE_CODE";

    private DAOUtils() {
    }

    /**
     * Gets single object from list or return null with logged warning message.
     *
     * @param items List of items
     * @param log as Logger
     * @param paramName parameter name as String
     * @param paramValue parameter value as String
     * @return single object
     *
     * @throws TooManyResultsException thrown when more than 1 item are in the List of items
     */
     public static <T> T getSingleObject(List<T> items, Logger log, String paramName, String paramValue) throws TooManyResultsException {
        if (CollectionUtils.isEmpty(items)) {
            return null;
        }
        if (items.size() > 1) {
            final String errorMessage= String.format("Found <%d> records; expected only one (1) for parameter <%s> with value '%s' ", items.size(), paramName, paramValue);
            if(log != null) {
                log.warn(errorMessage);
            }
            throw new TooManyResultsException(errorMessage);
        }
        return items.get(0);
    }
}
