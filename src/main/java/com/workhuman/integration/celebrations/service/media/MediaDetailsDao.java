package com.workhuman.integration.celebrations.service.media;

public interface MediaDetailsDao {

    /**
     * Find nomination Media Entry Id by award history id.
     *
     * @param pkAwardHistory
     * @return mediaEntryId as String
     */
    String findNominationMediaEntryId(final long pkAwardHistory);

    /**
     * Find First Media Gallery Entry Id for a specific award.
     *
     * @param pkAwardHistory as Award History Id
     * @return mediaEntryId as String
     */
    String findFirstMediaGalleryEntryId(final long pkAwardHistory);

    /**
     * Find First Media Gallery Entry Id for a specific award by linkId.
     *
     * @param linkId as award link identifier
     * @return mediaEntryId as String
     */
    String findFirstMediaGalleryEntryIdByLinkId(final long linkId);

    /**
     * Get Media Details based on specified mediaEntryId
     *
     * @param mediaEntryId as String
     * @return mediaDetails object
     */
    MediaDetailsTO getMediaDetails(final String mediaEntryId);
}
