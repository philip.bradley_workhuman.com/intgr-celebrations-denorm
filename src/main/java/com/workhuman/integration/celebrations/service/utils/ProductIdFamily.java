package com.workhuman.integration.celebrations.service.utils;

import java.util.Arrays;
import java.util.List;

public enum ProductIdFamily {
    PREMIUM_AWARDS(680L, 780L, 682L, 782L),
    YOS(650L,750L,660L,651L,751L,661L);

    private List<Long> productIds;

    ProductIdFamily(Long... productIds) {
        this.productIds = Arrays.asList(productIds);
    }

    public static boolean isYOSProdId(long productId) {
        return YOS.productIds.contains(productId);
    }

    public static boolean isPremiumAwardsProdId(long productId) {
        return PREMIUM_AWARDS.productIds.contains(productId);
    }

    public static boolean isCelebrationProdId(long productId) {
        return isYOSProdId(productId) || isPremiumAwardsProdId(productId);
    }
}
