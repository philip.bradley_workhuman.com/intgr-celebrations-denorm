package com.workhuman.integration.celebrations.service.exception;

/**
 * This exception is to mark cases when single or null object should be returned but somehow more than 1 are.
 */
public class TooManyResultsException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public TooManyResultsException(String message) {
        super(message);
    }

    public TooManyResultsException(String message, Throwable cause) {
        super(message, cause);
    }
}
