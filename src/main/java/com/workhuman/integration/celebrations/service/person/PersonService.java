package com.workhuman.integration.celebrations.service.person;

public interface PersonService {

    PersonTO findPersonTO (long param1, long param2);
}
