package com.workhuman.integration.celebrations.service.award;

public interface AwardDetailsService {

    /**
     * Find Life Event Award
     *
     * @param clientId as long
     * @param glbcertId as String
     * @return awardDetailsTO as AwardDetailsTO
     */
    AwardDetailsTO findLifeEventAward(long clientId, String glbcertId);

    /**
     * Find ServiceMilestones Award
     *
     * @param clientId as long
     * @param glbcertId as String
     * @return awardDetailsTO as AwardDetailsTO
     */
    AwardDetailsTO findServiceMilestonesAward(long clientId, String glbcertId);

     /**
     * Find AwardDetails By AwardHistoryId
     * @param clientId as long
     * @param pkAwardHistory as Long
     * @return
     */
    AwardDetailsTO findAwardDetailsByAwardHistoryId(long clientId, Long pkAwardHistory);

    /**
     *
     * @param glbcertId
     * @return true if YOS award is timelines
     */
    boolean isTimelinesYosAward(String glbcertId);
}
