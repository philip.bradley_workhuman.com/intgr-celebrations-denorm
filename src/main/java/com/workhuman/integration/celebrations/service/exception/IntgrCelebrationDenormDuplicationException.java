package com.workhuman.integration.celebrations.service.exception;

public class IntgrCelebrationDenormDuplicationException extends RuntimeException  {

    public IntgrCelebrationDenormDuplicationException(Throwable cause) {
        super(cause);
    }
}
