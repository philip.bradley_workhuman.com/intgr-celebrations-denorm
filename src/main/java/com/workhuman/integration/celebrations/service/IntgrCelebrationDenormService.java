package com.workhuman.integration.celebrations.service;


import com.workhuman.integration.celebrations.service.award.AwardDetailsTO;

public interface IntgrCelebrationDenormService {

    void denormalizeAwardCreation(AwardDetailsTO awardDetails);

    void denormalizeAwardCancellation(AwardDetailsTO awardDetails);

    void denormalizeAwardPrivacyChanged(AwardDetailsTO awardDetails);

    void denormalizeAwardDeliveryDateChanged(AwardDetailsTO awardDetails);

    /**
     * Update Delivery Date in rec_celebration_denorm table
     * for Group Premium Award
     * with GG_INBOX.IDEAL_DATE value changes
     *
     * @param clientId
     * @param linkId
     */
    void denormalizeGroupAwardDeliveryDateChanged(long clientId, long linkId);

    void denormalizeAwardDelivery(AwardDetailsTO awardDetails);

    /**
     * Persist award's gallery update changes. Will update mediaEntryId for one single award.
     *
     * @param clientId       the client id
     * @param awardHistoryId the award history id
     * @param mediaEntryId   the media entry id
     */
    void denormalizeSingleAwardGalleryUpdated(final long clientId, final long awardHistoryId, final String mediaEntryId);

    /**
     * Persist award's gallery update changes. Will update mediaEntryId for all awards sharing
     * same linkId(group awards).
     *
     * @param clientId     the client id
     * @param linkId       the award link or group id.
     * @param mediaEntryId the media entry id
     */
    void denormalizeGroupAwardGalleryUpdated(final long clientId, final long linkId, final String mediaEntryId);

    void denormalizePersonDataloadFinished(long clientId);

    /**
     * Synchronizes new celebration denorm award by adding existing media from other awards belonging to the same group award.
     * This is required for the cases when media has potentially been updated (or deleted) for some other awards belonging to
     * the same group award as the newly created {@code awardDetails}.
     *
     * @param awardDetails the new award to be sync.
     */
    void syncGroupAwardMedia(final AwardDetailsTO awardDetails);
}
