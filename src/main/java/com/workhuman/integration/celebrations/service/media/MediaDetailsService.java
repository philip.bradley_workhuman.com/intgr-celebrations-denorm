package com.workhuman.integration.celebrations.service.media;


import com.workhuman.integration.celebrations.service.celebration.IntgrCelebrationDenormVO;

public interface MediaDetailsService {
    /**
     * Get Media Details based on specified mediaEntryId
     *
     * @param mediaEntryId as String
     * @return mediaDetails object
     */
    MediaDetailsTO getMediaDetails(String mediaEntryId);

    /**
     * Find First Media Gallery Entry Id from Celebration Award Gallery,
     * prioritizing media attached as part of nomination process.
     *
     * @param celebrationDenorm as the denormalized celebration award
     * @return mediaEntryId as String
     */
    String findFirstMediaEntryId(final IntgrCelebrationDenormVO celebrationDenorm);

}
