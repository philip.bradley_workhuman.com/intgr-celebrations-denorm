package com.workhuman.integration.celebrations.service.media;


import com.workhuman.integration.celebrations.service.celebration.IntgrCelebrationDenormVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MediaValidatorImpl implements MediaValidator {

    @Autowired
    protected MediaDetailsService mediaDetailsService;

    @Override
    public boolean mediaEntryDoesNotBelongToAwardGallery(final String eventMediaEntryId) {
        if (eventMediaEntryId == null) {
            throw new IllegalArgumentException("Event media entry id can not be null.");
        }

        final MediaDetailsTO mediaDetails = mediaDetailsService.getMediaDetails(eventMediaEntryId);

        return mediaDetails != null && mediaDetails.getFkAwardHistory() == null && mediaDetails.getLinkId() == null;
    }

    @Override
    public boolean isRemovedMediaNotTheFirstAwardMedia(final String mediaEntryId, final IntgrCelebrationDenormVO denormalizedAward) {
        final MediaDetailsTO mediaDetails = mediaDetailsService.getMediaDetails(mediaEntryId);
        return mediaDetails == null && !StringUtils.equals(mediaEntryId, denormalizedAward.getMediaEntryId());
    }

    @Override
    public boolean areEquals(final String mediaEntryId, final IntgrCelebrationDenormVO denormalizedAward) {
        return StringUtils.equals(mediaEntryId, denormalizedAward.getMediaEntryId());
    }
}
