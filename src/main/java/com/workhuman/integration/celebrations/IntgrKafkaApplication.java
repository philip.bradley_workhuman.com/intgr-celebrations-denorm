package com.workhuman.integration.celebrations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class IntgrKafkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntgrKafkaApplication.class, args);
	}

}
