CREATE TABLE IF NOT EXISTS GG_AWARD_SUMMARY_DENORM
(
    ID                             NUMBER(38) PRIMARY KEY,
    NON_PRIVATE_AWARDS_COUNT       NUMBER(4) DEFAULT 0,
    USERS_WITH_NEWSFEED_PRIV_COUNT NUMBER(4) DEFAULT 0,
    IS_VIDEO_AWARD                 NUMBER(1) DEFAULT 0,
    LINK_ID                        NUMBER(38),
    DATE_CREATED                   DATE,
    DATE_DISPLAY                   DATE,
    DATE_PUBLIC                    DATE,
    CLIENT_ID                      NUMBER(38)        NOT NULL,
    AWARD_REASON_ID                VARCHAR2(15) NULL
);