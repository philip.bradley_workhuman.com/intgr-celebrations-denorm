CREATE TABLE IF NOT EXISTS GG_MS_CLIENTS
(
    PK_CLIENT               NUMBER(22, 0)                 NOT NULL,
    NAME                    VARCHAR2(50)                  NOT NULL,
    STORE_CODE              VARCHAR2(15)                  NOT NULL,
    DESCRIPTION             VARCHAR2(200)                 NULL,
    REPORTING_FX            VARCHAR2(10)                  ,
    REDIRECT_EMAIL_ADDRESS  VARCHAR2(50)                  NULL,
    FROM_ADDRESS            VARCHAR2(100)                 NULL,
    REPLY_TO_ADDRESS        VARCHAR2(100)                 NULL,
    IS_USGAAP               NUMBER(1),
    ALIAS                   varchar2(70),
    TEST                    NUMBER(1, 0) DEFAULT 0        ,
    PREPAID                 NUMBER(1, 0) DEFAULT 0        ,
    config_client_id        NUMBER(38)                    ,
    client_type             VARCHAR2(15)                  ,
    status                  VARCHAR(10)  DEFAULT ('LIVE') ,
    IDENTITY_COMMUNITY_GUID VARCHAR2(40),
    PRIMARY KEY (PK_CLIENT),
    CONSTRAINT AK_IDENTIFIER_2_GG_MS_CL UNIQUE (NAME, STORE_CODE),
    CONSTRAINT GG_MS_CLIENTS_IC_GUID_UQ UNIQUE (IDENTITY_COMMUNITY_GUID)
);
