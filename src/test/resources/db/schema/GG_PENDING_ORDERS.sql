CREATE TABLE IF NOT EXISTS GG_PENDING_ORDERS
(
    PK_PENDING_ORDER        NUMBER(22, 0)  ,
    PK_BUDGET               NUMBER(22, 0)  NULL,
    STATUS                  NUMBER(2, 0)   ,
    PENDING                 NUMBER(1, 0)   ,
    LANG                    VARCHAR2(3)    ,
    PK_PERSON_AWARDER       NUMBER(22, 0)  NULL,
    PK_PERSON_AWARDEE       NUMBER(22, 0)  NULL,
    PK_PERSON_SHIPTO        NUMBER(22, 0)  NULL,
    PK_PERSON_AUTHORIZER    NUMBER(22, 0)  NULL,
    CLIENT_STORECODE        VARCHAR2(15)   ,
    RETAILER_CODE           VARCHAR2(15)   ,
    FF_STORECODE            VARCHAR2(15)   ,
    PRODUCTID               NUMBER(22, 0)  ,
    PROMOID                 VARCHAR2(50)   NULL,
    OCCASION                VARCHAR2(15)   NULL,
    SPECIAL_WORDING         VARCHAR2(2000) NULL,
    IDEAL_DATE              DATE           NULL,
    SHIPPING                NUMBER(10, 2)  NULL,
    SERVICE_CHARGE          NUMBER(10, 2)  NULL,
    ORIG_AMOUNT             NUMBER(12, 2)  ,
    DEST_AMOUNT             NUMBER(12, 2)  ,
    FX_RATE                 NUMBER(16, 6)  ,
    ORIG_FX                 VARCHAR2(6)    ,
    DEST_FX                 VARCHAR2(6)    ,
    PAYMENT_METHOD          NUMBER(2, 0)   ,
    CARD_TYPE               VARCHAR2(50)   NULL,
    CARD_NUMBER             VARCHAR2(50)   NULL,
    CARD_EXPIRY             VARCHAR2(50)   NULL,
    CARD_HOLDER             VARCHAR2(100)  NULL,
    CREATED                 DATE           ,
    MODIFIED                DATE           NULL,
    GLBCERT_DESIGN          VARCHAR2(50)   NULL,
    GLBCERT_UNIQUE_ID       VARCHAR2(50)   NULL,
    GLBCERT_LOGO            VARCHAR2(50)   NULL,
    GROUPID                 NUMBER(22, 0)  NULL,
    APPROVERID              NUMBER(22, 0)  NULL,
    REASON                  VARCHAR2(2000) NULL,
    PK_ORGANISATION         NUMBER(22, 0)  NULL,
    USER_DEF_1              VARCHAR2(1000) NULL,
    USER_DEF_2              VARCHAR2(1000) NULL,
    USER_DEF_3              VARCHAR2(1000) NULL,
    COST_CENTRE             VARCHAR2(100)  NULL,
    MANAGER                 VARCHAR2(50)   NULL,
    FK_AWARD_TYPE           NUMBER(12, 0)  NULL,
    CREATOR                 NUMBER(22, 0)  ,
    LAST_APPROVAL_ACTOR     Number,
    LAST_APPROVAL_ROLE      Number,
    PRIMARY KEY (PK_PENDING_ORDER),
    TITLE                   VARCHAR2(400),
    MESSAGE_PRIVATE         NUMBER(1)    DEFAULT 0,
    RECOMMENDED_AWARD       NUMBER(12)   DEFAULT 0,
    RECOMMENDED_AWARD_Test  NUMBER(12)   DEFAULT 0,
    AWARD_PRIVATE           NUMBER(1, 0) DEFAULT 0,
    INVOICING_ENTITY_ID     VARCHAR2(50)   NULL,
    IND_ORDER_ID            VARCHAR2(50)   NULL,
    LINK_ID                 NUMBER         NULL,
    GF_BILLING_STRATEGY     NUMBER         NULL,
    GF_COST_CENTRE_STRATEGY NUMBER         NULL,
    FK_INVOICE_GROUP        NUMBER(12, 0)  NULL,
    PROGRAM_ID              NUMBER(12)
);
