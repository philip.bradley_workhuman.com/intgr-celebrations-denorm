CREATE TABLE IF NOT EXISTS GG_JUBILEE
(
    PK_JUBILEE           NUMBER(22, 0)          ,
    FK_CLIENT            NUMBER(22, 0)          ,
    FK_AWARD_TYPE        NUMBER(22, 0)          ,
    AWARD_ADVANCE_PERIOD NUMBER(22, 0)          ,
    YEARS                NUMBER(22, 0)          ,
    IS_BATCH_PROCESSING  NUMBER(1, 0) default 0 ,
    INVITEES_TARGET      NUMBER(7)    DEFAULT 0 ,
    FK_MERCHANDISE_CODE  NUMBER,
    PRIMARY KEY (PK_JUBILEE)
);
