-- ==========================================================================
--  NEW TABLE GG_YOS_AWARD
-- ==========================================================================
CREATE TABLE IF NOT EXISTS GG_YOS_AWARD
(
    PK_YOS_AWARD              NUMBER,
    GLBCERT_UNIQUE_ID         VARCHAR2(50),
    FK_JUBILEE                NUMBER,
    FK_YOS_CEO_MESSAGE        NUMBER,
    FK_YOS_CEO_SIGNATURE      NUMBER,
    MANAGER_MESSAGE_AUTHOR_ID NUMBER,
    MANAGER_MESSAGE           CLOB,
    RECOGNITION_MOMENT_ID     VARCHAR2(36),
    YEARS                     NUMBER(22, 0)
);

CREATE SEQUENCE IF NOT EXISTS GG_YOS_AWARD_SEQ START WITH 1;

-- ==========================================================================
--  NEW TABLE GG_YOS_CEO_SIGNATURE
-- ==========================================================================

CREATE TABLE IF NOT EXISTS GG_YOS_CEO_SIGNATURE
(
    PK_YOS_CEO_SIGNATURE NUMBER,
    IS_PICTURE_LOADED    NUMBER(1)           NOT NULL,
    IS_SIGNATURE_LOADED  NUMBER(1)           NOT NULL,
    FK_CLIENT            NUMBER,
    IS_CURRENT           NUMBER(2) DEFAULT 1 NOT NULL,
    NAME                 VARCHAR2(800)       NOT NULL,
    PICTURE_ORDER        NUMBER
);

CREATE SEQUENCE IF NOT EXISTS GG_YOS_CEO_SIGNATURE_SEQ START WITH 1;

-- ==========================================================================
--  NEW TABLE GG_YOS_CEO_MESSAGE
-- ==========================================================================

CREATE TABLE IF NOT EXISTS GG_YOS_CEO_MESSAGE
(
    PK_YOS_CEO_MESSAGE NUMBER,
    FK_JUBILEE         NUMBER,
    IS_CURRENT         NUMBER(2) DEFAULT 1 NOT NULL
);

CREATE SEQUENCE IF NOT EXISTS  GG_YOS_CEO_MESSAGE_SEQ START WITH 1;

-- ==========================================================================
--  NEW TABLE GG_YOS_CEO_MESSAGE_LOCALE
-- ==========================================================================

CREATE TABLE IF NOT EXISTS GG_YOS_CEO_MESSAGE_LOCALE
(
    FK_YOS_CEO_MESSAGE NUMBER,
    LANG               VARCHAR2(3) NOT NULL,
    TEXT               CLOB
);
