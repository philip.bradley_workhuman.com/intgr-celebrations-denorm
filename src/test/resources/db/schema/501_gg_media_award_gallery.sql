CREATE TABLE IF NOT EXISTS gg_media_award_gallery (
  fk_media NUMBER(19) NOT NULL,
  fk_award_history NUMBER,
  link_id NUMBER,
  sort_order NUMBER(5) NOT NULL
);
