Create view V_award_types AS
with v_client_details_004 as (select c.pk_client,
                                     case
                                         WHEN M.FK_CLIENT_SRC IS NULL THEN C.CONFIG_CLIENT_ID
                                         when m.custom = 1 then c.pk_client
                                         else c.config_client_id
                                         end as config_client
                              from gg_ms_clients c
                                       left join GG_CONFIG_META m
                                                 on M.FK_CLIENT_SRC = c.pk_client and (m.feature = 'AWARD_LEVELS'))
select PK_AWARD_TYPE,
       client.pk_client as fk_client,
       DELIVERY_TYPE,
       ISRANGE,
       MINVALUE,
       MAXVALUE,
       CURRENCY,
       BILLING_CURRENCY,
       AWARD_NAME,
       DISPLAY_PROPS_ID,
       SORT_ORDER,
       TIER,
       NOTIFICATION,
       APPROVAL,
       IS_CASH,
       FK_PERSON_AWARDS,
       FK_CLIENT_PROGRAMS,
       MAX_WIZARD_TOTAL,
       IS_RECOGNITION,
       IDEAL_DELAY_CASH,
       IDEAL_DELAY_PHYSICAL,
       IDEAL_DELAY_DIGITAL,
       FK_AWARD_PRINT_TEMPLATE,
       IS_VIDEO_ENABLED,
       IS_LIVE,
       IS_PRIVATE,
       IS_CUSTOM_DELIVERY_DATE,
       ACTIONABLE_COEFFICIENT,
       IS_BIRTHDAY,
       IS_COUNTRY_SPECIFIC,
       IS_LINKED_TO_REASON,
       IS_FULLSCREEN,
       FK_INVOICE_GROUP,
       AWARD_PLUS,
       NOTIFY_COLLEAGUES,
       IS_GALLERY_ENABLED,
       IS_ENABLED
from gg_award_types at,
     v_client_details_004 client
where at.fK_CLIENT = client.config_client;


