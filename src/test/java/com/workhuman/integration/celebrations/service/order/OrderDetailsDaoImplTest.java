package com.workhuman.integration.celebrations.service.order;


import com.github.database.rider.core.api.dataset.DataSet;
import com.workhuman.integration.celebrations.service.RepositoryTestBase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.*;

@Sql({
        "/db/schema/GG_AWARD_HISTORY.sql",
        "/db/schema/GG_PENDING_ORDERS.sql",
        "/db/schema/GG_YOS_AWARD.sql",
        "/db/schema/GG_INBOX.sql",
        "/db/schema/GG_MS_PERSON.sql",
        "/db/schema/GG_MS_CLIENTS.sql",
        "/db/schema/GG_CONFIG_META.sql",
        "/db/schema/18_gg_award_types.sql",
        "/db/schema/342_gg_media.sql",
        "/db/schema/59_gg_jubilee.sql",
        "/db/schema/88_gg_jubilee_notifications.sql",
        "/db/schema/522_rec_celebrations_denorm.sql",
        "/db/schema/501_gg_media_award_gallery.sql",
        "/db/schema/rec_celebrations_denorm_seq.sql",
        "/db/schema/11152_yos_inheritance_views.sql",
        "/db/schema/V_AWARD_TYPES.sql"
})
@DataSet(value = {"/db/datasets/OrderDetailsDaoImplTest.xml"})
public class OrderDetailsDaoImplTest extends RepositoryTestBase {

    private final DataSource dataSource;

    private OrderDetailsDaoImpl dao;

    private static final long CLIENT_ID = 10000L;

    @Autowired
    public OrderDetailsDaoImplTest(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @BeforeEach
    public void setUp() {
        this.dao = new OrderDetailsDaoImpl(dataSource);
    }

    /**
     * Record is found
     */
    @Test
    public void findByGlbcertId_Success() {
        final String glbcertId = "GLBCERT1";

        final OrderDetailsTO order = dao.findByGlbcertId(CLIENT_ID, glbcertId);

        assertEquals(glbcertId, order.getGlbcertUniqueId());
        assertEquals(2, order.getBaoBatchOutId());
        assertEquals(670, order.getProductId().longValue());
    }

    /**
     * Record does not exist
     */
    @Test
    public void findByGlbcertId_NotFound() {
        final String glbcertId = "GLBCERT2";

        final OrderDetailsTO order = dao.findByGlbcertId(CLIENT_ID, glbcertId);

        assertNull(order);
    }

    /**
     * Record is found
     */
    @Test
    public void findByOrderId_Success() {
        final long orderId = 1;

        final OrderDetailsTO order = dao.findByOrderId(CLIENT_ID, Long.toString(orderId));

        assertEquals("GLBCERT1", order.getGlbcertUniqueId());
        assertEquals(2, order.getBaoBatchOutId());
        assertEquals(670, order.getProductId().longValue());
    }

    /**
     * Record does not exist
     */
    @Test
    public void findByOrderId_NotFound() {
        final long orderId = 2;

        final OrderDetailsTO order = dao.findByOrderId(CLIENT_ID, Long.toString(orderId));

        assertNull(order);
    }

    @Test
    public void findProductId_success() {
        final String glbcertId = "GLBCERT1";

        final Long productId = dao.findProductId(CLIENT_ID, glbcertId);
        assertNotNull(productId);
        assertEquals(670, productId.longValue());
    }
}
