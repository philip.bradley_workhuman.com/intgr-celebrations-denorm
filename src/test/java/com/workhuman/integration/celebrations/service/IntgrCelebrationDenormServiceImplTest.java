package com.workhuman.integration.celebrations.service;

import com.workhuman.integration.celebrations.service.award.AwardDetailsTO;
import com.workhuman.integration.celebrations.service.celebration.CelebrationAwardDenormServiceImpl;
import com.workhuman.integration.celebrations.service.celebration.IntgrCelebrationDenormVO;
import com.workhuman.integration.celebrations.service.exception.IntgrCelebrationDenormServiceException;
import com.workhuman.integration.celebrations.service.exception.IntgrCelebrationDenormUpdateDataException;
import com.workhuman.integration.celebrations.service.person.PersonService;
import com.workhuman.integration.celebrations.service.person.PersonTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class IntgrCelebrationDenormServiceImplTest {

    @InjectMocks
    IntgrCelebrationDenormServiceImpl intgrCelebrationDenormService;

    @Mock
    CelebrationAwardDenormServiceImpl celebrationAwardDenormService;

    @Mock
    PersonService personService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_denormalizeAwardCreation_success() {
        final Long recipientId = 123456l;
        long clientId = 901l;
        int rowsAdded = 1;

        AwardDetailsTO awardDetails = new AwardDetailsTO();
        awardDetails.setPkRecipient(recipientId);
        awardDetails.setPkClient(clientId);

        PersonTO personTO = new PersonTO();
        personTO.setPkPerson(recipientId);
        personTO.setPrivilege(IntgrCelebrationDenormServiceImpl.NEWS_FEEDER_PRIVILEGE);

        when(personService.findPersonTO(recipientId, clientId)).thenReturn(personTO);
        when(celebrationAwardDenormService.add(any(IntgrCelebrationDenormVO.class))).thenReturn(rowsAdded);

        intgrCelebrationDenormService.denormalizeAwardCreation(awardDetails);

        verify(celebrationAwardDenormService, atLeastOnce()).add(any(IntgrCelebrationDenormVO.class));
        verify(personService, atLeastOnce()).findPersonTO(any(Long.class), any(Long.class));
    }

    @Test
    public void test_denormalizeAwardCreation_exception() {
        final Long recipientId = 123456l;
        long clientId = 901l;

        AwardDetailsTO awardDetails = new AwardDetailsTO();
        awardDetails.setPkRecipient(recipientId);
        awardDetails.setPkClient(clientId);
        awardDetails.setTimelines(true);

        PersonTO personTO = new PersonTO();
        personTO.setPkPerson(recipientId);
        personTO.setPrivilege(IntgrCelebrationDenormServiceImpl.NEWS_FEEDER_PRIVILEGE);

        when(personService.findPersonTO(recipientId, clientId)).thenReturn(personTO);
        when(celebrationAwardDenormService.add(any(IntgrCelebrationDenormVO.class))).thenThrow(IntgrCelebrationDenormServiceException.class);

        IntgrCelebrationDenormServiceException thrown = assertThrows(IntgrCelebrationDenormServiceException.class, () -> {
            intgrCelebrationDenormService.denormalizeAwardCreation(awardDetails);
        });

    }


    @Test
    public void test_denormalizeAwardCancellation_success() {
        int rowsUpdated = 1;
        AwardDetailsTO awardDetails = mock(AwardDetailsTO.class);

        when(celebrationAwardDenormService.delete(any(IntgrCelebrationDenormVO.class))).thenReturn(rowsUpdated);

        intgrCelebrationDenormService.denormalizeAwardCancellation(awardDetails);

        verify(celebrationAwardDenormService).delete(any(IntgrCelebrationDenormVO.class));
    }

    @Test
    public void test_denormalizeAwardCancellation_failure() {
        int rowsUpdated = 0;
        AwardDetailsTO awardDetails = mock(AwardDetailsTO.class);
        when(celebrationAwardDenormService.delete(any(IntgrCelebrationDenormVO.class))).thenReturn(rowsUpdated);
        try {
            intgrCelebrationDenormService.denormalizeAwardCancellation(awardDetails);
        } catch (IntgrCelebrationDenormUpdateDataException e) {
            fail("Event consumed when award details are not available anymore does not cause exception");
        }
    }

    @Test
    public void test_denormalizeAwardPrivacyChanged_success() {
        int rowsUpdated = 1;
        AwardDetailsTO awardDetails = mock(AwardDetailsTO.class);

        when(celebrationAwardDenormService.updatePrivacyLevel(any(IntgrCelebrationDenormVO.class))).thenReturn(rowsUpdated);

        intgrCelebrationDenormService.denormalizeAwardPrivacyChanged(awardDetails);
        verify(celebrationAwardDenormService).updatePrivacyLevel(any(IntgrCelebrationDenormVO.class));
    }

    @Test
    public void test_denormalizeAwardPrivacyChanged_error() {
        AwardDetailsTO awardDetails = mock(AwardDetailsTO.class);

        when(celebrationAwardDenormService.updatePrivacyLevel(any(IntgrCelebrationDenormVO.class))).thenThrow(IntgrCelebrationDenormServiceException.class);


        IntgrCelebrationDenormServiceException thrown = assertThrows(IntgrCelebrationDenormServiceException.class, () -> {
            intgrCelebrationDenormService.denormalizeAwardPrivacyChanged(awardDetails);
        });
    }

    @Test
    public void test_denormalizeAwardDeliveryDateChanged_success() {
        int rowsUpdated = 1;
        AwardDetailsTO awardDetails = mock(AwardDetailsTO.class);

        when(celebrationAwardDenormService.updateDeliveryDate(any(IntgrCelebrationDenormVO.class))).thenReturn(rowsUpdated);

        intgrCelebrationDenormService.denormalizeAwardDeliveryDateChanged(awardDetails);

        verify(celebrationAwardDenormService).updateDeliveryDate(any(IntgrCelebrationDenormVO.class));
    }

    @Test
    public void test_denormalizeAwardDelivery_success() {
        int rowsUpdated = 1;
        AwardDetailsTO awardDetails = mock(AwardDetailsTO.class);

        when(celebrationAwardDenormService.updateBaoBatchOutId(any(IntgrCelebrationDenormVO.class))).thenReturn(rowsUpdated);

        intgrCelebrationDenormService.denormalizeAwardDelivery(awardDetails);

        verify(celebrationAwardDenormService).updateBaoBatchOutId(any(IntgrCelebrationDenormVO.class));
    }

    @Test
    public void test_denormalizePersonDataloadFinished_success() {
        int rowsUpdated = 1;
        long clientId = 901l;

        when(celebrationAwardDenormService.updateRecipientsByClientId(eq(clientId))).thenReturn(rowsUpdated);

        intgrCelebrationDenormService.denormalizePersonDataloadFinished(clientId);

        verify(celebrationAwardDenormService).updateRecipientsByClientId(eq(clientId));
    }

    @Test
    public void test_denormalizeAwardGalleryUpdated_success() {
        final int rowsUpdated = 1;
        final long clientId = 901L;
        final String mediaEntryId = "x_asdf";
        final long linkId = 1111L;

        when(celebrationAwardDenormService.updateGroupAwardGallery(clientId, linkId, mediaEntryId))
                .thenReturn(rowsUpdated);

        intgrCelebrationDenormService.denormalizeGroupAwardGalleryUpdated(clientId, linkId, mediaEntryId);

        verify(celebrationAwardDenormService).updateGroupAwardGallery(clientId, linkId, mediaEntryId);
    }

    @Test
    public void whenBackFillGroupAwardMediaForNullAward_shouldThrowException() {

        IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> {
            intgrCelebrationDenormService.syncGroupAwardMedia(null);
        });
    }

    @Test
    public void whenBackFillGroupAwardMediaForNonGroupAward_shouldDoNothing() {
        final AwardDetailsTO awardDetails = new AwardDetailsTO();
        awardDetails.setLinkId(null);

        intgrCelebrationDenormService.syncGroupAwardMedia(awardDetails);

        verify(celebrationAwardDenormService, never()).findFirstByLinkId(anyLong());
        verify(celebrationAwardDenormService, never()).updateSingleAwardGallery(anyLong(), anyLong(), anyString());
    }

    @Test
    public void whenBackFillGroupAwardMediaAndItsTheFirstAward_shouldDoNothing() {
        final long linkId = 1234L;
        final AwardDetailsTO awardDetails = new AwardDetailsTO();
        awardDetails.setLinkId(linkId);

        when(celebrationAwardDenormService.findFirstByLinkId(linkId))
                .thenReturn(null);

        intgrCelebrationDenormService.syncGroupAwardMedia(awardDetails);

        verify(celebrationAwardDenormService, atLeastOnce()).findFirstByLinkId(linkId);
        verify(celebrationAwardDenormService, never()).updateSingleAwardGallery(anyLong(), anyLong(), anyString());
    }

    @Test
    public void whenBackFillGroupAwardMediaAndExistingAwardHasTheSameMedia_shouldDoNothing() {
        final long linkId = 1234L;
        final String mediaEntryId = "q_qwert";
        final AwardDetailsTO awardDetails = new AwardDetailsTO();
        awardDetails.setLinkId(linkId);
        awardDetails.setMediaEntryId(mediaEntryId);

        final IntgrCelebrationDenormVO celebrationDenormAward = new IntgrCelebrationDenormVO();
        celebrationDenormAward.setMediaEntryId(mediaEntryId);

        when(celebrationAwardDenormService.findFirstByLinkId(linkId))
                .thenReturn(celebrationDenormAward);

        intgrCelebrationDenormService.syncGroupAwardMedia(awardDetails);

        verify(celebrationAwardDenormService, atLeastOnce()).findFirstByLinkId(linkId);
        verify(celebrationAwardDenormService, never()).updateSingleAwardGallery(anyLong(), anyLong(), anyString());
    }

    @Test
    public void whenBackFillGroupAwardMediaAndExistingAwardHasNoMedia_shouldRemoveMedia() {
        final long linkId = 1234L;
        final String newAwardMediaEntryId = "q_asdf";
        final String existingGroupAwardMediaEntryId = null;
        final long clientId = 33L;
        final long awardHistoryId = 1234L;

        final AwardDetailsTO awardDetails = new AwardDetailsTO();
        awardDetails.setLinkId(linkId);
        awardDetails.setMediaEntryId(newAwardMediaEntryId);
        awardDetails.setPkClient(clientId);
        awardDetails.setPkAwardHistory(awardHistoryId);

        final IntgrCelebrationDenormVO celebrationDenormAward = new IntgrCelebrationDenormVO();
        celebrationDenormAward.setMediaEntryId(existingGroupAwardMediaEntryId);

        when(celebrationAwardDenormService.findFirstByLinkId(linkId))
                .thenReturn(celebrationDenormAward);

        when(celebrationAwardDenormService.updateSingleAwardGallery(clientId, awardHistoryId, existingGroupAwardMediaEntryId))
                .thenReturn(1);

        intgrCelebrationDenormService.syncGroupAwardMedia(awardDetails);

        verify(celebrationAwardDenormService, atLeastOnce()).findFirstByLinkId(linkId);
        verify(celebrationAwardDenormService, atLeastOnce()).updateSingleAwardGallery(clientId, awardHistoryId, existingGroupAwardMediaEntryId);
    }

    @Test
    public void whenBackFillGroupAwardMediaAndExistingAwardHasDifferentMedia_shouldUpdateNewAwardWithExistingMedia() {
        final long linkId = 1234L;
        final String newAwardMediaEntryId = "q_asdf";
        final String existingGroupAwardMediaEntryId = "q_qewrt";
        final long clientId = 33L;
        final long awardHistoryId = 1234L;

        final AwardDetailsTO awardDetails = new AwardDetailsTO();
        awardDetails.setLinkId(linkId);
        awardDetails.setMediaEntryId(newAwardMediaEntryId);
        awardDetails.setPkClient(clientId);
        awardDetails.setPkAwardHistory(awardHistoryId);

        final IntgrCelebrationDenormVO celebrationDenormAward = new IntgrCelebrationDenormVO();
        celebrationDenormAward.setMediaEntryId(existingGroupAwardMediaEntryId);

        when(celebrationAwardDenormService.findFirstByLinkId(linkId))
                .thenReturn(celebrationDenormAward);

        when(celebrationAwardDenormService.updateSingleAwardGallery(clientId, awardHistoryId, existingGroupAwardMediaEntryId))
                .thenReturn(1);

        intgrCelebrationDenormService.syncGroupAwardMedia(awardDetails);

        verify(celebrationAwardDenormService, atLeastOnce()).findFirstByLinkId(linkId);
        verify(celebrationAwardDenormService, atLeastOnce()).updateSingleAwardGallery(clientId, awardHistoryId, existingGroupAwardMediaEntryId);
    }
}
