package com.workhuman.integration.celebrations.service.celebration;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class CelebrationAwardDenormServiceImplTest {

    @InjectMocks
    CelebrationAwardDenormServiceImpl celebrationAwardDenormService;

    @Mock
    CelebrationAwardDenormDao dao;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_find_by_award_history_id() {
        long awardHistoryId = 123456l;
        celebrationAwardDenormService.findByAwardHistoryId(awardHistoryId);
        verify(dao).findByAwardHistoryId(awardHistoryId);
    }

    @Test
    public void test_find_by_link_id() {
        final long linkId = 123456L;
        celebrationAwardDenormService.findFirstByLinkId(linkId);
        verify(dao).findFirstByLinkId(linkId);
    }

    @Test
    public void test_add() {
        IntgrCelebrationDenormVO celebrationDenormVO = mock(IntgrCelebrationDenormVO.class);
        celebrationAwardDenormService.add(celebrationDenormVO);
        verify(dao).add(celebrationDenormVO);
    }

    @Test
    public void test_update_privacy_level() {
        IntgrCelebrationDenormVO celebrationDenormVO = mock(IntgrCelebrationDenormVO.class);
        celebrationAwardDenormService.updatePrivacyLevel(celebrationDenormVO);
        verify(dao).updatePrivacyLevel(celebrationDenormVO);
    }

    @Test
    public void test_update_delivery_date() {
        IntgrCelebrationDenormVO celebrationDenormVO = mock(IntgrCelebrationDenormVO.class);
        celebrationAwardDenormService.updateDeliveryDate(celebrationDenormVO);
        verify(dao).updateDeliveryDate(celebrationDenormVO);
    }

    @Test
    public void test_update_bao_batch_out_id() {
        IntgrCelebrationDenormVO celebrationDenormVO = mock(IntgrCelebrationDenormVO.class);
        celebrationAwardDenormService.updateBaoBatchOutId(celebrationDenormVO);
        verify(dao).updateBaoBatchOutId(celebrationDenormVO);
    }

    @Test
    public void test_delete() {
        IntgrCelebrationDenormVO celebrationDenormVO = mock(IntgrCelebrationDenormVO.class);
        celebrationAwardDenormService.delete(celebrationDenormVO);
        verify(dao).delete(celebrationDenormVO);
    }

    @Test
    public void test_update_recipient_by_client_id() {
        long clientId = 901l;
        celebrationAwardDenormService.updateRecipientsByClientId(clientId);
        verify(dao).updateRecipientsByClientId(clientId);
    }

    @Test
    public void test_update_single_award_gallery_award_history_id() {
        final long clientId = 901L;
        final long awardHistoryId = 1L;
        final String mediaEntryId = "x_asdf";

        celebrationAwardDenormService.updateSingleAwardGallery(clientId, awardHistoryId, mediaEntryId);
        verify(dao).updateSingleAwardMediaEntryId(clientId, awardHistoryId, mediaEntryId);
    }

    @Test
    public void test_update_award_gallery_by_link_id() {
        final long clientId = 901L;
        final long linkId = 1L;
        final String mediaEntryId = "x_asdf";

        celebrationAwardDenormService.updateGroupAwardGallery(clientId, linkId, mediaEntryId);
        verify(dao).updateGroupAwardMediaEntryId(clientId, linkId, mediaEntryId);
    }
}
