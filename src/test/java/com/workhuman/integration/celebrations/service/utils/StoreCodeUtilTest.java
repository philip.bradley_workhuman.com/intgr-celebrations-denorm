package com.workhuman.integration.celebrations.service.utils;


import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class StoreCodeUtilTest {

    @Test
    public void whenGetStoreCode_shouldReturnValidClientStoreCode() {
        assertThat(StoreCodeUtil.getStoreCode(1L), is("INTCOR0001"));
        assertThat(StoreCodeUtil.getStoreCode(10L), is("INTCOR0010"));
        assertThat(StoreCodeUtil.getStoreCode(100L), is("INTCOR0100"));
        assertThat(StoreCodeUtil.getStoreCode(1000L), is("INTCOR1000"));

        assertThat(StoreCodeUtil.getStoreCode(9999L), is("INTCOR9999"));

        assertThat(StoreCodeUtil.getStoreCode(10000L), is("INTCOR10000"));
        assertThat(StoreCodeUtil.getStoreCode(100000L), is("INTCOR100000"));
        assertThat(StoreCodeUtil.getStoreCode(1000000L), is("INTCOR1000000"));
        assertThat(StoreCodeUtil.getStoreCode(10000000L), is("INTCOR10000000"));
    }
}
