package com.workhuman.integration.celebrations.service.media;

import com.workhuman.integration.celebrations.service.celebration.IntgrCelebrationDenormVO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class MediaValidatorImplTest {

    @InjectMocks
    private MediaValidatorImpl mediaValidator;

    @Mock
    private MediaDetailsService mediaDetailsService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenMediaEntryIsNull_mediaEntryDoesNotBelongToAwardGalleryShouldThrowException() {
        IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> {
            mediaValidator.mediaEntryDoesNotBelongToAwardGallery(null);
        });
    }

    @Test
    public void whenMediaDetailsDoesNotExist_mediaEntryDoesNotBelongToAwardGalleryShouldReturnFalse() {
        final String mediaEntryId = "1_asdf";
        when(mediaDetailsService.getMediaDetails(mediaEntryId))
                .thenReturn(null);

        assertFalse(mediaValidator.mediaEntryDoesNotBelongToAwardGallery(mediaEntryId));
    }

    @Test
    public void whenMediaDetailsAwardHistoryDoesNotExist_mediaEntryDoesNotBelongToAwardGalleryShouldReturnFalse() {
        final String mediaEntryId = "1_asdf";
        final MediaDetailsTO mediaDetails = mock(MediaDetailsTO.class);
        when(mediaDetails.getFkAwardHistory()).thenReturn(null);
        when(mediaDetails.getGroupId()).thenReturn(12345L);

        when(mediaDetailsService.getMediaDetails(mediaEntryId))
                .thenReturn(mediaDetails);

        assertFalse(mediaValidator.mediaEntryDoesNotBelongToAwardGallery(mediaEntryId));
    }

    @Test
    public void whenMediaDetailsGroupIdDoesNotExist_mediaEntryDoesNotBelongToAwardGalleryShouldReturnFalse() {
        final String mediaEntryId = "1_asdf";
        final MediaDetailsTO mediaDetails = mock(MediaDetailsTO.class);
        when(mediaDetails.getFkAwardHistory()).thenReturn(1234L);
        when(mediaDetails.getGroupId()).thenReturn(null);

        when(mediaDetailsService.getMediaDetails(mediaEntryId))
                .thenReturn(mediaDetails);

        assertFalse(mediaValidator.mediaEntryDoesNotBelongToAwardGallery(mediaEntryId));
    }

    @Test
    public void whenMediaDetailsGroupIdAndAwardHistoryExist_mediaEntryDoesNotBelongToAwardGalleryShouldReturnFalse() {
        final String mediaEntryId = "1_asdf";
        final MediaDetailsTO mediaDetails = mock(MediaDetailsTO.class);
        when(mediaDetails.getFkAwardHistory()).thenReturn(1234L);
        when(mediaDetails.getGroupId()).thenReturn(222L);

        when(mediaDetailsService.getMediaDetails(mediaEntryId))
                .thenReturn(mediaDetails);

        assertFalse(mediaValidator.mediaEntryDoesNotBelongToAwardGallery(mediaEntryId));
    }

    @Test
    public void whenMediaDetailsGroupIdAndAwardHistoryDoesNotExist_mediaEntryDoesNotBelongToAwardGalleryShouldReturnTrue() {
        final String mediaEntryId = "1_asdf";
        final MediaDetailsTO mediaDetails = mock(MediaDetailsTO.class);
        when(mediaDetails.getFkAwardHistory()).thenReturn(null);
        when(mediaDetails.getLinkId()).thenReturn(null);

        when(mediaDetailsService.getMediaDetails(mediaEntryId))
                .thenReturn(mediaDetails);

        assertTrue(mediaValidator.mediaEntryDoesNotBelongToAwardGallery(mediaEntryId));
    }

    @Test
    public void whenDenormalizedAwardMediaEntryIsNull_isFirstAwardMediaShouldReturnTrue() {
        final IntgrCelebrationDenormVO denormalizedAward = mock(IntgrCelebrationDenormVO.class);
        when(denormalizedAward.getMediaEntryId()).thenReturn(null);

        assertTrue(mediaValidator.isRemovedMediaNotTheFirstAwardMedia("x_asdf", denormalizedAward));
    }

    @Test
    public void whenDenormalizedAwardMediaEntryIsTheSame_isFirstAwardMediaShouldReturnFalse() {
        final IntgrCelebrationDenormVO denormalizedAward = mock(IntgrCelebrationDenormVO.class);
        final String entryMediaId = "x_asdf";
        when(denormalizedAward.getMediaEntryId()).thenReturn(entryMediaId);

        assertFalse(mediaValidator.isRemovedMediaNotTheFirstAwardMedia(entryMediaId, denormalizedAward));
    }

    @Test
    public void whenDenormalizedAwardMediaEntryIsNotTheSame_isFirstAwardMediaShouldReturnTrue() {
        final IntgrCelebrationDenormVO denormalizedAward = mock(IntgrCelebrationDenormVO.class);
        final String entryMediaId = "x_asdf";
        when(denormalizedAward.getMediaEntryId()).thenReturn("y_asdf");

        assertTrue(mediaValidator.isRemovedMediaNotTheFirstAwardMedia(entryMediaId, denormalizedAward));
    }
}
