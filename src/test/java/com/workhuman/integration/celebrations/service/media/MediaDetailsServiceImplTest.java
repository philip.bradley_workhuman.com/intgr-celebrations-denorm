package com.workhuman.integration.celebrations.service.media;

import com.workhuman.integration.celebrations.service.celebration.IntgrCelebrationDenormVO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

public class MediaDetailsServiceImplTest {

    @InjectMocks
    MediaDetailsServiceImpl mediaDetailsService;

    @Mock
    private MediaDetailsDao dao;

    private static final String MEDIA_ENTRY_ID = "1_g9gr5ma4";

    private static final String NOMINATION_MEDIA_ENTRY_ID = "1_xxxxxx";

    private static final String AWARD_GALLERY_MEDIA_ENTRY_ID = "1_yyyyyyy";

    private static final Long AWARD_HISTORY_ID = 51214858L;

    private static final Long LINK_ID = 9999L;

    private static final Long USER_ID = 11357772L;

    private static final Long CLIENT_ID = 5022L;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_find_media_details_success() {
        final MediaDetailsTO mediaDetails = new MediaDetailsTO();
        mediaDetails.setClientId(CLIENT_ID);
        mediaDetails.setCreatorId(USER_ID);
        mediaDetails.setFkAwardHistory(AWARD_HISTORY_ID);
        mediaDetails.setEntryId(MEDIA_ENTRY_ID);

        when(dao.getMediaDetails(MEDIA_ENTRY_ID)).thenReturn(mediaDetails);

        final MediaDetailsTO actualMediaDetails = mediaDetailsService.getMediaDetails(MEDIA_ENTRY_ID);

        assertNotNull(actualMediaDetails);
    }

    @Test
    public void test_find_no_media_details_success() {
        final MediaDetailsTO mediaDetails = null;

        when(dao.getMediaDetails(MEDIA_ENTRY_ID)).thenReturn(mediaDetails);

        final MediaDetailsTO actualMediaDetails = mediaDetailsService.getMediaDetails(MEDIA_ENTRY_ID);

        assertNull(actualMediaDetails);
    }

    @Test
    public void whenNominationMediaDoesntExists_findFirstMediaReturnsNominationMedia() {
        final IntgrCelebrationDenormVO denormalizedAward = mock(IntgrCelebrationDenormVO.class);
        when(denormalizedAward.getAwardHistoryId()).thenReturn(AWARD_HISTORY_ID);
        when(denormalizedAward.getLinkId()).thenReturn(null);

        when(dao.findNominationMediaEntryId(AWARD_HISTORY_ID))
                .thenReturn(NOMINATION_MEDIA_ENTRY_ID);

        final String actualFirstMediaEntryId = mediaDetailsService.findFirstMediaEntryId(denormalizedAward);

        assertNotNull(actualFirstMediaEntryId);

        verify(dao, never()).findFirstMediaGalleryEntryId(anyLong());
        verify(dao, never()).findFirstMediaGalleryEntryIdByLinkId(anyLong());
    }

    @Test
    public void whenNominationMediaExistsAndIsNotGroupAward_findFirstMediaReturnsAwardGalleryMedia() {
        final IntgrCelebrationDenormVO denormalizedAward = mock(IntgrCelebrationDenormVO.class);
        when(denormalizedAward.getAwardHistoryId()).thenReturn(AWARD_HISTORY_ID);
        when(denormalizedAward.getLinkId()).thenReturn(null);

        when(dao.findNominationMediaEntryId(AWARD_HISTORY_ID))
                .thenReturn(null);

        when(dao.findFirstMediaGalleryEntryId(AWARD_HISTORY_ID))
                .thenReturn(AWARD_GALLERY_MEDIA_ENTRY_ID);

        final String actualFirstMediaEntryId = mediaDetailsService.findFirstMediaEntryId(denormalizedAward);

        assertNotNull(actualFirstMediaEntryId);

        verify(dao, atLeastOnce()).findFirstMediaGalleryEntryId(anyLong());
        verify(dao, never()).findFirstMediaGalleryEntryIdByLinkId(anyLong());
    }

    @Test
    public void whenNominationMediaExistsAndIsGroupAward_findFirstMediaReturnsAwardGalleryMediaByLinkId() {
        final IntgrCelebrationDenormVO denormalizedAward = mock(IntgrCelebrationDenormVO.class);
        when(denormalizedAward.getAwardHistoryId()).thenReturn(AWARD_HISTORY_ID);
        when(denormalizedAward.getLinkId()).thenReturn(LINK_ID);

        when(dao.findNominationMediaEntryId(AWARD_HISTORY_ID))
                .thenReturn(null);

        when(dao.findFirstMediaGalleryEntryIdByLinkId(LINK_ID))
                .thenReturn(AWARD_GALLERY_MEDIA_ENTRY_ID);

        final String actualFirstMediaEntryId = mediaDetailsService.findFirstMediaEntryId(denormalizedAward);

        assertNotNull(actualFirstMediaEntryId);

        verify(dao, atLeastOnce()).findFirstMediaGalleryEntryIdByLinkId(anyLong());
        verify(dao, never()).findFirstMediaGalleryEntryId(anyLong());
    }
}
