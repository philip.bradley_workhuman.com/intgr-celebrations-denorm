package com.workhuman.integration.celebrations.service.order;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OrderDetailsServiceImplTest {

    @InjectMocks
    OrderDetailsServiceImpl orderDetailsService;

    @Mock
    private OrderDetailsDao dao;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_find_order_details_by_orderId_success() throws ParseException {
        long clientId = 901l;
        final String indOrderId = "123456";
        Long productId = 680l;

        OrderDetailsTO orderDetailsTO = new OrderDetailsTO();
        orderDetailsTO.setProductId(productId);
        orderDetailsTO.setGlbcertUniqueId("AAA-AAA-AAA");
        orderDetailsTO.setBaoBatchOutId(2);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date idealDate = simpleDateFormat.parse("2018-05-01 15:30:45");
        orderDetailsTO.setIdealDate(idealDate);

        when(dao.findByOrderId(clientId, indOrderId)).thenReturn(orderDetailsTO);

        OrderDetailsTO actualOrderDetails = orderDetailsService.findByOrderId(clientId, indOrderId);
        verify(dao).findByOrderId(eq(clientId), eq(indOrderId));

        assertNotNull(actualOrderDetails);
        assertEquals(actualOrderDetails.getGlbcertUniqueId(), orderDetailsTO.getGlbcertUniqueId());
        assertEquals(actualOrderDetails.getProductId(), orderDetailsTO.getProductId());
        assertEquals(actualOrderDetails.getBaoBatchOutId(), orderDetailsTO.getBaoBatchOutId());
        assertEquals(actualOrderDetails.getIdealDate(), orderDetailsTO.getIdealDate());
    }

    @Test
    public void test_find_order_details_by_glbcertId_success() throws ParseException {
        long clientId = 901l;
        final String glbcertId = "AAA-AAA-AAA";

        OrderDetailsTO orderDetailsTO = new OrderDetailsTO();
        orderDetailsTO.setGlbcertUniqueId(glbcertId);

        when(dao.findByGlbcertId(clientId, glbcertId)).thenReturn(orderDetailsTO);

        OrderDetailsTO actualOrderDetails = orderDetailsService.findByGlbcertId(clientId, glbcertId);

        verify(dao).findByGlbcertId(eq(clientId), eq(glbcertId));
        assertNotNull(actualOrderDetails);
        assertEquals(actualOrderDetails.getGlbcertUniqueId(), orderDetailsTO.getGlbcertUniqueId());
    }

    @Test
    public void test_find_productId_success() throws ParseException {
        long clientId = 901l;
        final String glbcertId = "AAA-AAA-AAA";
        final Long productId = 680l;

        OrderDetailsTO orderDetailsTO = new OrderDetailsTO();
        orderDetailsTO.setGlbcertUniqueId(glbcertId);

        when(dao.findProductId(clientId, glbcertId)).thenReturn(productId);

        Long actualProductId = orderDetailsService.findProductId(clientId, glbcertId);

        verify(dao).findProductId(eq(clientId), eq(glbcertId));
        assertNotNull(actualProductId);
        assertEquals(actualProductId, productId);
    }

}
