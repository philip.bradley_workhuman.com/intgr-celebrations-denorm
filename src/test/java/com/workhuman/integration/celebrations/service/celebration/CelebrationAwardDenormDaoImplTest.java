package com.workhuman.integration.celebrations.service.celebration;


import com.github.database.rider.core.api.dataset.DataSet;
import com.workhuman.integration.celebrations.service.RepositoryTestBase;
import com.workhuman.integration.celebrations.service.exception.IntgrCelebrationDenormDuplicationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.jdbc.Sql;

import javax.sql.DataSource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Sql({
        "/db/schema/GG_AWARD_HISTORY.sql",
        "/db/schema/GG_PENDING_ORDERS.sql",
        "/db/schema/GG_YOS_AWARD.sql",
        "/db/schema/GG_INBOX.sql",
        "/db/schema/GG_MS_PERSON.sql",
        "/db/schema/GG_MS_CLIENTS.sql",
        "/db/schema/GG_CONFIG_META.sql",
        "/db/schema/18_gg_award_types.sql",
        "/db/schema/342_gg_media.sql",
        "/db/schema/59_gg_jubilee.sql",
        "/db/schema/88_gg_jubilee_notifications.sql",
        "/db/schema/522_rec_celebrations_denorm.sql",
        "/db/schema/rec_celebrations_denorm_seq.sql",
        "/db/schema/11152_yos_inheritance_views.sql",
        "/db/schema/V_AWARD_TYPES.sql"
})
@DataSet(value = {"/db/datasets/CelebrationAwardDenormDaoImplTest.xml"})
public class CelebrationAwardDenormDaoImplTest extends RepositoryTestBase {

    private final DataSource dataSource;

    private CelebrationAwardDenormDao dao;

    @Autowired
    public CelebrationAwardDenormDaoImplTest(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @BeforeEach
    public void setUp() {
        dao = new CelebrationAwardDenormDaoImpl(dataSource);
    }

    @Test
    public void find_SM_success() {
        long clientId = 100l;
        long awardHistoryId = 10002l;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        IntgrCelebrationDenormVO celebrationDenormVO = dao.findByAwardHistoryId(awardHistoryId);
        assertNotNull(celebrationDenormVO);
        assertEquals(new Long(2l), celebrationDenormVO.getId());
        assertEquals(new Long(100l), celebrationDenormVO.getClientId());
        assertEquals(new Long(10002l), celebrationDenormVO.getAwardHistoryId());
        assertEquals("BBBB-BBBB-BBBB", celebrationDenormVO.getGlbcertUniqueId());
        assertEquals(new Long(1002l), celebrationDenormVO.getRecipientId());
        assertEquals(new Integer(2), celebrationDenormVO.getBaoBatchOutId());
        assertEquals(new Integer(2), celebrationDenormVO.getYosYears());
        assertEquals("Peter", celebrationDenormVO.getRecipientFirstname());
        assertEquals("Green", celebrationDenormVO.getRecipientLastname());
        assertEquals("YOS", celebrationDenormVO.getAwardReasonCode());
        assertTrue(celebrationDenormVO.isRecipientHasNewsfeedPrivilege());
        assertTrue(celebrationDenormVO.isTimelines());
        assertFalse(celebrationDenormVO.isAwardPrivate());
        assertEquals(new Long(502l), celebrationDenormVO.getAwardTypeId());
        assertNull(celebrationDenormVO.getMediaEntryId());
        assertNull(celebrationDenormVO.getMediaType());

        String deliveryDate = simpleDateFormat.format(celebrationDenormVO.getDeliveryDate());
        assertEquals("2018-03-01 15:30:45", deliveryDate);

        String createdDate = simpleDateFormat.format(celebrationDenormVO.getCreated());
        assertEquals("2018-02-10 15:30:45", createdDate);

        String modifiedDate = simpleDateFormat.format(celebrationDenormVO.getCreated());
        assertEquals("2018-02-10 15:30:45", modifiedDate);
    }

    @Test
    public void add_notUnique_LE_Failed() throws Exception {
        // Enable constraint back for testing purpose
        //new JdbcTemplate(dataSource).update("ALTER TABLE REC_CELEBRATIONS_DENORM ENABLE CONSTRAINT CELEBRATIONS_GLBCERT_UNIQUE");

        long clientId = 100l;
        long awardHistoryId = 10001l;
        String glbcertUniqueId = "AAAA-AAAA-AAAA";
        long awardTypeId = 501l;
        long recipientId = 1002l;
        String recipientFirstName = "Peter";
        String recipientLastName = "Green";

        IntgrCelebrationDenormVO celebration = new IntgrCelebrationDenormVO();
        celebration.setClientId(clientId);
        celebration.setGlbcertUniqueId(glbcertUniqueId);
        celebration.setAwardHistoryId(awardHistoryId);
        celebration.setRecipientHasNewsfeedPrivilege(true);
        celebration.setAwardTypeId(awardTypeId);
        celebration.setRecipientId(recipientId);
        celebration.setRecipientFirstname(recipientFirstName);
        celebration.setRecipientLastname(recipientLastName);
        celebration.setDeliveryDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2013-10-01 15:30:45"));

        IntgrCelebrationDenormDuplicationException thrown =
                assertThrows(IntgrCelebrationDenormDuplicationException.class,
                        () -> {
                            dao.add(celebration);
                        });

    }

    @Test
    public void add_LE_success() throws Exception {
        long clientId = 100l;
        long awardHistoryId = 10005l;
        String glbcertUniqueId = "CCCC-CCCC-CCCC";
        long awardTypeId = 501l;
        String reason = "REASON2";
        long recipientId = 1002l;
        String recipientFirstName = "Peter";
        String recipientLastName = "Green";
        IntgrCelebrationDenormVO celebration = new IntgrCelebrationDenormVO();
        celebration.setClientId(clientId);
        celebration.setGlbcertUniqueId(glbcertUniqueId);
        celebration.setAwardHistoryId(awardHistoryId);
        celebration.setRecipientHasNewsfeedPrivilege(true);
        celebration.setAwardTypeId(awardTypeId);
        celebration.setAwardReasonCode(reason);
        celebration.setBaoBatchOutId(2);
        celebration.setRecipientId(recipientId);
        celebration.setRecipientFirstname(recipientFirstName);
        celebration.setRecipientLastname(recipientLastName);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date deliveryDate = simpleDateFormat.parse("2013-10-01 15:30:45");
        Date createdDate = simpleDateFormat.parse("2013-10-01 15:30:46");
        Date modifiedDate = simpleDateFormat.parse("2014-10-01 15:30:46");
        celebration.setDeliveryDate(deliveryDate);
        celebration.setCreated(createdDate);
        celebration.setModified(modifiedDate);

        int rowsAdded = dao.add(celebration);

        assertEquals(1, rowsAdded);
        IntgrCelebrationDenormVO celebrationDenormVO = dao.findByAwardHistoryId(awardHistoryId);

        assertNotNull(celebrationDenormVO);
        assertEquals(celebration.getId(), celebrationDenormVO.getId());
        assertEquals(celebration.getClientId(), celebrationDenormVO.getClientId());
        assertEquals(celebration.getAwardHistoryId(), celebrationDenormVO.getAwardHistoryId());
        assertEquals(celebration.getGlbcertUniqueId(), celebrationDenormVO.getGlbcertUniqueId());
        assertEquals(celebration.getRecipientId(), celebrationDenormVO.getRecipientId());
        assertEquals(celebration.getBaoBatchOutId(), celebrationDenormVO.getBaoBatchOutId());
        assertEquals(celebration.getRecipientFirstname(), celebrationDenormVO.getRecipientFirstname());
        assertEquals(celebration.getRecipientLastname(), celebrationDenormVO.getRecipientLastname());
        assertEquals(celebration.getAwardReasonCode(), celebrationDenormVO.getAwardReasonCode());
        assertTrue(celebration.isRecipientHasNewsfeedPrivilege());
        assertFalse(celebrationDenormVO.isTimelines());
        assertFalse(celebrationDenormVO.isAwardPrivate());
        assertNull(celebrationDenormVO.getMediaEntryId());
        assertEquals(celebration.getAwardTypeId(), celebrationDenormVO.getAwardTypeId());
        assertEquals(celebration.getDeliveryDate(), celebrationDenormVO.getDeliveryDate());
    }

    @Test
    public void update_privacy_level_success() {
        long clientId = 100l;
        long awardHistoryId = 10001l;

        IntgrCelebrationDenormVO celebration = new IntgrCelebrationDenormVO();
        celebration.setClientId(clientId);
        celebration.setAwardHistoryId(awardHistoryId);
        celebration.setAwardPrivate(true);

        int rowsUpdated = dao.updatePrivacyLevel(celebration);
        assertEquals(1, rowsUpdated);

        IntgrCelebrationDenormVO celebrationDenormVO = dao.findByAwardHistoryId(awardHistoryId);
        assertNotNull(celebrationDenormVO);
        assertEquals(celebration.getClientId(), celebrationDenormVO.getClientId());
        assertEquals(celebration.getAwardHistoryId(), celebrationDenormVO.getAwardHistoryId());
        assertTrue(celebration.isAwardPrivate());
    }

    @Test
    public void update_delivery_date_success() throws ParseException {
        long clientId = 100l;
        long awardHistoryId = 10001l;
        String glbcertUniqueId = "AAAA-AAAA-AAAA";

        IntgrCelebrationDenormVO celebration = new IntgrCelebrationDenormVO();
        celebration.setClientId(clientId);
        celebration.setGlbcertUniqueId(glbcertUniqueId);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date deliveryDate = simpleDateFormat.parse("2018-05-01 15:30:45");
        celebration.setDeliveryDate(deliveryDate);

        int rowsUpdated = dao.updateDeliveryDate(celebration);
        assertEquals(1, rowsUpdated);

        IntgrCelebrationDenormVO celebrationDenormVO = dao.findByAwardHistoryId(awardHistoryId);
        assertNotNull(celebrationDenormVO);
        assertEquals(celebration.getClientId(), celebrationDenormVO.getClientId());
        assertEquals(celebration.getGlbcertUniqueId(), celebrationDenormVO.getGlbcertUniqueId());
        assertEquals(celebration.getDeliveryDate(), celebrationDenormVO.getDeliveryDate());
    }

    @Test
    public void update_baoBatchOutId_success() throws ParseException {
        long clientId = 100l;
        long awardHistoryId = 10001l;
        String glbcertUniqueId = "AAAA-AAAA-AAAA";

        IntgrCelebrationDenormVO celebration = new IntgrCelebrationDenormVO();
        celebration.setClientId(clientId);
        celebration.setGlbcertUniqueId(glbcertUniqueId);
        celebration.setBaoBatchOutId(2);

        int rowsUpdated = dao.updateBaoBatchOutId(celebration);
        assertEquals(1, rowsUpdated);

        IntgrCelebrationDenormVO celebrationDenormVO = dao.findByAwardHistoryId(awardHistoryId);
        assertNotNull(celebrationDenormVO);
        assertEquals(celebration.getClientId(), celebrationDenormVO.getClientId());
        assertEquals(celebration.getGlbcertUniqueId(), celebrationDenormVO.getGlbcertUniqueId());
        assertEquals(celebration.getBaoBatchOutId(), celebrationDenormVO.getBaoBatchOutId());
    }

    @Test
    public void update_recipients_success() {
        long clientId = 100l;
        int rowsUpdated = dao.updateRecipientsByClientId(clientId);
        assertEquals(4, rowsUpdated);
    }

    @Test
    public void update_group_award_delivery_date_success() {
        long clientId = 100L;
        long linkId = 123L;
        int rowsUpdated = dao.updateGroupAwardDeliveryDate(clientId, linkId);
        assertEquals(2, rowsUpdated);
    }

    @Test
    public void delete_success() throws ParseException {
        long clientId = 100l;
        long awardHistoryId = 10001l;
        String glbcertUniqueId = "AAAA-AAAA-AAAA";

        IntgrCelebrationDenormVO celebration = new IntgrCelebrationDenormVO();
        celebration.setClientId(clientId);
        celebration.setGlbcertUniqueId(glbcertUniqueId);
        celebration.setAwardHistoryId(awardHistoryId);

        int rowsUpdated = dao.delete(celebration);
        assertEquals(1, rowsUpdated);

        IntgrCelebrationDenormVO celebrationDenormVO = dao.findByAwardHistoryId(awardHistoryId);
        assertNull(celebrationDenormVO);
    }

    @Test
    public void whenFindFirstByNonExistingLinkId_shouldThrowException() {
        EmptyResultDataAccessException thrown =
                assertThrows(EmptyResultDataAccessException.class,
                        () -> {
                            dao.add(dao.findFirstByLinkId(9999L));
                        });

    }

    @Test
    public void whenFindFirstByExistingLinkId_shouldReturnFirstAward() {
        final IntgrCelebrationDenormVO award = dao.findFirstByLinkId(1L);
        assertEquals(11L, award.getId().longValue());
    }

    @Test
    public void whenFindByNonExistingLinkId_shouldReturnEmptyList() {
        final List<IntgrCelebrationDenormVO> awards = dao.findByLinkId(9999L);
        assertTrue(awards.isEmpty());
    }

    @Test
    public void whenFindByExistingLinkId_shouldReturnListContaining3Awards() {
        final List<IntgrCelebrationDenormVO> awards = dao.findByLinkId(1L);
        assertEquals(3, awards.size());
        assertEquals(11L, awards.get(0).getId().longValue());
        assertEquals(12L, awards.get(1).getId().longValue());
        assertEquals(13L, awards.get(2).getId().longValue());
    }

    @Test
    public void whenUpdateMediaForSingleAward_shouldReturnNumberOfRowsUpdated() {
        final long clientId = 100L;
        final long awardHistory = 10001L;
        final String newMediaEntryId = "x_asdf";
        final Integer newMediaType = 2;

        final int rowsUpdated = dao.updateSingleAwardMediaEntryId(clientId, awardHistory, newMediaEntryId);
        assertEquals(1, rowsUpdated);

        final IntgrCelebrationDenormVO award = dao.findByAwardHistoryId(awardHistory);
        assertEquals(10001L, award.getAwardHistoryId().longValue());
        assertEquals(newMediaEntryId, award.getMediaEntryId());
        assertEquals(newMediaType, award.getMediaType());
    }

    @Test
    public void whenUpdateMediaForAwardLinkId_shouldReturnNumberOfRowsUpdated() {
        final long clientId = 101L;
        final long linkId = 1L;
        final String newMediaEntryId = "x_asdf";
        final Integer newMediaType = 2;

        final int rowsUpdated = dao.updateGroupAwardMediaEntryId(clientId, linkId, newMediaEntryId);
        assertEquals(3, rowsUpdated);

        final List<IntgrCelebrationDenormVO> awards = dao.findByLinkId(linkId);
        assertEquals(3, awards.size());

        final IntgrCelebrationDenormVO award1 = awards.get(0);
        assertEquals(11L, award1.getId().longValue());
        assertEquals(newMediaEntryId, award1.getMediaEntryId());
        assertEquals(newMediaType, award1.getMediaType());

        final IntgrCelebrationDenormVO award2 = awards.get(1);
        assertEquals(12L, award2.getId().longValue());
        assertEquals(newMediaEntryId, award2.getMediaEntryId());
        assertEquals(newMediaType, award2.getMediaType());

        final IntgrCelebrationDenormVO award3 = awards.get(2);
        assertEquals(13L, award3.getId().longValue());
        assertEquals(newMediaEntryId, award3.getMediaEntryId());
        assertEquals(newMediaType, award3.getMediaType());
    }

    @Test
    public void add_DuplicateAward() {
        // TODO test exception thrown when trying to insert the same award
    }

    @Test
    public void add_ClientNull() {
        // TODO test exception thrown when inserting null client

    }


}
