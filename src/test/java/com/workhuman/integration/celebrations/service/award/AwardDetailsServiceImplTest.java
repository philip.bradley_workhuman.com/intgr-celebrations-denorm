package com.workhuman.integration.celebrations.service.award;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AwardDetailsServiceImplTest {

    @InjectMocks
    AwardDetailsServiceImpl awardDetailsService;

    @Mock
    private AwardDetailsDao dao;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_find_life_event_award_success() {
       long clientId = 901l;
       final String glbcertId = "A7M4-M6IGKO-FX1CZA";

        AwardDetailsTO awardDetailsTO = new AwardDetailsTO();
        awardDetailsTO.setGlbcertUniqueId(glbcertId);
        awardDetailsTO.setPkClient(clientId);

        when(dao.findLifeEventAward(clientId, glbcertId)).thenReturn(awardDetailsTO);

        AwardDetailsTO actualAwardDetails = awardDetailsService.findLifeEventAward(clientId, glbcertId);

        verify(dao).findLifeEventAward(eq(clientId),eq(glbcertId));
        assertNotNull(actualAwardDetails);
        assertEquals(actualAwardDetails.getGlbcertUniqueId(),awardDetailsTO.getGlbcertUniqueId());
    }

    @Test
    public void test_find_service_milestones_award_success() {
        long clientId = 901l;
        final String glbcertId = "A7M4-M6IGKO-FX1CZA";

        AwardDetailsTO awardDetailsTO = new AwardDetailsTO();
        awardDetailsTO.setGlbcertUniqueId(glbcertId);
        awardDetailsTO.setPkClient(clientId);

        when(dao.findServiceMilestonesAward(clientId, glbcertId)).thenReturn(awardDetailsTO);

        AwardDetailsTO actualAwardDetails = awardDetailsService.findServiceMilestonesAward(clientId, glbcertId);

        verify(dao).findServiceMilestonesAward(eq(clientId),eq(glbcertId));
        assertNotNull(actualAwardDetails);
        assertEquals(actualAwardDetails.getGlbcertUniqueId(),awardDetailsTO.getGlbcertUniqueId());
    }


    @Test
    public void test_find_award_details_success() {
        long clientId = 901l;
        final Long awardHistoryId = 123l;

        AwardDetailsTO awardDetailsTO = new AwardDetailsTO();
        awardDetailsTO.setPkAwardHistory(awardHistoryId);
        awardDetailsTO.setPkClient(clientId);

        final AwardHistoryTO awardHistoryTO = new AwardHistoryTO();
        awardHistoryTO.setPkAwardHistory(awardHistoryId);
        awardHistoryTO.setLinkId(1l);
        awardHistoryTO.setGlbcertId("AAA-AAA-AAA");
        awardHistoryTO.setAwardPrivate(true);
        awardHistoryTO.setPkAwardee(123456l);

        when(dao.findAwardHistoryByAwardHistoryId(clientId, awardHistoryId)).thenReturn(awardHistoryTO);

        AwardDetailsTO actualAwardDetails = awardDetailsService.findAwardDetailsByAwardHistoryId(clientId, awardHistoryId);

        verify(dao).findAwardHistoryByAwardHistoryId(eq(clientId),eq(awardHistoryId));
        assertNotNull(actualAwardDetails);
        assertEquals(actualAwardDetails.getPkClient(),awardDetailsTO.getPkClient());
        assertEquals(actualAwardDetails.getPkAwardHistory(),awardDetailsTO.getPkAwardHistory());
        assertEquals(actualAwardDetails.getGlbcertUniqueId(), awardHistoryTO.getGlbcertId());
        assertEquals(actualAwardDetails.isAwardPrivate(), awardHistoryTO.isAwardPrivate());
        assertEquals(actualAwardDetails.getLinkId(), awardHistoryTO.getLinkId());
        assertEquals(actualAwardDetails.getReason(), awardHistoryTO.getReason());
    }

    @Test
    public void test_award_history_not_found() {
        long clientId = 901l;
        final Long awardHistoryId = 123l;

        when(dao.findAwardHistoryByAwardHistoryId(clientId, awardHistoryId)).thenReturn(null);

        AwardDetailsTO actualAwardDetails = awardDetailsService.findAwardDetailsByAwardHistoryId(clientId, awardHistoryId);

        verify(dao).findAwardHistoryByAwardHistoryId(eq(clientId),eq(awardHistoryId));
        assertNull(actualAwardDetails);
    }
}
