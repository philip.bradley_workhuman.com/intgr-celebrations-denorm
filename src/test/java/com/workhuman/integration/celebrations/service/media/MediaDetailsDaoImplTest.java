package com.workhuman.integration.celebrations.service.media;


import com.github.database.rider.core.api.dataset.DataSet;
import com.workhuman.integration.celebrations.service.RepositoryTestBase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.*;

@Sql({
        "/db/schema/GG_AWARD_HISTORY.sql",
        "/db/schema/GG_PENDING_ORDERS.sql",
        "/db/schema/GG_YOS_AWARD.sql",
        "/db/schema/GG_INBOX.sql",
        "/db/schema/GG_MS_PERSON.sql",
        "/db/schema/GG_MS_CLIENTS.sql",
        "/db/schema/GG_CONFIG_META.sql",
        "/db/schema/18_gg_award_types.sql",
        "/db/schema/342_gg_media.sql",
        "/db/schema/59_gg_jubilee.sql",
        "/db/schema/88_gg_jubilee_notifications.sql",
        "/db/schema/522_rec_celebrations_denorm.sql",
        "/db/schema/501_gg_media_award_gallery.sql",
        "/db/schema/rec_celebrations_denorm_seq.sql",
        "/db/schema/11152_yos_inheritance_views.sql",
        "/db/schema/V_AWARD_TYPES.sql"
})
@DataSet(value = {"/db/datasets/MediaDetailsDaoImplTest.xml"})
public class MediaDetailsDaoImplTest extends RepositoryTestBase {

    private final DataSource dataSource;

    @Autowired
    public MediaDetailsDaoImplTest(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private MediaDetailsDaoImpl dao;

    private static final long CLIENT_ID = 50220L;

    private static final String NOMINATION_MEDIA_ENTRY_ID = "1_f05ohp5d";

    private static final String AWARD_MEDIA_ENTRY_ID = "1_g9gr5ma4";

    private static final String NO_LINKED_MEDIA_ENTRY_ID = "1_cgb9y4rZ";

    private static final String GROUP_AWARD_MEDIA_ENTRY_ID = "X_g9gr5ma4";

    private static final Long AWARD_HISTORY_ID = 51214858L;

    private static final Long AWARD_WITH_NO_MEDIA_ATTACHED = 51214859L;

    private static final Long LINK_ID = 9999L;

    @BeforeEach
    public void setUp() {
        this.dao = new MediaDetailsDaoImpl(dataSource);
    }

    @Test
    public void find_nomination_media_details_success() {
        final Long groupId = 43310599L;
        final MediaDetailsTO mediaDetails = dao.getMediaDetails(NOMINATION_MEDIA_ENTRY_ID);

        assertNotNull(mediaDetails);
        assertEquals(CLIENT_ID, mediaDetails.getClientId());
        assertEquals(groupId, mediaDetails.getGroupId());
        assertNull(mediaDetails.getLinkId());
        assertNull(mediaDetails.getFkAwardHistory());
    }

    @Test
    public void find_gallery_media_details_success() {
        final MediaDetailsTO mediaDetails = dao.getMediaDetails(AWARD_MEDIA_ENTRY_ID);

        assertNotNull(mediaDetails);
        assertEquals(CLIENT_ID, mediaDetails.getClientId());
        assertEquals(AWARD_HISTORY_ID, mediaDetails.getFkAwardHistory());
        assertNull(mediaDetails.getGroupId());
        assertNull(mediaDetails.getLinkId());
    }

    @Test
    public void find_no_linked_media_details() {
        final MediaDetailsTO mediaDetails = dao.getMediaDetails(NO_LINKED_MEDIA_ENTRY_ID);

        assertNotNull(mediaDetails);
        assertEquals(CLIENT_ID, mediaDetails.getClientId());
        assertNull(mediaDetails.getFkAwardHistory());
        assertNull(mediaDetails.getGroupId());
        assertNull(mediaDetails.getLinkId());
    }

    @Test
    public void whenFindNominationMediaForNonExistingAwardId_shouldReturnNull() {
        final long nonExistingAwardId = 123456L;
        final String mediaEntryId = dao.findNominationMediaEntryId(nonExistingAwardId);

        assertNull(mediaEntryId);
    }

    @Test
    public void whenFindNominationMediaForExistingAwardIdWithNoNominationMedia_shouldReturnNull() {
        final long awardWithNoMediaAttached = 51214859L;
        final String mediaEntryId = dao.findNominationMediaEntryId(awardWithNoMediaAttached);

        assertNull(mediaEntryId);
    }

    @Test
    public void whenFindNominationMediaForExistingAwardIdWithNominationMedia_shouldReturnMediaEntryId() {
        final String mediaEntryId = dao.findNominationMediaEntryId(AWARD_HISTORY_ID);

        assertNotNull(mediaEntryId);
        assertEquals(NOMINATION_MEDIA_ENTRY_ID, mediaEntryId);
    }

    @Test
    public void whenFindFirstGalleryMediaForNonExistingAwardId_shouldReturnNull() {
        final long nonExistingAwardId = 123456L;
        final String mediaEntryId = dao.findFirstMediaGalleryEntryId(nonExistingAwardId);

        assertNull(mediaEntryId);
    }

    @Test
    public void whenFindFirstMediaForExistingAwardIdWithNoNominationMedia_shouldReturnNull() {
        final String mediaEntryId = dao.findFirstMediaGalleryEntryId(AWARD_WITH_NO_MEDIA_ATTACHED);

        assertNull(mediaEntryId);
    }

    @Test
    public void whenFindFirstMediaForExistingAwardIdWithNominationMedia_shouldReturnFirstMediaEntryId() {
        final String mediaEntryId = dao.findFirstMediaGalleryEntryId(AWARD_HISTORY_ID);

        assertNotNull(mediaEntryId);
        assertEquals(AWARD_MEDIA_ENTRY_ID, mediaEntryId);
    }

    @Test
    public void whenFindFirstGalleryMediaForNonExistingLinkId_shouldReturnNull() {
        final long nonExistingLinkId = 123456L;
        final String mediaEntryId = dao.findFirstMediaGalleryEntryIdByLinkId(nonExistingLinkId);

        assertNull(mediaEntryId);
    }

    @Test
    public void whenFindFirstMediaForExistingLinkIdWithNominationMedia_shouldReturnFirstMediaEntryId() {
        final String mediaEntryId = dao.findFirstMediaGalleryEntryIdByLinkId(LINK_ID);

        assertNotNull(mediaEntryId);
        assertEquals(GROUP_AWARD_MEDIA_ENTRY_ID, mediaEntryId);
    }
}
