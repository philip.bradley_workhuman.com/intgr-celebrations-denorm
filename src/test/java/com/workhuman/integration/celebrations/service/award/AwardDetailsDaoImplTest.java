package com.workhuman.integration.celebrations.service.award;

import com.github.database.rider.core.api.dataset.DataSet;
import com.workhuman.integration.celebrations.service.RepositoryTestBase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.*;

@Sql({
        "/db/schema/GG_AWARD_HISTORY.sql",
        "/db/schema/GG_PENDING_ORDERS.sql",
        "/db/schema/GG_YOS_AWARD.sql",
        "/db/schema/GG_INBOX.sql",
        "/db/schema/GG_MS_PERSON.sql",
        "/db/schema/GG_MS_CLIENTS.sql",
        "/db/schema/GG_CONFIG_META.sql",
        "/db/schema/18_gg_award_types.sql",
        "/db/schema/342_gg_media.sql",
        "/db/schema/59_gg_jubilee.sql",
        "/db/schema/88_gg_jubilee_notifications.sql",
        "/db/schema/11152_yos_inheritance_views.sql",
        "/db/schema/V_AWARD_TYPES.sql"
})
@DataSet(value = {"/db/datasets/AwardDetailsDaoImplTest.xml"})
public class AwardDetailsDaoImplTest extends RepositoryTestBase {

    private final DataSource dataSource;

    private AwardDetailsDaoImpl dao;

    private static final long CLIENT_ID = 10000L;

    private static final long CLIENT_ID_2 = 90100L;

    private static final long CLIENT_ID_INHERITED = 90101L;


    @Autowired
    public AwardDetailsDaoImplTest(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @BeforeEach
    public void setUp() {
        this.dao = new AwardDetailsDaoImpl(dataSource);
    }

    @Test
    public void find_LifeEventAward_found_cancelled_too() {
        final String glbcertId = "ABC-DEF-GHI-009";

        final AwardDetailsTO awardDetails = dao.findLifeEventAward(CLIENT_ID, glbcertId);

        assertNotNull(awardDetails);
        assertTrue(awardDetails.isCancelled());
    }

    @Test
    public void find_LifeEventAward_success() {
        final String glbcertId = "ABC-DEF-GHI-010";
        final String mediaEntryId = "1_hbygf3aj";
        final Integer mediaType = 2;

        final AwardDetailsTO awardDetails = dao.findLifeEventAward(CLIENT_ID, glbcertId);

        assertNotNull(awardDetails);
        assertEquals(glbcertId, awardDetails.getGlbcertUniqueId());
        assertEquals(CLIENT_ID, awardDetails.getPkClient());

        assertEquals("reason1", awardDetails.getReason());
        assertEquals(Long.valueOf(10), awardDetails.getPkAwardHistory());
        assertNull(awardDetails.getLinkId());
        assertEquals(Long.valueOf(2), awardDetails.getPkRecipient());
        assertEquals(Long.valueOf(1), awardDetails.getFkAwardType());
        assertEquals(Long.valueOf(680), awardDetails.getProProdId());
        assertEquals(0, awardDetails.getBaoBatchOutId());
        assertEquals(mediaEntryId, awardDetails.getMediaEntryId());
        assertEquals(mediaType, awardDetails.getMediaType());
        assertNull(awardDetails.getYears());
        assertFalse(awardDetails.isAwardPrivate());
        assertFalse(awardDetails.isTimelines());
    }

    @Test
    public void find_ServiceMilestones_found_cancelled_too() {
        final String glbcertId = "ABC-DEF-GHI-005";

        final AwardDetailsTO awardDetails = dao.findServiceMilestonesAward(CLIENT_ID_2, glbcertId);

        assertNotNull(awardDetails);
        assertTrue(awardDetails.isCancelled());
    }

    @Test
    public void find_ServiceMilestones_inheritance_test() throws Exception {
        final String glbcertId = "ABC-DEF-GHI-100";

        final AwardDetailsTO awardDetails = dao.findServiceMilestonesAward(CLIENT_ID_INHERITED, glbcertId);

        assertNotNull(awardDetails);
        assertEquals(Long.valueOf(100), awardDetails.getFkAwardType());

    }

    @Test
    public void find_award_history_found() {
        final String glbcertId = "ABC-DEF-GHI-013";
        final Long awardHistoryId = 13L;

        final AwardHistoryTO awardHistoryDetails = dao.findAwardHistoryByAwardHistoryId(CLIENT_ID_2, awardHistoryId);

        assertNotNull(awardHistoryDetails);
        assertEquals(glbcertId, awardHistoryDetails.getGlbcertId());
    }

    @Test
    public void check_award_is_timelines_false() {
        final String glbcertId = "ABC-DEF-GHI-001";

        final boolean isAwardTimelines = dao.isTimelinesYosAward(glbcertId);

        assertFalse(isAwardTimelines);
    }

    @Test
    public void check_award_is_timelines_true() {
        final String glbcertId = "ABC-DEF-GHI-002";

        final boolean isAwardTimelines = dao.isTimelinesYosAward(glbcertId);

        assertTrue(isAwardTimelines);
    }
}
