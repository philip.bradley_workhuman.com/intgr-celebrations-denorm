package com.workhuman.integration.celebrations.service.utils;


import com.workhuman.integration.celebrations.service.exception.TooManyResultsException;
import com.workhuman.integration.celebrations.service.media.MediaDetailsTO;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class DAOUtilsTest {

    @Test
    public void test_get_object_result_success() {
        Long userId = 11357772L;
        Long clientId = 5022L;
        String mediaEntryParamName = "mediaEntryId";
        String mediaEntryId = "1_g9gr5ma4";
        Long awardHistoryId = 51214858L;
        Logger logger = LoggerFactory.getLogger(DAOUtilsTest.class);
        MediaDetailsTO mediaDetails = new MediaDetailsTO();
        mediaDetails.setClientId(clientId);
        mediaDetails.setCreatorId(userId);
        mediaDetails.setFkAwardHistory(awardHistoryId);
        mediaDetails.setEntryId(mediaEntryId);

        List<MediaDetailsTO> itemsList = new ArrayList<>();
        itemsList.add(mediaDetails);

        MediaDetailsTO actualMediaDetails = DAOUtils.getSingleObject(itemsList, logger, mediaEntryParamName, mediaEntryId);
        assertNotNull(actualMediaDetails);
        assertEquals(mediaEntryId, actualMediaDetails.getEntryId());
    }

    @Test
    public void test_get_object_result_null() {
        String mediaEntryParamName = "mediaEntryId";
        String mediaEntryId = "1_g9gr5ma4";
        Logger logger = LoggerFactory.getLogger(DAOUtilsTest.class);

        List<MediaDetailsTO> itemsList = new ArrayList<>();

        MediaDetailsTO actualMediaDetails = DAOUtils.getSingleObject(itemsList, logger, mediaEntryParamName, mediaEntryId);
        assertNull(actualMediaDetails);
    }

    @Test
    public void test_get_object_result_more_than_two() {
        Long userId = 11357772L;
        Long clientId = 5022L;
        String mediaEntryParamName = "mediaEntryId";
        String mediaEntryId = "1_g9gr5ma4";
        Long awardHistoryId = 51214858L;
        Logger logger = LoggerFactory.getLogger(DAOUtilsTest.class);
        MediaDetailsTO mediaDetails = new MediaDetailsTO();
        mediaDetails.setClientId(clientId);
        mediaDetails.setCreatorId(userId);
        mediaDetails.setFkAwardHistory(awardHistoryId);
        mediaDetails.setEntryId(mediaEntryId);

        List<MediaDetailsTO> itemsList = new ArrayList<>();
        itemsList.add(mediaDetails);
        itemsList.add(mediaDetails);

        TooManyResultsException thrown = assertThrows(TooManyResultsException.class, () -> {
            DAOUtils.getSingleObject(itemsList, logger, mediaEntryParamName, mediaEntryId);
        });
    }
}
