package com.workhuman.integration.celebrations.processor;

import com.workhuman.integration.celebrations.events.GroupAwardGalleryUpdatedEvent;
import com.workhuman.integration.celebrations.service.IntgrCelebrationDenormService;
import com.workhuman.integration.celebrations.service.celebration.CelebrationAwardDenormService;
import com.workhuman.integration.celebrations.service.celebration.IntgrCelebrationDenormVO;
import com.workhuman.integration.celebrations.service.media.MediaDetailsService;
import com.workhuman.integration.celebrations.service.media.MediaValidator;
import org.apache.camel.Exchange;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

public class GroupAwardGalleryUpdatedProcessorTest extends DefaultAwardGalleryUpdatedProcessorTest {

    private static final long USER_ID = 11357772L;

    private static final String MEDIA_ENTRY_ID = "1_g9gr5ma4";

    private static final long CLIENT_ID = 5022L;

    private static final long LINK_ID = 11111L;

    private static final long AWARD_HISTORY_ID = 51214858L;

    @InjectMocks
    private GroupAwardGalleryUpdatedProcessor processor;

    @Mock
    private IntgrCelebrationDenormService intgrCelebrationDenormService;

    @Mock
    private CelebrationAwardDenormService celebrationAwardDenormService;

    @Mock
    private MediaDetailsService mediaDetailsService;

    @Mock
    private MediaValidator mediaValidator;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenMediaDoesntExists_processShouldInsertFirst() {
        final String firstMediaEntryId = MEDIA_ENTRY_ID;
        final GroupAwardGalleryUpdatedEvent event = createEvent(USER_ID, LINK_ID, MEDIA_ENTRY_ID);
        final Exchange exchange = createEventExchange(event);
        final IntgrCelebrationDenormVO denormalizedAward =
                createCelebrationDenorm(CLIENT_ID, AWARD_HISTORY_ID, null, LINK_ID);

        when(mediaValidator.mediaEntryDoesNotBelongToAwardGallery(MEDIA_ENTRY_ID))
                .thenReturn(false);
        when(celebrationAwardDenormService.findFirstByLinkId(LINK_ID))
                .thenReturn(denormalizedAward);
        when(mediaValidator.isRemovedMediaNotTheFirstAwardMedia(MEDIA_ENTRY_ID, denormalizedAward))
                .thenReturn(false);
        when(mediaDetailsService.findFirstMediaEntryId(denormalizedAward))
                .thenReturn(firstMediaEntryId);
        when(mediaValidator.areEquals(firstMediaEntryId, denormalizedAward))
                .thenReturn(false);

        processor.process(exchange);

        verify(intgrCelebrationDenormService, atLeastOnce()).denormalizeGroupAwardGalleryUpdated(denormalizedAward.getClientId(), LINK_ID, MEDIA_ENTRY_ID);
    }

    @Test
    public void whenMediaIsSameAsFirst_processShouldBeSkipped() {
        final String existingFirstMediaEntryId = "1_f05ohp5d";
        final GroupAwardGalleryUpdatedEvent event = createEvent(USER_ID, LINK_ID, MEDIA_ENTRY_ID);
        final Exchange exchange = createEventExchange(event);
        final IntgrCelebrationDenormVO denormalizedAward =
                createCelebrationDenorm(CLIENT_ID, AWARD_HISTORY_ID, existingFirstMediaEntryId, LINK_ID);

        when(mediaValidator.mediaEntryDoesNotBelongToAwardGallery(MEDIA_ENTRY_ID))
                .thenReturn(false);
        when(celebrationAwardDenormService.findFirstByLinkId(LINK_ID))
                .thenReturn(denormalizedAward);
        when(mediaValidator.isRemovedMediaNotTheFirstAwardMedia(MEDIA_ENTRY_ID, denormalizedAward))
                .thenReturn(false);
        when(mediaDetailsService.findFirstMediaEntryId(denormalizedAward))
                .thenReturn(existingFirstMediaEntryId);
        when(mediaValidator.areEquals(existingFirstMediaEntryId, denormalizedAward))
                .thenReturn(true);

        processor.process(exchange);

        verify(intgrCelebrationDenormService, never()).denormalizeGroupAwardGalleryUpdated(anyLong(), anyLong(), anyString());
    }

    @Test
    public void whenMediaHasBeenRemoved_processShouldBeSkipped() {
        final String existingFirstMediaEntryId = "1_f05ohp5d";
        final GroupAwardGalleryUpdatedEvent event = createEvent(USER_ID, LINK_ID, MEDIA_ENTRY_ID);
        final Exchange exchange = createEventExchange(event);
        final IntgrCelebrationDenormVO denormalizedAward =
                createCelebrationDenorm(CLIENT_ID, AWARD_HISTORY_ID, existingFirstMediaEntryId, LINK_ID);

        when(mediaValidator.mediaEntryDoesNotBelongToAwardGallery(MEDIA_ENTRY_ID))
                .thenReturn(false);
        when(celebrationAwardDenormService.findFirstByLinkId(LINK_ID))
                .thenReturn(denormalizedAward);
        when(mediaValidator.isRemovedMediaNotTheFirstAwardMedia(MEDIA_ENTRY_ID, denormalizedAward))
                .thenReturn(true);

        processor.process(exchange);

        verify(intgrCelebrationDenormService, never()).denormalizeGroupAwardGalleryUpdated(anyLong(), anyLong(), anyString());
    }

    @Test
    public void whenMediaDoesNotBelongToAwardGallery_processShouldBeSkipped() {
        final GroupAwardGalleryUpdatedEvent event = createEvent(USER_ID, LINK_ID, MEDIA_ENTRY_ID);
        final Exchange exchange = createEventExchange(event);

        when(mediaValidator.mediaEntryDoesNotBelongToAwardGallery(MEDIA_ENTRY_ID))
                .thenReturn(true);

        processor.process(exchange);

        verify(intgrCelebrationDenormService, never()).denormalizeGroupAwardGalleryUpdated(anyLong(), anyLong(), anyString());
    }

    private GroupAwardGalleryUpdatedEvent createEvent(final long userId, final long linkId, final String mediaEntryId) {
        final GroupAwardGalleryUpdatedEvent event = new GroupAwardGalleryUpdatedEvent();
        event.setUserId(userId);
        event.setLinkId(linkId);
        event.setMediaEntryId(mediaEntryId);

        return event;
    }
}
