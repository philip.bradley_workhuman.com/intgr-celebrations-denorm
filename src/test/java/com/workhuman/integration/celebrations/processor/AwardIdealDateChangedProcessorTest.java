package com.workhuman.integration.celebrations.processor;

import com.workhuman.integration.celebrations.events.AwardIdealDateChangedEvent;
import com.workhuman.integration.celebrations.service.IntgrCelebrationDenormService;
import com.workhuman.integration.celebrations.service.award.AwardDetailsService;
import com.workhuman.integration.celebrations.service.award.AwardDetailsTO;
import com.workhuman.integration.celebrations.service.exception.IntgrCelebrationDenormUpdateDataException;
import com.workhuman.integration.celebrations.service.order.OrderDetailsService;
import com.workhuman.integration.celebrations.service.order.OrderDetailsTO;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.support.DefaultExchange;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class AwardIdealDateChangedProcessorTest {

    @InjectMocks
    private AwardIdealDateChangedProcessor processor;

    @Mock
    private IntgrCelebrationDenormService intgrCelebrationDenormService;

    @Mock
    private OrderDetailsService orderDetailsService;

    @Mock
    private AwardDetailsService awardDetailsService;


    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void process_with_glbcertId_success() throws Exception {
        String glbcertId = "CAT2-XQUAH8-T9TYDA";
        Long clientId = 33L;
        Long productId = 680l;

        AwardIdealDateChangedEvent event = new AwardIdealDateChangedEvent();
        event.setClientId(clientId);
        event.setGlbcertId(glbcertId);

        OrderDetailsTO orderDetails = new OrderDetailsTO();
        orderDetails.setGlbcertUniqueId(glbcertId);
        orderDetails.setProductId(productId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(event);

        when(orderDetailsService.findByGlbcertId(clientId, glbcertId)).thenReturn(orderDetails);

        processor.process(exchange);
        verify(intgrCelebrationDenormService, atLeastOnce()).denormalizeAwardDeliveryDateChanged(any(AwardDetailsTO.class));
    }

    @Test
    public void process_with_YOS_productId_should_be_skipped() throws Exception {
        String indOrderId = "7785694";
        Long clientId = 33L;
        Long productId = 650l;

        AwardIdealDateChangedEvent event = new AwardIdealDateChangedEvent();
        event.setClientId(clientId);
        event.setIndOrderId(indOrderId);

        OrderDetailsTO orderDetails = new OrderDetailsTO();
        orderDetails.setProductId(productId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(event);

        when(orderDetailsService.findByOrderId(clientId, indOrderId)).thenReturn(orderDetails);

        processor.process(exchange);

        verify(intgrCelebrationDenormService, never()).denormalizeAwardDeliveryDateChanged(any(AwardDetailsTO.class));
    }

    @Test
    public void process_without_glbcertId_and_indOrderId_are_skipped() throws Exception {
        Long clientId = 33L;
        AwardIdealDateChangedEvent event = new AwardIdealDateChangedEvent();
        event.setClientId(clientId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(event);

        IntgrCelebrationDenormUpdateDataException thrown = assertThrows(IntgrCelebrationDenormUpdateDataException.class, () -> {
            processor.process(exchange);
        });

        verify(intgrCelebrationDenormService, never()).denormalizeAwardDeliveryDateChanged(any(AwardDetailsTO.class));
        verify(orderDetailsService, never()).findByOrderId(any(Long.class), any(String.class));
        verify(orderDetailsService, never()).findByGlbcertId(any(Long.class), any(String.class));
    }
}
