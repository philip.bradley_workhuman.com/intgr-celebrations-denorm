package com.workhuman.integration.celebrations.processor;

import com.workhuman.integration.celebrations.events.GroupAwardIdealDateChangedEvent;
import com.workhuman.integration.celebrations.service.IntgrCelebrationDenormService;
import com.workhuman.integration.celebrations.service.exception.IntgrCelebrationDenormUpdateDataException;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.support.DefaultExchange;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class GroupAwardIdealDateChangedProcessorTest {

    private static final long CLIENT_ID = 5021L;

    private static final long LINK_ID = 34L;

    @InjectMocks
    private GroupAwardIdealDateChangedProcessor processor;

    @Mock
    private IntgrCelebrationDenormService intgrCelebrationDenormService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testProcess_success() throws Exception {
        GroupAwardIdealDateChangedEvent event = createEvent(CLIENT_ID, LINK_ID);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(event);

        processor.process(exchange);
        verify(intgrCelebrationDenormService, atLeastOnce()).denormalizeGroupAwardDeliveryDateChanged(eq(CLIENT_ID), eq(LINK_ID));
    }

    @Test
    public void testProcess_invalidClientId() throws Exception {
        long invalidClientId = 0;
        GroupAwardIdealDateChangedEvent event = createEvent(invalidClientId, LINK_ID);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(event);

        IntgrCelebrationDenormUpdateDataException thrown = assertThrows(IntgrCelebrationDenormUpdateDataException.class, () -> {
            processor.process(exchange);
        });

        verify(intgrCelebrationDenormService, never()).denormalizeGroupAwardDeliveryDateChanged((any(Long.class)), eq(LINK_ID));
    }

    @Test
    public void testProcess_invalidLinkId() throws Exception {
        long invalidLinkId = 0;
        GroupAwardIdealDateChangedEvent event = createEvent(CLIENT_ID, invalidLinkId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(event);

        IntgrCelebrationDenormUpdateDataException thrown = assertThrows(IntgrCelebrationDenormUpdateDataException.class, () -> {
            processor.process(exchange);
        });

        verify(intgrCelebrationDenormService, never()).denormalizeGroupAwardDeliveryDateChanged(eq(CLIENT_ID), (any(Long.class)));
    }

    private GroupAwardIdealDateChangedEvent createEvent(final long clientId, final long linkId) {
        GroupAwardIdealDateChangedEvent event = new GroupAwardIdealDateChangedEvent();
        event.setClientId(clientId);
        event.setLinkId(linkId);

        return event;
    }

}
