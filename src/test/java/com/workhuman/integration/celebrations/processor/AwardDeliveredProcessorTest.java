package com.workhuman.integration.celebrations.processor;


import com.workhuman.integration.celebrations.events.AwardDeliveredEvent;
import com.workhuman.integration.celebrations.service.IntgrCelebrationDenormService;
import com.workhuman.integration.celebrations.service.award.AwardDetailsService;
import com.workhuman.integration.celebrations.service.award.AwardDetailsTO;
import com.workhuman.integration.celebrations.service.exception.IntgrCelebrationDenormUpdateDataException;
import com.workhuman.integration.celebrations.service.order.OrderDetailsService;
import com.workhuman.integration.celebrations.service.order.OrderDetailsTO;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.support.DefaultExchange;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;


public class AwardDeliveredProcessorTest {

    @InjectMocks
    private AwardDeliveredProcessor processor;

    @Mock
    private IntgrCelebrationDenormService intgrCelebrationDenormService;

    @Mock
    private OrderDetailsService orderDetailsService;

    @Mock
    private AwardDetailsService awardDetailsService;


    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void process_life_event_success() throws Exception {
        String glbcertId = "CAT2-XQUAH8-T9TYDA";
        Long clientId = 33L;
        Long productId = 680l;

        AwardDeliveredEvent event = new AwardDeliveredEvent();
        event.setClientId(clientId);
        event.setGlbcertId(glbcertId);

        OrderDetailsTO orderDetails = new OrderDetailsTO();
        orderDetails.setGlbcertUniqueId(glbcertId);
        orderDetails.setProductId(productId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(event);

        when(orderDetailsService.findByGlbcertId(clientId, glbcertId)).thenReturn(orderDetails);

        processor.process(exchange);
        verify(intgrCelebrationDenormService, atLeastOnce()).denormalizeAwardDelivery(any(AwardDetailsTO.class));
    }

    @Test
    public void process_event_skipped() throws Exception {
        String glbcertId = "CAT2-XQUAH8-T9TYDA";
        Long clientId = 33L;
        Long productId = 620l;

        AwardDeliveredEvent event = new AwardDeliveredEvent();
        event.setClientId(clientId);
        event.setGlbcertId(glbcertId);

        OrderDetailsTO orderDetails = new OrderDetailsTO();
        orderDetails.setGlbcertUniqueId(glbcertId);
        orderDetails.setProductId(productId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(event);

        when(orderDetailsService.findByGlbcertId(clientId, glbcertId)).thenReturn(orderDetails);

        processor.process(exchange);
        verify(intgrCelebrationDenormService, never()).denormalizeAwardDelivery(any(AwardDetailsTO.class));
    }

    @Test
    public void process_failure_details_not_found() throws Exception {
        String glbcertId = "CAT2-XQUAH8-T9TYDA";
        Long clientId = 33L;

        AwardDeliveredEvent event = new AwardDeliveredEvent();
        event.setClientId(clientId);
        event.setGlbcertId(glbcertId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(event);

        when(orderDetailsService.findByGlbcertId(clientId, glbcertId)).thenReturn(null);

        IntgrCelebrationDenormUpdateDataException thrown = assertThrows(IntgrCelebrationDenormUpdateDataException.class, () -> {
            processor.process(exchange);
        });

        verify(intgrCelebrationDenormService, never()).denormalizeAwardDelivery(any(AwardDetailsTO.class));
    }

    @Test
    public void process_failure_empty_param() throws Exception {
        String glbcertId = new String();
        Long clientId = 33L;

        AwardDeliveredEvent event = new AwardDeliveredEvent();
        event.setClientId(clientId);
        event.setGlbcertId(glbcertId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(event);

        IntgrCelebrationDenormUpdateDataException thrown = assertThrows(IntgrCelebrationDenormUpdateDataException.class, () -> {
            processor.process(exchange);
        });

        verify(intgrCelebrationDenormService, never()).denormalizeAwardDelivery(any(AwardDetailsTO.class));
    }

}
