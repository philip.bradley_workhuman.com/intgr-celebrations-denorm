package com.workhuman.integration.celebrations.processor;

import com.workhuman.integration.celebrations.events.PersonDataloadFinishedEvent;
import com.workhuman.integration.celebrations.service.IntgrCelebrationDenormService;
import com.workhuman.integration.celebrations.service.exception.IntgrCelebrationDenormUpdateDataException;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.support.DefaultExchange;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class PersonDataloadFinishedProcessorTest {

    @InjectMocks
    private PersonDataloadFinishedProcessor processor;

    @Mock
    private IntgrCelebrationDenormService intgrCelebrationDenormService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testProcess_success() throws Exception {
        Long clientId = 33L;
        PersonDataloadFinishedEvent event = new PersonDataloadFinishedEvent();
        event.setClientId(clientId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(event);

        processor.process(exchange);
        verify(intgrCelebrationDenormService, atLeastOnce()).denormalizePersonDataloadFinished(eq(clientId));
    }

    @Test
    public void testProcess_zeroClient() throws Exception {
        Long clientId = 0l;
        PersonDataloadFinishedEvent event = new PersonDataloadFinishedEvent();
        event.setClientId(clientId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(event);

        IntgrCelebrationDenormUpdateDataException thrown = assertThrows(IntgrCelebrationDenormUpdateDataException.class, () -> {
            processor.process(exchange);
        });

        verify(intgrCelebrationDenormService, never()).denormalizePersonDataloadFinished(any(Long.class));
    }


}
