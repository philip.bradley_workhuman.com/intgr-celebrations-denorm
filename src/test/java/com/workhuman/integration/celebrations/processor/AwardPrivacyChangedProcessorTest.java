package com.workhuman.integration.celebrations.processor;


import com.workhuman.integration.celebrations.events.AwardPrivacyChangedEvent;
import com.workhuman.integration.celebrations.service.IntgrCelebrationDenormService;
import com.workhuman.integration.celebrations.service.award.AwardDetailsService;
import com.workhuman.integration.celebrations.service.award.AwardDetailsTO;
import com.workhuman.integration.celebrations.service.exception.IntgrCelebrationDenormUpdateDataException;
import com.workhuman.integration.celebrations.service.order.OrderDetailsService;
import com.workhuman.integration.celebrations.service.order.OrderDetailsTO;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.support.DefaultExchange;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class AwardPrivacyChangedProcessorTest {

    @InjectMocks
    private AwardPrivacyChangedProcessor processor;

    @Mock
    private IntgrCelebrationDenormService intgrCelebrationDenormService;

    @Mock
    private OrderDetailsService orderDetailsService;

    @Mock
    private AwardDetailsService awardDetailsService;


    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void process_update_life_event_success() throws Exception {
        String glbcertId = "CAT2-XQUAH8-T9TYDA";
        Long clientId = 33L;
        Long awardHistoryId = 1452L;
        Long productId = 680l;
        OrderDetailsTO orderDetails = new OrderDetailsTO();
        orderDetails.setProductId(productId);

        AwardDetailsTO awardDetailsTO = new AwardDetailsTO();
        awardDetailsTO.setGlbcertUniqueId(glbcertId);
        awardDetailsTO.setPkAwardHistory(awardHistoryId);

        AwardPrivacyChangedEvent event = new AwardPrivacyChangedEvent();
        event.setClientId(clientId);
        event.setGlbcertId(glbcertId);
        event.setAwardHistoryId(awardHistoryId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(event);

        when(orderDetailsService.findByGlbcertId(clientId, glbcertId)).thenReturn(orderDetails);
        when(awardDetailsService.findAwardDetailsByAwardHistoryId(clientId, awardHistoryId)).thenReturn(awardDetailsTO);

        processor.process(exchange);
        verify(intgrCelebrationDenormService, atLeastOnce()).denormalizeAwardPrivacyChanged(any(AwardDetailsTO.class));
    }

    @Test
    public void process_with_wrong_productId_skipped() throws Exception {
        String glbcertId = "CAT2-XQUAH8-T9TYDA";
        Long clientId = 33L;
        Long productId = 620l;
        OrderDetailsTO orderDetails = new OrderDetailsTO();
        orderDetails.setProductId(productId);

        AwardPrivacyChangedEvent event = new AwardPrivacyChangedEvent();
        event.setClientId(clientId);
        event.setGlbcertId(glbcertId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(event);

        when(orderDetailsService.findByGlbcertId(clientId, glbcertId)).thenReturn(orderDetails);

        processor.process(exchange);
        verify(intgrCelebrationDenormService, never()).denormalizeAwardPrivacyChanged(any(AwardDetailsTO.class));
    }

    @Test
    public void process_without_award_details_failed() throws Exception {
        String glbcertId = "CAT2-XQUAH8-T9TYDA";
        Long clientId = 33L;
        Long awardHistoryId = 1452L;
        Long productId = 680l;
        OrderDetailsTO orderDetails = new OrderDetailsTO();
        orderDetails.setProductId(productId);

        AwardPrivacyChangedEvent event = new AwardPrivacyChangedEvent();
        event.setClientId(clientId);
        event.setGlbcertId(glbcertId);
        event.setAwardHistoryId(awardHistoryId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(event);

        when(orderDetailsService.findByGlbcertId(clientId, glbcertId)).thenReturn(orderDetails);
        when(awardDetailsService.findAwardDetailsByAwardHistoryId(clientId, awardHistoryId)).thenReturn(null);

        IntgrCelebrationDenormUpdateDataException thrown = assertThrows(IntgrCelebrationDenormUpdateDataException.class, () -> {
            processor.process(exchange);
        });

        verify(intgrCelebrationDenormService, never()).denormalizeAwardPrivacyChanged(any(AwardDetailsTO.class));
    }

    @Test
    public void process_without_glbcertId_failed() throws Exception {
        String glbcertId = new String();
        Long clientId = 33L;
        Long awardHistoryId = 1452L;

        AwardPrivacyChangedEvent event = new AwardPrivacyChangedEvent();
        event.setClientId(clientId);
        event.setGlbcertId(glbcertId);
        event.setAwardHistoryId(awardHistoryId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(event);

        IntgrCelebrationDenormUpdateDataException thrown = assertThrows(IntgrCelebrationDenormUpdateDataException.class, () -> {
            processor.process(exchange);
        });

        verify(intgrCelebrationDenormService, never()).denormalizeAwardPrivacyChanged(any(AwardDetailsTO.class));
    }
}
