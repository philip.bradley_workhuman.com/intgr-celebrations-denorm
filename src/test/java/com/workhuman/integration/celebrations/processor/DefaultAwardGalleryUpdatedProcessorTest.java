package com.workhuman.integration.celebrations.processor;

import com.workhuman.integration.celebrations.service.celebration.IntgrCelebrationDenormVO;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.support.DefaultExchange;

import java.io.Serializable;

class DefaultAwardGalleryUpdatedProcessorTest {

    Exchange createEventExchange(final Serializable event) {
        final CamelContext mockContext = new DefaultCamelContext();
        final Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(event);

        return exchange;
    }

    IntgrCelebrationDenormVO createCelebrationDenorm(final long clientId, final long awardHistoryId, final String mediaEntryId, final Long linkId) {
        final IntgrCelebrationDenormVO celebrationDenormVO = new IntgrCelebrationDenormVO();
        celebrationDenormVO.setClientId(clientId);
        celebrationDenormVO.setAwardHistoryId(awardHistoryId);
        celebrationDenormVO.setMediaEntryId(mediaEntryId);
        celebrationDenormVO.setLinkId(linkId);

        return celebrationDenormVO;
    }
}
