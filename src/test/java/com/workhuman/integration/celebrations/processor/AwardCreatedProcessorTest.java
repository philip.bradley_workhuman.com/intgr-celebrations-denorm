package com.workhuman.integration.celebrations.processor;

import com.workhuman.integration.celebrations.events.AwardCreatedEvent;
import com.workhuman.integration.celebrations.service.IntgrCelebrationDenormService;
import com.workhuman.integration.celebrations.service.award.AwardDetailsService;
import com.workhuman.integration.celebrations.service.award.AwardDetailsTO;
import com.workhuman.integration.celebrations.service.exception.IntgrCelebrationDenormUpdateDataException;
import com.workhuman.integration.celebrations.service.order.OrderDetailsService;
import com.workhuman.integration.celebrations.service.order.OrderDetailsTO;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.support.DefaultExchange;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;


public class AwardCreatedProcessorTest {

    @InjectMocks
    private AwardCreatedProcessor awardCreatedProcessor;

    @Mock
    private IntgrCelebrationDenormService intgrCelebrationDenormService;

    @Mock
    private OrderDetailsService orderDetailsService;

    @Mock
    private AwardDetailsService awardDetailsService;


    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void process_LE_success() throws Exception {
        String glbcertId = "CAT2-XQUAH8-T9TYDA";
        Long clientId = 33L;
        Long awardHistoryId = 1452134785L;
        Long productId = 680l;
        OrderDetailsTO orderDetails = new OrderDetailsTO();
        orderDetails.setProductId(productId);
        orderDetails.setGlbcertUniqueId(glbcertId);

        AwardCreatedEvent awardCreatedEvent = new AwardCreatedEvent();
        awardCreatedEvent.setClientId(clientId);
        awardCreatedEvent.setAwardId(awardHistoryId);
        awardCreatedEvent.setGlbcertId(glbcertId);
        awardCreatedEvent.setProductTypeId(productId);

        AwardDetailsTO awardDetailsTO = new AwardDetailsTO();
        awardDetailsTO.setProProdId(productId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(awardCreatedEvent);

        when(orderDetailsService.findByGlbcertId(clientId, glbcertId)).thenReturn(orderDetails);
        when(awardDetailsService.findLifeEventAward(clientId, glbcertId)).thenReturn(awardDetailsTO);

        awardCreatedProcessor.process(exchange);

        verify(intgrCelebrationDenormService, atLeastOnce()).denormalizeAwardCreation(any(AwardDetailsTO.class));

    }

    @Test
    public void process_SM_success() throws Exception {
        String glbcertId = "CAT2-XQUAH8-T9TYDA";
        Long clientId = 33L;
        Long awardHistoryId = 1452134785L;
        Long productId = 650l;
        OrderDetailsTO orderDetails = new OrderDetailsTO();
        orderDetails.setProductId(productId);
        orderDetails.setGlbcertUniqueId(glbcertId);

        AwardCreatedEvent awardCreatedEvent = new AwardCreatedEvent();
        awardCreatedEvent.setClientId(clientId);
        awardCreatedEvent.setAwardId(awardHistoryId);
        awardCreatedEvent.setGlbcertId(glbcertId);
        awardCreatedEvent.setProductTypeId(productId);

        AwardDetailsTO awardDetailsTO = new AwardDetailsTO();
        awardDetailsTO.setProProdId(productId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(awardCreatedEvent);

        when(awardDetailsService.isTimelinesYosAward(glbcertId)).thenReturn(Boolean.TRUE);
        when(orderDetailsService.findByGlbcertId(clientId, glbcertId)).thenReturn(orderDetails);
        when(awardDetailsService.findServiceMilestonesAward(clientId, glbcertId)).thenReturn(awardDetailsTO);

        awardCreatedProcessor.process(exchange);

        verify(intgrCelebrationDenormService, atLeastOnce()).denormalizeAwardCreation(any(AwardDetailsTO.class));
    }

    @Test
    public void process_with_wrong_productId_skipped() throws Exception {
        String glbcertId = "CAT2-XQUAH8-T9TYDA";
        Long clientId = 33L;
        Long awardHistoryId = 1452134785L;
        Long productId = 620l;
        OrderDetailsTO orderDetails = new OrderDetailsTO();
        orderDetails.setProductId(productId);

        AwardCreatedEvent awardCreatedEvent = new AwardCreatedEvent();
        awardCreatedEvent.setClientId(clientId);
        awardCreatedEvent.setAwardId(awardHistoryId);
        awardCreatedEvent.setGlbcertId(glbcertId);
        awardCreatedEvent.setProductTypeId(productId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(awardCreatedEvent);

        when(orderDetailsService.findByGlbcertId(clientId, glbcertId)).thenReturn(orderDetails);

        awardCreatedProcessor.process(exchange);
        verify(intgrCelebrationDenormService, never()).denormalizeAwardCreation(any(AwardDetailsTO.class));
        verify(orderDetailsService, never()).findByGlbcertId(eq(clientId), eq(glbcertId));
    }

    @Test
    public void process_without_award_details_failed() throws Exception {
        String glbcertId = "CAT2-XQUAH8-T9TYDA";
        Long clientId = 33L;
        Long awardHistoryId = 1452134785L;
        Long productId = 680l;
        OrderDetailsTO orderDetails = new OrderDetailsTO();
        orderDetails.setProductId(productId);

        AwardCreatedEvent awardCreatedEvent = new AwardCreatedEvent();
        awardCreatedEvent.setClientId(clientId);
        awardCreatedEvent.setAwardId(awardHistoryId);
        awardCreatedEvent.setGlbcertId(glbcertId);
        awardCreatedEvent.setProductTypeId(productId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(awardCreatedEvent);

        when(orderDetailsService.findByGlbcertId(clientId, glbcertId)).thenReturn(orderDetails);
        when(awardDetailsService.findLifeEventAward(clientId, glbcertId)).thenReturn(null);


        IntgrCelebrationDenormUpdateDataException thrown = assertThrows(IntgrCelebrationDenormUpdateDataException.class, () -> {
            awardCreatedProcessor.process(exchange);
        });

        verify(intgrCelebrationDenormService, never()).denormalizeAwardCreation(any(AwardDetailsTO.class));
    }


    @Test
    public void process_without_glbcertId_failed() throws Exception {
        String glbcertId = new String();
        Long clientId = 33L;
        Long awardHistoryId = 1452134785L;
        Long productId = 680l;

        AwardCreatedEvent awardCreatedEvent = new AwardCreatedEvent();
        awardCreatedEvent.setClientId(clientId);
        awardCreatedEvent.setAwardId(awardHistoryId);
        awardCreatedEvent.setGlbcertId(glbcertId);
        awardCreatedEvent.setProductTypeId(productId);

        CamelContext mockContext = new DefaultCamelContext();
        Exchange exchange = new DefaultExchange(mockContext);
        exchange.getIn().setBody(awardCreatedEvent);

        IntgrCelebrationDenormUpdateDataException thrown = assertThrows(IntgrCelebrationDenormUpdateDataException.class, () -> {
            awardCreatedProcessor.process(exchange);
        });

        verify(intgrCelebrationDenormService, never()).denormalizeAwardCreation(any(AwardDetailsTO.class));
    }

    @Test
    public void getAwardDetails_whenAwardDetailsIsNull_returnNull() {
        long clientId = 901l;
        long awardHistoryId = 9001L;
        OrderDetailsTO orderDetails = new OrderDetailsTO();
        String glbcertId = "NULL-BLOODY-EXISTS";
        long yosProductId = 650L;
        orderDetails.setGlbcertUniqueId(glbcertId);
        orderDetails.setProductId(yosProductId);
        when(awardDetailsService.findServiceMilestonesAward(clientId, glbcertId)).thenReturn(null);
        AwardDetailsTO actualAwardDetails = awardCreatedProcessor.getAwardDetails(clientId, awardHistoryId, orderDetails);
        verify(awardDetailsService).findServiceMilestonesAward(eq(clientId), eq(glbcertId));
        assertNull(actualAwardDetails);

        long premiumProductId = 680L;
        orderDetails.setProductId(premiumProductId);
        when(awardDetailsService.findLifeEventAward(clientId, glbcertId)).thenReturn(null);
        actualAwardDetails = awardCreatedProcessor.getAwardDetails(clientId, awardHistoryId, orderDetails);
        verify(awardDetailsService).findLifeEventAward(eq(clientId), eq(glbcertId));
        assertNull(actualAwardDetails);
    }

}

